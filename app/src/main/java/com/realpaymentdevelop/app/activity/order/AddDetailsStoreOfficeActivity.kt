package com.realpaymentdevelop.app.activity.order

import android.content.Intent
import android.os.Bundle
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import kotlinx.android.synthetic.main.activity_add_details_store_office.*

class AddDetailsStoreOfficeActivity : JupiterNewActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.building_title)
        setContentView(R.layout.activity_add_details_store_office)
    }

    override fun onResume() {

        super.onResume()
        val merchantId = intent.getStringExtra(getString(R.string.key_merchants))
        val token = intent.getStringExtra(getString(R.string.key_token))
        val merchantName = intent.getStringExtra(getString(R.string.key_name))
        val merchantLocationId = intent.getStringExtra(getString(R.string.key_merchants_localization))
        val buildingDelivery = intent.getBooleanExtra(getString(R.string.key_building_delivery), false)
        val locationName = intent.getStringExtra(getString(R.string.key_localization_name))
        val url = intent.getStringExtra(getString(R.string.key_url_image))
        val locationJson = intent.getStringExtra(getString(R.string.key_localization))
        val tipEnable = intent.getIntExtra(getString(R.string.key_enable), 0)
        val pickupSpot = intent.getBooleanExtra(getString(R.string.key_pickupSpot), false)
        val pickupCounter = intent.getBooleanExtra(getString(R.string.key_pickupCounter), false)
        val fullDelivery = intent.getBooleanExtra(getString(R.string.key_full_delivery), false)

        btn_add_details_store_next.setOnClickListener {
            val intentExpress = Intent(this, CategoriesActivity::class.java)
            val building = "{\"buildingPickupDescription\":\"${til_add_details_store_office.editText!!.text}\"}"
            val deliveryJSON = "{\"location\":$locationJson," +
                    "\"delivery\":{" +
                    "\"pickupCounter\":false," +
                    "\"pickupSpot\":false," +
                    "\"buildingDelivery\":$buildingDelivery," +
                    "\"fullDelivery\":false}," +
                    "\"type\":3," +
                    "\"fullDelivery\":{\"building\":$building}}"
            intentExpress.putExtra(getString(R.string.key_json_null), deliveryJSON)
            intentExpress.putExtra(getString(R.string.key_localization), locationJson)
            intentExpress.putExtra(getString(R.string.key_merchants), merchantId)
            intentExpress.putExtra(getString(R.string.key_token), token)
            intentExpress.putExtra(getString(R.string.key_name), merchantName)
            intentExpress.putExtra(getString(R.string.key_merchants_localization), merchantLocationId)
            intentExpress.putExtra(getString(R.string.key_localization_name), locationName)
            intentExpress.putExtra(getString(R.string.key_url_image), url)
            intentExpress.putExtra(getString(R.string.key_enable), tipEnable)
            startActivity(intentExpress)
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }

    override fun onBackPressed() {
        finish()
    }
}
