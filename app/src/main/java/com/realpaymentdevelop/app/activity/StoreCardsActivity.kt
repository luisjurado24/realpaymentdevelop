package com.realpaymentdevelop.app.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.AdapterStoreGift
import com.realpaymentdevelop.app.customviews.ExtendableListView
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.Config
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import com.realpaymentdevelop.app.utils.Utils
import kotlinx.android.synthetic.main.activity_store_cards.*
import org.json.JSONException
import org.json.JSONObject

class StoreCardsActivity : JupiterNewActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpContext(this)
        setContentView(R.layout.activity_store_cards)
        title = getString(R.string.store_cards)
        getStoreActivity()

    }
    private fun getStoreActivity(){
        val http = JupiterNewHttp(this, "/getGiftcards")
        http.setOnSuccess {
            Log.d("MainStoreFragment", "giftcard $it")
            runOnUiThread {
                cardAdapter(it)
            }
        }

        http.setOnFailure {
            val message = if (it == null) getString(R.string.error_an_occurred) else it.getString("message")
            runOnUiThread { errorAlert(this, "$title", message) { } }
        }

        http.setOnConnectFailure {
            val msg = getString(R.string.unfortunately_timeout_message)
            runOnUiThread { Toast.makeText(context, msg, Toast.LENGTH_SHORT).show() }
        }

        val user = Utils.makeUser(this, this)
        val config = Config(this)
        val token: String? = config.get("token", "undefined")
        val requestParams = java.util.ArrayList<Pair<String, String>>(2)
        requestParams.add(Pair("ApiKey", getString(R.string.apikey)))
        requestParams.add(Pair("AccessToken", token!!))
        requestParams.add(Pair("userId", user.userId))
        http.doAsyncApiCallMenu(requestParams)
    }

    private fun cardAdapter(response: JSONObject) {
        try {
            Log.d("doFinalCallback: ", "Result: $response")

            Log.d("getGiftcards:", response.toString())
            val giftcards = response.getJSONArray("body")

            if (giftcards.length() > 0) {

                // Need merchantName and Balance for each giftcard
                val adapter = AdapterStoreGift(this, giftcards)
                val listRecords = listGiftcards
                listRecords.adapter = adapter

                listRecords.onItemClickListener =
                    AdapterView.OnItemClickListener { parent, view, position, id ->
                        try {
                            var token: String
                            val giftcardRow = giftcards.getJSONObject(position)
                            val tokenUnformatted = giftcardRow.getString("token")
                            token = Utils.formatToken(tokenUnformatted)
                            // Launch the qr code screen
                            val displayIntent = Intent(this, ShowGiftCardActivity::class.java)
                            displayIntent.putExtra(getString(R.string.key_token), token)
                            val giftCardImagePath = giftcardRow.getString("giftcardImagePath")
                            val giftCardImageName = giftcardRow.getString("giftcardImageName")
                            val giftCardBalance = giftcardRow.getString("balance")
                            val activationDate = giftcardRow.optString("futureActivationDate", "")
                            val expirationDate = giftcardRow.optString("expirationDate", "")
                            val giftCardId = giftcardRow.getString("prepaidAccountId")
                            if (!(giftCardImageName.isEmpty() || giftCardImageName == "null")) {
                                displayIntent.putExtra(
                                    "ICON",
                                    resources.getString(R.string.static_hostname) + giftCardImagePath + giftCardImageName
                                )
                            }
                            displayIntent.putExtra(
                                getString(R.string.key_merchants),
                                giftcardRow.getString("merchantName")
                            )
                            displayIntent.putExtra(
                                getString(R.string.key_card),
                                Utils.getPrepaidType(giftcardRow.getInt("prepaidAccountTypeId"))
                            )
                            displayIntent.putExtra(
                                getString(R.string.key_balance),
                                Utils.formatMoney(giftCardBalance)
                            )
                            displayIntent.putExtra(
                                getString(R.string.key_giftcard),
                                giftCardId
                            )
                            displayIntent.putExtra(
                                getString(R.string.key_expiry),
                                Utils.formatValidDates(activationDate, expirationDate, this)
                            )
                            startActivity(displayIntent)
                        } catch (e: JSONException) {
                            Log.e("ErrorStoreCards", e.toString())
                        }
                    }

            } else {
                Toast.makeText(context, getString(R.string.no_cards_to_show), Toast.LENGTH_SHORT).show()
            }
        } catch (e: JSONException) {
            e.printStackTrace()
            // Something went very wrong
            Log.d("Api success result: ", "Error: Json Exception.")

        }


    }
}