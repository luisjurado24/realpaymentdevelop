package com.realpaymentdevelop.app.activity.order

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import berlin.volders.badger.BadgeShape
import berlin.volders.badger.Badger
import berlin.volders.badger.CountBadge
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.order.AdapterCategories
import com.realpaymentdevelop.app.db.DataBase
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import kotlinx.android.synthetic.main.activity_combo.*
import org.json.JSONArray
import org.json.JSONObject

class CategoriesActivity : JupiterNewActivity(), AdapterCategories.onClickCategories {
    var finalOrderExtras: JSONArray? = JSONArray()
    var merchantId: String? = ""
    var merchantLocationId: String? = ""
    var accesstoken: String? = ""
    var tipEnable = 0
    var jsonDelivery: String? = ""
    var nameMerchant: String? = ""
    var locationName: String? = ""
    var data = JSONObject()
    var cartSize = 0
    private var badge :CountBadge? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpContext(this)
        setContentView(R.layout.activity_combo)
        title = resources.getString(R.string.activity_menu)
        nameMerchant = intent.getStringExtra(getString(R.string.key_name))
        locationName = intent.getStringExtra(getString(R.string.key_localization_name))
        jsonDelivery = intent.getStringExtra(getString(R.string.key_json_null))
        tipEnable = intent.getIntExtra(getString(R.string.key_enable), -1)
        accesstoken = intent.getStringExtra(getString(R.string.key_token))
        merchantId = intent.getStringExtra(getString(R.string.key_merchants))
        merchantLocationId = intent.getStringExtra(getString(R.string.key_merchants_localization))
        cartSize = DataBase(this).getCart(merchantId!!,merchantLocationId!!).size
        getAllMenu()
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val factory = CountBadge.Factory(this, BadgeShape.circle(0.5f, Gravity.TOP))
        val menuItem = menu?.findItem(R.id.cart_menu)
        badge = Badger.sett(menuItem!!, factory)
        badge?.count = cartSize
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_card, menu)
        val factory = CountBadge.Factory(this, BadgeShape.circle(0.5f,Gravity.TOP))
        val menuItem = menu?.findItem(R.id.cart_menu)
        badge = Badger.sett(menuItem!!, factory)
        badge?.count = cartSize
        return true

    }

    override fun onResume() {
        super.onResume()
        if(badge != null){
            val data= DataBase(this).getCart(merchantId!!,merchantLocationId!!).size
            badge?.count = data
            Log.e("TAG","NOTmenuItemNull")
        }else{
            Log.e("TAG","menuItemNull")
        }
    }

    private fun getAllMenu() {

        val http = JupiterNewHttp(this, "/quickOrderApi", "/getMenu")

        http.setOnSuccess {
            data = it.getJSONObject("data")
            var menu: MutableList<JSONObject> = mutableListOf()
            menu = buildMenu(menu)
            if (menu.size >= 0) {
                setElementsOnDisplay(menu, data)
            }

        }

        http.setOnFailure {

            val msg = getString(R.string.message_orders_disabled)
            runOnUiThread {
                errorAlert(this, "$title", msg) {
                    finish()
                }
            }
        }

        http.setOnConnectFailure {

            val msg = getString(R.string.unfortunately_timeout_message)
            runOnUiThread {
                errorAlert(this, "$title", msg) { onBackPressed() }
            }
        }

        val requestParams: ArrayList<Pair<String, String>> = ArrayList()
        requestParams.add(Pair("AccessToken", accesstoken!!))
        requestParams.add(Pair("merchantId", merchantId!!))
        requestParams.add(Pair("merchantLocationId", merchantLocationId!!))

        http.doAsyncApiCallMenu(requestParams)
    }

    private fun buildMenu(menu: MutableList<JSONObject>): MutableList<JSONObject> {
        val categories = data.getJSONObject("Categories")
        val keys = categories.keys()
        while (keys.hasNext()) {
            val key = keys.next() as String
            if (categories.get(key) is JSONObject) {
                val categories = JSONObject(categories.get(key).toString())
                //menu.add(categories)
                if (!categories.getJSONObject("QuickCategory").getBoolean("deleted")) {
                    menu.add(categories)
                }
            }
        }

        //Second sort it
        menu.sortBy {
            it.getJSONObject("QuickCategory").getInt("sequenceOfDisplay")
        }

        val extrasOrder =
            if (data.has("ModifierItemGroup")) data.optJSONObject("ModifierItemGroup") else null
        finalOrderExtras = createJSONWithSortOfExtras(extrasOrder)
        return menu
    }

    private fun createJSONWithSortOfExtras(extrasOrder: JSONObject?): JSONArray? {

        val extras = JSONArray()
        var result: JSONArray? = null

        if (extrasOrder != null) {
            val keys = extrasOrder.keys()
            while (keys.hasNext()) {
                val key = keys.next() as String
                if (extrasOrder.get(key) is JSONObject) {
                    val parent = JSONObject(extrasOrder.get(key).toString())
                    extras.put(parent)
                }
            }
            result = JSONArray()
            for (i in 0 until extras.length()) {
                val quickItemModifierGroup =
                    extras.getJSONObject(i).getJSONObject("QuickItemModifierGroup")
                result.put(quickItemModifierGroup)
            }
        }
        return result
    }

    private fun setElementsOnDisplay(menu: MutableList<JSONObject>, data: JSONObject) {
        val listRecord = combo_list_view
        val url = intent.getStringExtra(getString(R.string.key_url_image))
        Log.e("url", "$url")
        runOnUiThread {
            listRecord.adapter = AdapterCategories(this, menu, url!!)
            val linear = LinearLayoutManager(this)
            linear.orientation = LinearLayoutManager.VERTICAL
            listRecord.layoutManager = linear
        }
    }

    private fun buildItemsMenu(idCategory: Int, data: JSONObject): MutableList<JSONObject> {

        val items: MutableList<JSONObject> = mutableListOf()
        val itemsLocal = data.getJSONObject("Items")

        val keys = itemsLocal.keys()
        while (keys.hasNext()) {
            val key = keys.next() as String
            if (itemsLocal.get(key) is JSONObject) {
                val itemToSelect = JSONObject(itemsLocal.get(key).toString())
                if (itemToSelect.getJSONObject("QuickItem").get("quickCategoryId").toString()
                        .toInt() == idCategory
                    && itemToSelect.getJSONObject("QuickItem").get("available").toString() != "null"
                    && itemToSelect.getJSONObject("QuickItem").get("available").toString()
                        .toInt() == 1
                ) {
                    items.add(itemToSelect)
                }
            }
        }

        items.sortBy {
            it.getJSONObject("QuickItem").getInt("sequenceOfDisplay")
        }

        return items
    }

    override fun clickCategories(records: MutableList<JSONObject>, position: Int, merchantImage: String) {
        if (records[position].getJSONObject("QuickCategory").getBoolean("enabled")) {
            val itemsMenu = buildItemsMenu(
                records[position].getJSONObject("QuickCategory").getInt("quickCategoryId"), data
            )
            Log.d("ItemSelected", records[position].toString())
            when (itemsMenu.size) {
                0 -> Toast.makeText(this, getString(R.string.no_items_show), Toast.LENGTH_SHORT)
                    .show()
                else -> selectMenuCategory(
                    records[position].getJSONObject("QuickCategory").getString("name"),
                    records[position].getJSONObject("QuickCategory").getInt("quickCategoryId"),
                    finalOrderExtras, data, merchantImage
                )
            }
        }
    }

    private fun selectMenuCategory(name: String, itemsMenu: Int, finalOrderExtras: JSONArray?, data: JSONObject, url: String) {
        val dataBase = DataBase(this).getCart(merchantId!!,merchantLocationId!!).size
        val intent = Intent(this, DishActivity::class.java)

        intent.putExtra(getString(R.string.key_card), dataBase)
        intent.putExtra(getString(R.string.key_enable), tipEnable)
        intent.putExtra(getString(R.string.key_url_image), url)
        intent.putExtra(getString(R.string.key_title), name)
        intent.putExtra(getString(R.string.key_dish), itemsMenu)
        intent.putExtra(getString(R.string.key_token), accesstoken)
        intent.putExtra(getString(R.string.key_merchants), merchantId)
        intent.putExtra(getString(R.string.key_localization), merchantLocationId)
        intent.putExtra(getString(R.string.key_name), nameMerchant)
        intent.putExtra(getString(R.string.key_localization_name), locationName)
        intent.putExtra(getString(R.string.key_json_null), jsonDelivery)
        intent.putExtra(getString(R.string.key_categories), data.toString())
        intent.putExtra(getString(R.string.key_products), finalOrderExtras.toString())
        startActivityForResult(intent,10)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.cart_menu -> {
                val data = DataBase(this).getCart(merchantId!!,merchantLocationId!!).size
                if(data!=0) {
                    val factory = CountBadge.Factory(this,BadgeShape.circle(0.5f,Gravity.TOP))
                    val intent = Intent(this, ConfirmOrderActivity::class.java)
                    intent.putExtra(getString(R.string.key_json_null), jsonDelivery)
                    intent.putExtra(getString(R.string.key_name), nameMerchant)
                    intent.putExtra(getString(R.string.key_localization_name), locationName)
                    intent.putExtra(getString(R.string.key_token), accesstoken)
                    intent.putExtra(getString(R.string.key_merchants), merchantId)
                    intent.putExtra(getString(R.string.key_localization), merchantLocationId)
                    intent.putExtra(getString(R.string.key_enable), tipEnable)
                    startActivity(intent)
                    Badger.sett(item, factory).count = data
                }else{
                    Toast.makeText(this,getString(R.string.not_elements), Toast.LENGTH_SHORT).show()
                }
            }
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(resultCode){
            Activity.RESULT_OK-> {

            }
            Activity.RESULT_CANCELED ->{ }
        }
    }

}