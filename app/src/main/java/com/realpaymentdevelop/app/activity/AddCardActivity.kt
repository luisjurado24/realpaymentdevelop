package com.realpaymentdevelop.app.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.data.CreditCards
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.*
import kotlinx.android.synthetic.main.activity_add_card.*
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.Year
import java.util.*

class AddCardActivity : JupiterNewActivity() {
    private val TAG = "AddCard"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = if(intent.getStringExtra("title")!= null){
            intent.getStringExtra("title")
        }else{
            resources.getString(R.string.title_activity_add_card)
        }
        setContentView(R.layout.activity_add_card)
        val date = SimpleDateFormat("yyyy").format(Date())
        var listYear= listOf<Int>()
        listYear=listYear.plus(date.toInt())
        for(i in 1..6){
            listYear=listYear.plus((date.toInt())+i)
        }
        var month = listOf<Int>()
        for(x in 1..12){
            month = month.plus(x)
        }
        val adapterMonth= ArrayAdapter(this,R.layout.list_item,month)
        val adapterYear = ArrayAdapter(this,R.layout.list_item,listYear)
        Log.e("TAGMonth",month.toString())
        Log.e("TAGYear",listYear.toString())
        (til_add_credit_expiration_date_year.editText as? AutoCompleteTextView)?.setAdapter(
            adapterYear
        )
        (til_add_credit_expiration_date_month.editText as? AutoCompleteTextView)?.setAdapter(
            adapterMonth
        )
        val config = Config(applicationContext)

        val token: String = config["token", "undefined"]!!
        if (token != "undefined") {
            makeUser()
        } else {
            config["remember"] = "false"
            config["token"] = "undefined"
            Utils.launch(this, LogInActivity::class.java)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()

        val numberCard = til_add_credit_card_number
        val dateYear = til_add_credit_expiration_date_year
        val dateMonth = til_add_credit_expiration_date_month
        val cVV = til_add_credit_security_code
        val nameCard = til_add_credit_name_on_card
        val address = til_add_credit_street_address
        val city = til_add_credit_city
        val zIP = til_add_credit_postal_code
        val state = til_add_credit_state

        btn_activity_add_card.setOnClickListener {
            val booleanName = til_add_credit_card_number.editText!!.text.isNullOrBlank()
            val booleanCVV = til_add_credit_security_code.editText!!.text.isNullOrBlank()
            val booleanNameCard = til_add_credit_name_on_card.editText!!.text.isNullOrBlank()
            val booleanAddress = til_add_credit_street_address.editText!!.text.isNullOrBlank()
            val booleanCity = til_add_credit_city.editText!!.text.isNullOrBlank()
            val booleanZIP = til_add_credit_postal_code.editText!!.text.isNullOrBlank()
            val booleanState = til_add_credit_state.editText!!.text.isNullOrBlank()
            if (booleanAddress or booleanCVV or booleanCity or booleanName or
                booleanNameCard or booleanState or booleanZIP
            ) {
                if (booleanAddress) address.error =
                    resources.getString(R.string.error_password_bank)
                else address.error = null
                if (booleanCVV) cVV.error = resources.getString(R.string.error_password_bank)
                else cVV.error = null
                if (booleanCity) city.error = resources.getString(R.string.error_password_bank)
                else city.error = null
                if (booleanName) numberCard.error =
                    resources.getString(R.string.error_password_bank)
                else numberCard.error = null
                if (booleanNameCard) nameCard.error =
                    resources.getString(R.string.error_password_bank)
                else nameCard.error = null
                if (booleanState) state.error = resources.getString(R.string.error_password_bank)
                else state.error = null
                if (booleanZIP) zIP.error = resources.getString(R.string.error_password_bank)
                else zIP.error = null
            } else {
                if ((cVV.editText!!.text.length != 3) or (numberCard.editText!!.text.length != 16)
                or (dateMonth.editText!!.text.length>2) or (dateYear.editText!!.text.length>4)) {
                    if (numberCard.editText!!.text.length != 16)
                        numberCard.error = resources.getString(R.string.error_password_bank)
                    else numberCard.error = null
                    if (cVV.editText!!.text.length != 3){
                        cVV.error = resources.getString(R.string.error_password_bank)
                    }
                    else cVV.error = null
                    if (dateMonth.editText!!.text.length != 2)
                        dateMonth.error = resources.getString(R.string.error_password_bank)
                    else dateMonth.error= null
                    if (dateYear.editText!!.text.length != 4)
                        dateYear.error = resources.getString(R.string.error_password_bank)
                    else dateYear.error = null

                    address.error = null
                    state.error = null
                    zIP.error = null
                    city.error = null

                    nameCard.error = null
                } else {
                    dateMonth.error = null
                    dateYear.error = null
                    // Get values

                    val jsonPayload = String.format(
                        getString(R.string.add_credit_card_payload),
                        numberCard.editText!!.text,
                        dateYear.editText!!.text,
                        dateMonth.editText!!.text,
                        cVV.editText!!.text,
                        nameCard.editText!!.text,
                        address.editText!!.text,
                        city.editText!!.text,
                        state.editText!!.text,
                        zIP.editText!!.text
                    )

                    if(title==getString(R.string.title_activity_add_card)){
                        addCard(jsonPayload)
                    }else{
                        returnActivityPayOrder(jsonPayload)
                    }

                }
            }

        }
    }

    private fun returnActivityPayOrder(encrypted:String){
        var intent =intent.putExtra(getString(R.string.key_card),encrypted)
        intent =intent.putExtra("aa",4321)
        setResult(RESULT_OK,intent)
        finish()
    }

    private fun processAddCardResult(response: JSONObject) {
        try {
            Log.d(TAG, "processAddCardResult: $response")
            val success = response.getInt("success")
            val responseCode = response.getInt("responseCode")
            when (success) {
                0 -> {
                    //Toast.makeText(context, , Toast.LENGTH_SHORT).show();
                    message("Oops, something went wrong. Please try again in a few minutes.")
                    return
                }
                1 -> if (responseCode == 100) {
                    runOnUiThread {
                        errorAlert(this,"$title",response.getString("message")) {onBackPressed()}
                    }
                    return
                } else {
                    //Toast.makeText(context, "Oops, something went wrong. Please try again in a few minutes.", Toast.LENGTH_SHORT).show();
                    message("Oops, something went wrong. Please try again in a few minutes.")
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
            // Something went very wrong
            Log.d("Api success result: ", "Error: Json Exception.")
            // LANG: translation
            //Toast.makeText(context, "Error accessing data. Try again in a few minutes.", Toast.LENGTH_LONG).show();
            message("Error accessing data. Try again in a few minutes.")
        }
    }

    private fun addCard(jsonPayload: String) {
        var token: String = ""
        var userID: String = ""

        val serial_login = config["login", "undefined"]
        if (serial_login !== "undefined") {
            try {
                val user = JSONObject(serial_login)
                token = user.getString("token")
                userID = user.getString("userId")
            } catch (e: JSONException) {
                Log.d("makeUser", "JSONException caught: " + e.message)
            }
        } else {
            config["login"] = "undefined"
            config["remember"] = "false"
            config["token"] = "undefined"
            Utils.launch(this, LogInActivity::class.java)
        }

        val apikey = resources.getString(R.string.apikey)
        val requestParams = ArrayList<Pair<String, String>>(3)
        requestParams.add(Pair("ApiKey", apikey))
        requestParams.add(Pair("AccessToken", token))
        requestParams.add(Pair("userId", userID))
        val jsonObject = JSONObject(jsonPayload)
        jsonObject.keys().forEach {
            requestParams.add(Pair(it,jsonObject.getString(it)))
        }
        //requestParams.add(Pair("payload", jsonPayload))
        val http = JupiterNewHttp(this, "/addCard")
        http.setOnSuccess { it: JSONObject? ->
            Log.d("AddCard", "addCard::onSuccess called")
            if (it != null) {
                Log.d(TAG,  "addCardSucces")
                processAddCardResult(it)
            }

        }
        http.setOnFailure {
            if (it != null) {
                Log.d(TAG,  "addCardFailure")
                processAddCardResult(it)
            }
        }
        http.setOnConnectFailure {
            Log.e("TAG","$it")
            val msg = getString(R.string.unfortunately_timeout_message)
            message(msg)

        }
        http.doAsyncApiCallMenu(requestParams)
    }

    private fun message(msg: String) {
        runOnUiThread { errorAlert(this, "$title",msg) {onBackPressed()} }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

}