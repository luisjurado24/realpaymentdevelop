package com.realpaymentdevelop.app.activity

import android.content.Intent
import android.content.res.Resources
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.widget.Toast
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.utils.Config
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import com.realpaymentdevelop.app.utils.Utils
import kotlinx.android.synthetic.main.activity_in_log.*
import org.json.JSONObject
import java.io.InputStream
import java.lang.Exception
import java.util.*

class LogInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.NoActionBarLight)
        setContentView(R.layout.activity_in_log)
        if (intent != null) {
            til_login_phone_number.editText!!.setText(intent.getStringExtra(resources.getString(R.string.key_phone_number)))
        } else {
            til_login_password.editText!!.text.clear()
            til_login_phone_number.editText!!.text.clear()
        }
        try {
            val stringPath = "android.resource://" + packageName + "/" + R.raw.second_video
            vv_login_activity.setVideoURI(Uri.parse(stringPath))
            vv_login_activity.start()
            vv_login_activity.setOnPreparedListener {
                it.isLooping = true
                it.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
                it.setScreenOnWhilePlaying(true)
            }
            vv_login_activity.start()
            vv_login_activity.setOnErrorListener(MediaPlayer.OnErrorListener { mediaPlayer, i, i2 ->
                contrain_login.setBackgroundResource(R.drawable.login)
                vv_login_activity.visibility = View.INVISIBLE

                return@OnErrorListener true
            })

        } catch (e: Exception) {
            Log.e("TAGLogin", "Error Video${e}")

        }
        tv_login_doSignup.text =
            Html.fromHtml(resources.getString(R.string.sign_up_for_realpayment))
        tv_login_doForgot.text = Html.fromHtml(resources.getString(R.string.forgot_password))

    }

    override fun onResume() {
        super.onResume()
        tv_login_doForgot.setOnClickListener {
            Utils.launch(this, ForgotPasswordActivity::class.java)
        }
        tv_login_doSignup.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }
        btn_login__doLogin.setOnClickListener {
            val passwordBoolean = til_login_password.editText?.text.isNullOrBlank()
            val phoneBoolean = til_login_phone_number.editText?.text.isNullOrBlank()
            if (passwordBoolean or phoneBoolean) {
                if (passwordBoolean) til_login_password.error =
                    resources.getString(R.string.error_password)
                else til_login_password.error = null
                if (phoneBoolean) til_login_phone_number.error =
                    resources.getString(R.string.error_phone_number)
                else til_login_phone_number.error = null
            } else {
                login()
            }
        }

    }


    private fun login() {
        val http = JupiterNewHttp(this, "/login")
        http.setOnSuccess {
            val config = Config(this)
            config["login"] = it.toString()
            val token = it.getString("token")
            config["token"] = token
            config.rememberMe = sw_remember.isChecked
            config.username = til_login_phone_number.editText!!.text.toString()
            config.avatarURL = it.getString("fullImageUrl")
            config.name = it.getString("firstName")
            Log.d(TAG, "$it")
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }

        http.setOnFailure {
            Log.d(Companion.TAG, it.toString())
            val message = when (it?.getInt("responseCode")) {
                200 -> getString(R.string.error_message_user_password_incorrect)
                261 -> getString(R.string.error_message_user_password_incorrect)
                else -> getString(R.string.generic_error_message)
            }
            runOnUiThread { errorAlert(this, "Login", message) { } }
        }

        http.setOnConnectFailure {
            val message = getString(R.string.unable_to_connect)
            runOnUiThread { errorAlert(this, "Login", message) { } }
        }

        val requestParams = ArrayList<Pair<String, String>>(2)
        requestParams.add(Pair("Username", til_login_phone_number.editText!!.text.toString()))
        requestParams.add(Pair("Password", til_login_password.editText!!.text.toString()))
        http.doAsyncApiCallMenu(requestParams)
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (backPressed + 2000 > System.currentTimeMillis()) {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        } else {
            Toast.makeText(this, resources.getString(R.string.tap_to_exit), Toast.LENGTH_SHORT)
                .show()
        }
        backPressed = System.currentTimeMillis()
    }

    companion object {
        const val TAG = "LoginAct"
        private var backPressed: Long = 0
    }

}