package com.realpaymentdevelop.app.activity.order

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_confirm_order.*
import kotlinx.android.synthetic.main.activity_review_order.*
import org.json.JSONObject
import java.text.DecimalFormat

class ReviewOrderActivity : JupiterNewActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpContext(this)
        title = getString(R.string.title_activity_review_order)
        setContentView(R.layout.activity_review_order)
        val cart = intent.getStringExtra(getString(R.string.key_json_null))
        val localization = intent.getStringExtra(getString(R.string.key_localization))
        val nameMerchant = intent.getStringExtra(getString(R.string.key_name))
        val nameLocation = intent.getStringExtra(getString(R.string.key_localization_name))
        tv_review_merchant_title.text= nameLocation
        val localizationJson= JSONObject(localization)
        val location = tv_review_merchant_localization
        Log.e("locationJson",localizationJson.toString())
        val jsonCart = JSONObject(cart)
        if(jsonCart.getJSONArray("Extras").length()<0){
            tv_merchant_topics.visibility = View.INVISIBLE
        }else{
            val jsonExtras= jsonCart.getJSONArray("Extras")
            var topics = ""
            for(a in 0 until jsonExtras.length()){
                val json = jsonExtras.getJSONObject(a)
                topics += "${json.getString("name")}\n"
            }
            tv_merchant_topics.text = topics
        }
        when (localizationJson.getInt("type")) {
            0 -> {
                location.text = "${getString(R.string.delivery_title_0)}\n\t${getString(R.string.delivery_subtitle_0)}"
            }
            1 -> {
                val name =
                    localizationJson.getJSONObject("pickupSpot").getString("displayName")
                location.text = "${getString(R.string.delivery_title_1)}\n$name"
            }
            2 -> {
                val fullDelivery =
                    localizationJson.getJSONObject("fullDelivery").getJSONObject("address")
                val address1 = fullDelivery.getString("deliveryAddress1")
                val address2 = fullDelivery.getString("deliveryAddress2")
                val city = fullDelivery.getString("deliveryCity")
                val state = fullDelivery.getString("deliveryState")
                val zip = fullDelivery.getString("deliveryZipCode")
                when {
                    address1 == "null" -> {
                        location.text =
                            "${getString(R.string.delivery_title_2)}\n$address2, $city,\n$state, $zip."
                    }
                    address2 == "null" -> {
                        location.text =
                            "${getString(R.string.delivery_title_2)}\n$address1, $city,\n$state, $zip."
                    }
                    else -> {
                        location.text =
                            "${getString(R.string.delivery_title_2)}\n$address1,\n$address2,$city,\n$state, $zip."
                    }
                }
            }
            3 -> {
                val building =
                    localizationJson.getJSONObject("fullDelivery").getJSONObject("building")
                val office = building.getString("buildingPickupDescription")

                location.text = getString(R.string.delivery_title_3) + "\n$office"
            }
        }
        val url = intent.getStringExtra(getString(R.string.key_url_image))
        val urlmerchant = intent.getStringExtra(getString(R.string.key_merchants))
        try{
            Picasso.with(this).load(url).error(R.drawable.placeholder).into(iv_merchant_review)
        }catch (e:Exception){
            Picasso.with(this).load(urlmerchant).error(R.drawable.placeholder).into(iv_merchant_review)
        }
        tv_merchant_dish.text= "${jsonCart.getJSONObject("Item").getString("name")} (${jsonCart.getInt("Quantity")})"
        val mf = DecimalFormat("0.00")
        tv_merchant_price.text = "$${mf.format(jsonCart.getDouble("TotalCost"))}"
    }

    override fun onResume() {
        super.onResume()
        btn_continue_review_order.setOnClickListener {
            setResult(RESULT_OK)
            finish()
        }
    }

    override fun navigateUpTo(upIntent: Intent?): Boolean {
        onBackPressed()
        return true
    }
    override fun onBackPressed() {
        setResult(RESULT_CANCELED)
        super.onBackPressed()
    }

}