package com.realpaymentdevelop.app.activity.order

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.order.AdapterLocalization
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.Config
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import kotlinx.android.synthetic.main.activity_init_order.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class InitOrderActivity : JupiterNewActivity(), AdapterLocalization.onClickLocation{

    var merchantId :String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpContext(this)
        makeUser()
        setContentView(R.layout.activity_init_order)
        title = intent.getStringExtra(getString(R.string.key_name))
        merchantId = intent.getStringExtra(getString(R.string.key_merchants))
        getLocations(merchantId)
    }

    private fun getLocations(merchantId: String?) {
        val url = "${resources.getString(R.string.static_hostname)}${intent.getStringExtra(getString(R.string.key_url_image))}"
        val http = JupiterNewHttp(this, "/quickOrderApi", "/getLocations")
        http.setOnSuccess {
            val array = createArrayLocations(it.getJSONObject("data"))
            if (array.length() > 0) {
                runOnUiThread {
                    elv_init_order.adapter = AdapterLocalization(this, array, url)
                    val linear = LinearLayoutManager(this)
                    linear.orientation = LinearLayoutManager.VERTICAL
                    elv_init_order.layoutManager = linear

                }
            } else {
                val msg = getString(R.string.message_orders_disabled)
                runOnUiThread {
                    errorAlert(this, "$title", msg) { this.onBackPressed() }
                }
            }
        }

        http.setOnFailure {

            val msg = getString(R.string.message_orders_disabled)
            runOnUiThread { errorAlert(this, "$title", msg) { onBackPressed() } }
        }

        http.setOnConnectFailure {

            val msg = getString(R.string.unfortunately_timeout_message)
            runOnUiThread {
                errorAlert(this, "$title", msg) { this.onBackPressed() }
            }
        }
        val config = Config(this)
        val token: String? = config["token", "undefined"]
        val requestParams = ArrayList<Pair<String, String>>(2)
        requestParams.add(Pair("AccessToken", token!!))
        requestParams.add(Pair("merchantId", merchantId!!))
        http.doAsyncApiCallMenu(requestParams)
    }


    private fun createArrayLocations(locations: JSONObject): JSONArray {
        val allLocations = JSONArray()
        val keys = locations.keys()
        while (keys.hasNext()) {
            val key = keys.next() as String
            if (locations.get(key) is JSONObject) {
                val parent = JSONObject(locations.get(key).toString())
                allLocations.put(parent)
            }
        }
        return allLocations
    }

    private fun selectedLocation(location: JSONObject,merchantName:String,merchantId: String?) {
        try {
            val url =
                "${resources.getString(R.string.static_hostname)}${intent.getStringExtra(getString(R.string.key_url_image))}"

            val pickupCounter =
                location.getJSONObject("QuickMerchantLocation").optBoolean("pickupCounter", false)
            val pickupSpot =
                location.getJSONObject("QuickMerchantLocation").optBoolean("pickupSpot", false)
            val buildingDelivery = location.getJSONObject("QuickMerchantLocation")
                .optBoolean("buildingDelivery", false)
            val fullDelivery =
                location.getJSONObject("QuickMerchantLocation").optBoolean("fullDelivery", false)
            val jsonNull = "{\"location\":$location," +
                    "\"delivery\":{" +
                    "\"pickupCounter\":$pickupCounter," +
                    "\"pickupSpot\":$pickupSpot," +
                    "\"buildingDelivery\":$buildingDelivery," +
                    "\"fullDelivery\":$fullDelivery}," +
                    "\"type\":0,"+
                    "\"counterPickup\":\"noDeliveryInformation\"}"
            if (pickupCounter && !pickupSpot && !buildingDelivery && !fullDelivery) {
                val expressOrderIntent = Intent(context, CategoriesActivity::class.java)
                val merchantLocationId =
                    location.getJSONObject("MerchantLocation").getString("merchantLocationId")
                val tipEnable = location.getJSONObject("QuickMerchantLocation").getInt("tipEnabled")
                val locationName = location.getJSONObject("MerchantLocation").getString("name")
                expressOrderIntent.putExtra(getString(R.string.key_merchants), merchantId)
                expressOrderIntent.putExtra(getString(R.string.key_token), token)
                expressOrderIntent.putExtra(getString(R.string.key_name), merchantName)
                expressOrderIntent.putExtra(
                    getString(R.string.key_merchants_localization),
                    merchantLocationId
                )
                expressOrderIntent.putExtra(getString(R.string.key_localization_name), locationName)
                expressOrderIntent.putExtra(getString(R.string.key_url_image), url)
                expressOrderIntent.putExtra(getString(R.string.key_enable), tipEnable)
                expressOrderIntent.putExtra(getString(R.string.key_json_null), jsonNull)

                startActivity(expressOrderIntent)
            } else {
                val deliveryDisplay = Intent(this, SelectedOrderActivity::class.java)
                val merchantLocationId =
                    location.getJSONObject("MerchantLocation").getString("merchantLocationId")
                val tipEnable = location.getJSONObject("QuickMerchantLocation").getInt("tipEnabled")
                val locationName = location.getJSONObject("MerchantLocation").getString("name")

                deliveryDisplay.putExtra(getString(R.string.key_merchants), merchantId)
                deliveryDisplay.putExtra(getString(R.string.key_token), token)
                deliveryDisplay.putExtra(getString(R.string.key_name), merchantName)
                deliveryDisplay.putExtra(
                    getString(R.string.key_merchants_localization),
                    merchantLocationId
                )
                deliveryDisplay.putExtra(getString(R.string.key_localization_name), locationName)
                deliveryDisplay.putExtra(getString(R.string.key_url_image), url)
                deliveryDisplay.putExtra(getString(R.string.key_enable), tipEnable)

                deliveryDisplay.putExtra(getString(R.string.key_pickupCounter), pickupCounter)
                deliveryDisplay.putExtra(getString(R.string.key_full_delivery), fullDelivery)
                deliveryDisplay.putExtra(getString(R.string.key_pickupSpot), pickupSpot)
                deliveryDisplay.putExtra(
                    getString(R.string.key_building_delivery),
                    buildingDelivery
                )
                deliveryDisplay.putExtra(getString(R.string.key_localization), location.toString())

                startActivity(deliveryDisplay)
            }

        } catch (eJSON: JSONException) {
            val msg = getString(R.string.error_an_occurred)
            runOnUiThread {
                errorAlert(this, "$title", msg) { onBackPressed() }
            }
        }
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }
    override fun onBackPressed() {
        finish()
    }

    override fun onClickLocation(location: JSONObject, title: String, abiable:Boolean) {
        if(abiable){
            selectedLocation(location,title,merchantId)
        }else{
            val msg = getString(R.string.message_orders_disabled)
            runOnUiThread {
                errorAlert(this, getString(R.string.title_activity_get_locations), msg, {})
            }
        }
    }

}