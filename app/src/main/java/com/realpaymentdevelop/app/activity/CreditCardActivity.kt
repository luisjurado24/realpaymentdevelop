package com.realpaymentdevelop.app.activity

import android.app.AlertDialog
import android.graphics.Bitmap
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import com.realpaymentdevelop.app.utils.User
import com.realpaymentdevelop.app.utils.Utils
import kotlinx.android.synthetic.main.activity_credit_card.*
import java.util.ArrayList



class CreditCardActivity : JupiterNewActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_credit_card)
        title = resources.getString(R.string.title_activity_view_credit_card)
    }
    val otherCardLogo = "other_logo_badge"
    val visaCardLogo = "visa_logo_badge"
    val mcCardLogo = "mstr_logo_badge"
    val discoverCardLogo = "dcvr_logo_badge"
    val amexCardLogo = "amex_logo_badge"
    var user2: User? = null
    var cardNetwork: String = ""
    var cardLast4: String = ""
    var payToken: String = ""
    var responseCode = 100
    internal inner class Result {
        var payToken: String? = null
        var bmp: Bitmap? = null
        var cardLast4: String? = null
        var cardNetwork: String? = null
        var responseCode : Int = 100
    }


    override fun onStart() {
        super.onStart()
        val token = config.get("token", "undefined")
        if (! token.equals("undefined")) {
            listenEvents()
            user2 = Utils.makeUser(this, this)
            setToken()
        } else {
            config.set("remember", "false")
            config.set("token", "undefined")
            Utils.launch(this, LogInActivity::class.java)
            return
        }
    }
    fun listenEvents() {
        doDone.setOnClickListener { Utils.launch(this, HomeActivity::class.java) }
    }

    fun setToken() {
        val task = object : AsyncTask<String, Void, Result>() {
            internal var result: String? = null
            private val success = false

            override fun onPreExecute() {
                // Maybe put a ProgressBar here
                progress.visibility = View.VISIBLE
                imageToken.visibility = View.GONE
            }

            override fun doInBackground(vararg arg: String) : Result {
                //                Result result = new Result();

                getToken()
                return generateQrCode()

            }

            override fun onPostExecute(result: Result) {
                setLogo()

                when(result.responseCode){
                    0 -> {
                        val msg = getString(R.string.unfortunately_timeout_message)
                        message(msg)
                    }

                    100 ->{
                        postGenerateToken(result)
                        imageToken.visibility = View.VISIBLE
                    }

                    else -> {
                        val msg = getString(R.string.message_pay)
                        message(msg)
                    }

                }

                progress.visibility = View.GONE
            }
        }

        val params = arrayOf<String>()
        task.execute(*params)
    }

    private fun message(msg:String){



        val builder = MaterialAlertDialogBuilder(this)
        builder.setTitle(getString(R.string.title_message_pay))
        builder.setMessage(msg)

        builder.setPositiveButton(getString(R.string.ok)){_, _ ->
            onBackPressed()
        }

        val dialog = builder.create()
        dialog.show()

    }

    fun setLogo() {
        Log.d("GETCODE", "preGenerateToken")
        // use CardNetwork to determine which image to use
        val selectedLogo = when (cardNetwork) {
            "AMEX" -> amexCardLogo
            "MSTR" -> mcCardLogo
            "VISA" -> visaCardLogo
            "DCVR" -> discoverCardLogo
            else -> otherCardLogo
        }

        val resourceId = resources.getIdentifier(selectedLogo, "drawable", packageName)

        val drawable = resources.getDrawable(resourceId)
        logoImage.setImageDrawable(drawable)
        logoImage.visibility = View.VISIBLE
    }

    private fun generateQrCode(): Result {
        Log.d("GETCODE", "generateQrCode")
        val result = Result()
        result.responseCode = responseCode
        result.payToken = payToken
        result.cardNetwork = cardNetwork
        result.cardLast4 = cardLast4

        if(result.responseCode == 100){
            try {
                val width = 450
                val height = 450
                val qwriter = QRCodeWriter()
                val qrcode = qwriter.encode(result.payToken, BarcodeFormat.QR_CODE, width, height)
                Log.d("GETCODE", "Generate BMP")
                result.bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)

                for (x in 0 until width) {
                    for (y in 0 until height) {
                        result.bmp!!.setPixel(x, y, if (qrcode.get(x, y)) Color.BLACK else Color.WHITE)
                    }
                }
                Log.d("GETCODE", "Done copying bitmap")
            } catch (e: WriterException) {
                // TODO
                Log.d("GETCODE", "QR Writer failed " + e.localizedMessage)
            }
        }
        return result
    }

    private fun postGenerateToken(result: Result) {
        Log.d("GETCODE", "postGenerateToken")
        Log.d("GETCODE", "Setting token to " + result.payToken!!)
        textToken.text = result.payToken
        imageToken.setImageBitmap(result.bmp)
    }

    fun generateToken() {
        // Get the token if it was passed as an Intent extra
        val extras = intent.extras
        if (extras != null) {
            val payToken = extras.getString("PayToken")
            val cardNetwork = extras.getString("CardNetwork")
            val cardLast4 = extras.getString("CardLast4")


            val resources = resources
            Log.d("PACKAGE", packageName)
            val resourceId = resources.getIdentifier(amexCardLogo, "drawable", packageName)
            val drawable = resources.getDrawable(resourceId)
            logoImage.setImageDrawable(drawable)

            try {
                val width = 450
                val height = 450
                val qwriter = QRCodeWriter()
                val qrcode = qwriter.encode(payToken!!, BarcodeFormat.QR_CODE, width, height)

                val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)

                for (x in 0 until width) {
                    for (y in 0 until height) {
                        bmp.setPixel(x, y, if (qrcode.get(x, y)) Color.BLACK else Color.WHITE)
                    }
                }

                textToken.text = payToken
                imageToken.setImageBitmap(bmp)
            } catch (e: WriterException) {
                // TODO: Implement

            }

        } else {
            // TODO: Implement

        }

    }

    fun getToken() {

        val http = JupiterNewHttp(this,"/getCode")
        http.setOnSuccess {
            Log.d("Success",it.toString())
            cardNetwork = it.optString("cardNetwork", "")
            cardLast4 = it.optString("cardLast4", "")
            payToken = if (it.has("token")) Utils.formatToken(it.getString("token")) else ""
        }

        http.setOnFailure {
            responseCode = it!!.getInt("responseCode")
        }

        http.setOnConnectFailure {
            responseCode = 0
        }


        val requestParams = ArrayList<Pair<String, String>>(3)
        requestParams.add(Pair("AccessToken",user2!!.token.toString()))
        requestParams.add(Pair("userId",user2!!.userId.toString()))
        http.doApiCall(requestParams)

    }

}
