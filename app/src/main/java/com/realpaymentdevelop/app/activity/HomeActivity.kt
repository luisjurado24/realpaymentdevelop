package com.realpaymentdevelop.app.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.activity.order.ConfirmOrderActivity
import com.realpaymentdevelop.app.db.DataBase
import com.realpaymentdevelop.app.fragment.CreditFragment
import com.realpaymentdevelop.app.fragment.HistoryFragment
import com.realpaymentdevelop.app.fragment.HomeFragment
import com.realpaymentdevelop.app.fragment.RewardsFragment
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.Utils
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : JupiterNewActivity() {
    var userID: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpContext(this)
        makeUser()
        setContentView(R.layout.activity_home)
        title = resources.getString(R.string.title_activity_home)
        bnv_home.setOnNavigationItemSelectedListener(navListener)
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_conteiner_home, HomeFragment()).commit()
    }

    private val navListener: BottomNavigationView.OnNavigationItemSelectedListener =
            BottomNavigationView.OnNavigationItemSelectedListener { item ->
                var selectedFragment: Fragment
                when (item.itemId) {
                    R.id.nav_home -> {
                        selectedFragment = HomeFragment()
                        supportFragmentManager.beginTransaction()
                                .replace(R.id.fragment_conteiner_home, selectedFragment, "aa")
                                .addToBackStack("home").commit()
                        true
                    }
                    R.id.nav_store -> {
                        selectedFragment = RewardsFragment()
                        supportFragmentManager.beginTransaction()
                                .replace(R.id.fragment_conteiner_home, selectedFragment)
                                .addToBackStack("store").commit()
                        true
                    }
                    R.id.nav_credit -> {
                        selectedFragment = CreditFragment()
                        supportFragmentManager.beginTransaction()
                                .replace(R.id.fragment_conteiner_home, selectedFragment)
                                .addToBackStack("credit").commit()
                        true
                    }
                    R.id.nav_history -> {
                        val fragment = HistoryFragment()
                        val bundle = Bundle()
                        bundle.putString(resources.getString(R.string.key_token), token)
                        bundle.putString("userId", userID)
                        fragment.arguments = bundle
                        supportFragmentManager.beginTransaction()
                                .replace(R.id.fragment_conteiner_home, fragment)
                                .addToBackStack("order").commit()
                        true
                    }
                    else -> false
                }
            }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_qr_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_qr -> {
                Utils.launch(this, CreditCardActivity::class.java)
            }
        }
        return true
    }

    override fun onBackPressed() {
        if (backPressed + 2000 > System.currentTimeMillis()) {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        } else {
            Toast.makeText(this, resources.getString(R.string.tap_to_exit), Toast.LENGTH_SHORT)
                    .show()
        }
        backPressed = System.currentTimeMillis()
    }

    companion object {
        private var backPressed: Long = 0
    }


}
