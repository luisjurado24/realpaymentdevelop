package com.realpaymentdevelop.app.activity

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.utils.ApiResult
import kotlinx.android.synthetic.main.activity_up_sign.*
import java.util.*


class SignUpActivity : JupiterActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpTheme()
        setUpContext(this)
        setContentView(R.layout.activity_up_sign)
        title = resources.getString(R.string.sign_up)
    }

    private fun setUpTheme() {
        val configuration = resources.configuration
        val currentNightMode = configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        when (currentNightMode) {
            Configuration.UI_MODE_NIGHT_NO -> setTheme(R.style.Light)
            Configuration.UI_MODE_NIGHT_YES -> setTheme(R.style.DayNight)
        }
    }

    override fun onResume() {
        super.onResume()
        btn_up_sign_doRegister.setOnClickListener {
            val nameBoolean = til_up_sign_name.editText!!.text.isNullOrEmpty()
            val phoneBoolean = til_up_sign_phone_number.editText!!.text.isNullOrEmpty()

            if (nameBoolean or phoneBoolean) {
                if (nameBoolean) til_up_sign_name.error =
                    resources.getString(R.string.error_invalid_username)
                else til_up_sign_name.error = null
                if (phoneBoolean) til_up_sign_phone_number.error =
                    resources.getString(R.string.error_phone_number)
                else til_up_sign_phone_number.error = null
            } else {

                val params = ArrayList<Pair<String, String>>()
                params.add(Pair("phone", til_up_sign_phone_number.editText!!.text.toString()))
                params.add(
                    Pair("name", til_up_sign_name.editText!!.text.toString())
                )

                doAsyncTask("/registerUserByPhone", params)

            }
        }
    }

    override fun doFinalCallback(result: ApiResult?) {

        val phoneNumber = til_up_sign_phone_number.editText!!.text.toString()
        val intentConfirm = Intent(this, ConfirmMessageActivity::class.java)
        intentConfirm.putExtra(getString(R.string.phone_extra), phoneNumber)
        intentConfirm.putExtra(getString(R.string.key_title), false)
        startActivity(intentConfirm)
        finish()
    }

}