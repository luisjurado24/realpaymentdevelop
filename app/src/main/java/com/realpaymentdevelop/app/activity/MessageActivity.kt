package com.realpaymentdevelop.app.activity

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.AdapterMessage
import com.realpaymentdevelop.app.customviews.ExtendableListView
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import com.realpaymentdevelop.app.utils.Utils
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class MessageActivity : JupiterNewActivity() {
    private var APIKEY: String? = null
    private var HOSTNAME: String? = null
    private var TOKEN: String? = null
    private var USERID: String? = null
    private var messages: JSONArray = JSONArray()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setUpTheme()
        setUpContext(this)
        setContentView(R.layout.activity_message)
        title = resources.getString(R.string.title_activity_messages)
        APIKEY = resources.getString(R.string.apikey)
        HOSTNAME = resources.getString(R.string.hostname)
        val token = config["token", "undefined"]
        if (token !== "undefined") {
            makeUser()
        } else {
            config["remember"] = "false"
            config["token"] = "undefined"
            Utils.launch(this, HomeActivity::class.java)
        }
    }


    override fun onResume() {
        super.onResume()
        getMessages()
    }


    override fun makeUser() {
        val serialLogin = config["login", "undefined"]
        if (serialLogin !== "undefined") {
            try {
                val user = JSONObject(serialLogin)
                TOKEN = user.getString("token")
                USERID = user.getString("userId")
            } catch (e: JSONException) {
            }
        } else {
            config["login"] = "undefined"
            config["remember"] = "false"
            config["token"] = "undefined"
            Utils.launch(this, HomeActivity::class.java)
        }
    }


    /**
     * Make the api call
     */
    private fun getMessages() {
        val params = ArrayList<Pair<String, String>>()
        params.add(Pair("ApiKey", APIKEY!!))
        params.add(Pair("AccessToken", TOKEN!!))
        params.add(Pair("userId", USERID!!))
        val http = JupiterNewHttp(this, "/getMessages")
        Log.d("TAG", params.toString())
        http.setOnSuccess { it: JSONObject ->
            Log.d("TAG", " Success ")
            try {
                messages = it.getJSONArray("body")
                if (messages.length() > 0) {
                    displayMessages()
                } else {
                    runOnUiThread {
                        Toast.makeText(this, getString(R.string.not_elements), Toast.LENGTH_SHORT).show()
                    }
                }
            } catch (e: JSONException) {
                runOnUiThread {
                    Toast.makeText(
                        context,
                        getString(R.string.error_an_occurred),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            null
        }
        http.setOnFailure { it: JSONObject? ->
            Log.d("Messages", "Failure")
            null
        }
        http.setOnConnectFailure { it: JSONObject? ->
            val msg = getString(R.string.unfortunately_timeout_message)
            runOnUiThread {
                errorAlert(this, "$title", msg) { this.onBackPressed() }
            }
        }
        http.doAsyncApiCallMenu(params)
    }

    private fun displayMessages() {
        runOnUiThread {
            try {
                val adapter = AdapterMessage(context, messages)
                val listMessages: ExtendableListView = findViewById(R.id.activity_reycler_view_message)
                //registerForContextMenu(listMessages)
                listMessages.adapter = adapter
                listMessages.onItemClickListener =
                    AdapterView.OnItemClickListener { adapterView, view, position, l ->
                        try {
                            showMessage(messages.getJSONObject(position))
                        } catch (e: JSONException) {
                            Toast.makeText(
                                context,
                                "We are sorry an error has occurred",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            } catch (e: Exception) {
                runOnUiThread {
                    Toast.makeText(
                        context,
                        "We are sorry don't have Messages to show",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }


    private fun showMessage(message: JSONObject) {
        try {
            val messageContainer = message.getJSONObject("Message")
            val imageContainer = message.optJSONObject("ContentImage")
            val title = messageContainer.optString("title")
            val text = messageContainer.optString("text")
            val read = messageContainer.optBoolean("read")
            val messageId = messageContainer.optString("messageId")
            val subTitle = messageContainer.optString("subtitle")
            val date = messageContainer.optString("created")
            val actionButtonText = messageContainer.optString("actionButtonText")
            val actionButtonUrl = messageContainer.optString("actionUrl")

            val messageIntent = Intent(context, MessageDetailsActivity::class.java)
            if (text.isNotEmpty()) {
                messageIntent.putExtra(resources.getString(R.string.key_message), text)
            }
            if (messageId.isNotEmpty()) {
                messageIntent.putExtra(resources.getString(R.string.key_id_message), messageId)
            }
            if (title.isNotEmpty()) {
                messageIntent.putExtra(resources.getString(R.string.key_title), title)
            }
            if (subTitle.isNotEmpty()) {
                messageIntent.putExtra(resources.getString(R.string.key_subtitle), subTitle)
            }
            if (actionButtonText.isNotEmpty()) {
                messageIntent.putExtra(resources.getString(R.string.key_button), actionButtonText)
            }
            if (read) {
                messageIntent.putExtra(resources.getString(R.string.key_boolean), read)
            }
            if (actionButtonUrl.isNotEmpty()) {
                messageIntent.putExtra(resources.getString(R.string.key_button_url), actionButtonUrl)
            }

            messageIntent.putExtra(resources.getString(R.string.key_date), date)
            if (imageContainer != null) {
                var path = imageContainer.optString("path")
                val name = imageContainer.optString("name")
                if (name.isNotEmpty()) {
                    if (path.isEmpty()) {
                        path = "/"
                    }
                    val host = resources.getString(R.string.static_hostname)
                    messageIntent.putExtra(resources.getString(R.string.key_url_image),
                        host + path + name
                    )
                }
            }
            startActivity(messageIntent)
        } catch (e: JSONException) {
            Log.d("Message", e.localizedMessage)
        }
    }

}