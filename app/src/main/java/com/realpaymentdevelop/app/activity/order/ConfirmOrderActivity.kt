package com.realpaymentdevelop.app.activity.order

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.order.AdapterListTopic
import com.realpaymentdevelop.app.db.DataBase
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.interfaces.SendDataToRemoveOrEdit
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.Config
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import com.realpaymentdevelop.app.utils.NewPublicKey
import com.realpaymentdevelop.app.utils.NewRPEncrypter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_confirm_order.*
import kotlinx.android.synthetic.main.tip_pay_layout.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat
import kotlin.math.floor

class ConfirmOrderActivity : JupiterNewActivity(), SendDataToRemoveOrEdit {
    private val TAG = "CartFrag"
    private var subTotal = 0.00
    private var tax = 0.00
    private var tip = 0.00
    private var total = 0.00
    private var tipEnable = 0
    private var arrayJson: ArrayList<JsonObject> = ArrayList()
    private var jsonDelivery: JsonObject = JsonObject()
    private var idMerchant: String? = ""
    private var idLocation: String? = ""
    private var tipSelected = 0.20
    private var adapter: AdapterListTopic? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_order)

        title = resources.getString(R.string.title_activity_confirm_order)
        idMerchant = intent.getStringExtra(getString(R.string.key_merchants))
        idLocation = intent.getStringExtra(getString(R.string.key_localization))
        val jsonParser = JsonParser()
        val db = DataBase(this)
        arrayJson = db.getCart(idMerchant!!, idLocation!!)

        if (arrayJson.size > 0) {
            makeUser()
            val jsonLocalization = db.localizationCart(idLocation!!, idMerchant!!)

            val stringDelivery = jsonLocalization.getString("jsonDelivery")
            jsonDelivery = jsonParser.parse(stringDelivery).asJsonObject
            tipEnable = jsonLocalization.getJSONObject("jsonDelivery").getJSONObject("location")
                .getJSONObject("QuickMerchantLocation").getString("tipEnabled").toInt()
            location_title.text = jsonLocalization.getString("nameLocation")
            var merchant = jsonLocalization.getString("merchantLocation")
            val spliLocalization = merchant.split(" ")
            if (spliLocalization?.size!! > 3) {
                merchant = ""
                spliLocalization.forEachIndexed { index, s ->
                    merchant += if (index % 3 == 0) {
                        "$s\n"
                    } else {
                        "$s "
                    }
                }
            }
            merchant_title.text = merchant
            val urlImage = jsonLocalization.getString("urlImageMerchant")

            Picasso.with(this).load(urlImage)
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(iv_review_order_logo)
            getOrders(arrayJson)
            getDeliveryInfo(jsonDelivery)
            twenty_percent.isChecked = true
            tipSelected = 0.20
            setValuesTotalTaxTip(arrayJson, tipEnable)
        } else {
            errorAlert(this, "$title", getString(R.string.not_elements)) { onBackPressed() }
        }

    }

    private fun buildJson(payloadBase: String): JSONObject {
        val json: JSONObject = JSONObject()
        val db = DataBase(this)
        val localization = db.localizationCart(idLocation!!, idMerchant!!)
        val merchantId = localization.getString("idLocation")
        val merchantLocationId = localization.getString("idMerchantLocation")
        val config = Config(this)
        val accessToken = config.get("token", "undefined")

        var arrayItems = JSONArray()
        arrayJson.forEach {
            val itemId = it.get("Item").asJsonObject.get("quickItemId").asInt
            val quantity = it.get("Quantity").asInt
            val extras = it.get("Extras").asJsonArray
            val extrasId: JsonArray = JsonArray()
            if (extras.size() > 0) {
                extras.forEachIndexed { index, jsonElement ->
                    val extaIntern = jsonElement.asJsonObject
                    extrasId.add(extaIntern.get("quickModifierId").asInt)
                }
            }
            val items =
                "{\"quickItemId\":$itemId,\"modifiers\":$extrasId,\"quantity\":$quantity}"  //String with format
            val jsonForItem = JSONObject(items)
            arrayItems.put(jsonForItem)
        }

        val mf = DecimalFormat("0.00")
        val subtotal = subtotal_text.text.subSequence(1, subtotal_text.text.length)
        json.apply {
            put("AccessToken", accessToken)
            put("merchantId", merchantLocationId)
            put("merchantLocationId", merchantId)
            put("items", changeToBaseSixtyFour(arrayItems.toString()))
            put(
                "quickPickupSpotId",
                if (jsonDelivery.get("type").asInt == 1) jsonDelivery.get("pickupSpot").asJsonObject
                    .get("quickPickupSpotId").asInt else ""
            )
            put(
                "buildingPickupDescription",
                if (jsonDelivery.get("type").asInt == 3) jsonDelivery.get("fullDelivery").asJsonObject
                    .get("building").asJsonObject
                    .get("buildingPickupDescription").asString else ""
            )
            put(
                "deliveryAddress1",
                if (jsonDelivery.get("type").asInt == 2) jsonDelivery.get("fullDelivery").asJsonObject
                    .get("address").asJsonObject.get("deliveryAddress1").asString else ""

            )
            put(
                "deliveryAddress2",
                if (jsonDelivery.get("type").asInt == 2) jsonDelivery.get("fullDelivery").asJsonObject
                    .get("address").asJsonObject.get("deliveryAddress2").asString else ""
            )
            put(
                "deliveryCity",
                if (jsonDelivery.get("type").asInt == 2) jsonDelivery.get("fullDelivery").asJsonObject
                    .get("address").asJsonObject.get("deliveryCity").asString else ""
            )
            put(
                "deliveryState",
                if (jsonDelivery.get("type").asInt == 2) jsonDelivery.get("fullDelivery").asJsonObject
                    .get("address").asJsonObject.get("deliveryState").asString else ""
            )
            put(
                "deliveryZipCode",
                if (jsonDelivery.get("type").asInt == 2) jsonDelivery.get("fullDelivery").asJsonObject
                    .get("address").asJsonObject.get("deliveryZipCode").asString else ""
            )
            put(
                "deliveryCountry",
                if (jsonDelivery.get("type").asInt == 2) jsonDelivery.get("fullDelivery").asJsonObject
                    .get("address").asJsonObject.get("deliveryCountry").asString else ""
            )
            put("tax", mf.format(tax).toDouble())
            put("tip", mf.format(tip).toDouble())
            if (message.text.isEmpty()) {
                put("orderNote", "")
            } else {
                put("orderNote", message.text.toString())
            }
            put("Total", total.toDouble())
            put("payload", payloadBase)

            put("subTotal", "$subtotal".toDouble())
        }
        return json
    }

    private fun getOrders(cartContent: ArrayList<JsonObject>) {
        val zero = "$0.00"
        adapter = AdapterListTopic(this, cartContent, this)
        cart_list.adapter = adapter
        var subTotalAux = 0.00
        var subTotal = 0.00
        var taxAux = 0.00
        var tip = 0.0
        val tipSelected = 0.0
        val mf = DecimalFormat("0.00")
        if (tipEnable != 1) {
            tips.visibility = View.GONE
            list_tips.visibility = View.GONE
            amount.visibility = View.GONE
            custom_amount.visibility = View.GONE
            tip_field.visibility = View.GONE
            tip_text.visibility = View.GONE
        }
        cartContent.forEachIndexed { index, jsonObject ->
            subTotalAux += jsonObject.get("TotalCost").asDouble
            val taxForItem = jsonObject.get("Item").asJsonObject.get("taxRate")
            if (taxForItem.toString() != "null") {
                taxAux += (jsonObject.get("Item").asJsonObject.get("taxRate").asDouble / 100) * jsonObject.get(
                    "TotalCost"
                ).asDouble
            } else {
                taxAux += 0.00
            }
        }
        subTotal = Math.floor(subTotalAux * 100) / 100
        subtotal_text.text = "$${mf.format(subTotal)}"
        val tax = Math.floor(taxAux * 100) / 100
        tax_text.text = "$${mf.format(tax)}"

        when (tipEnable) {
            1 -> {
                if (custom_amount.text.toString() == "") {
                    tip = Math.floor((subTotal * tipSelected) * 100) / 100
                }
                tip_text.text = "$${mf.format(tip)}"
                custom_amount.hint = "$${mf.format(tip)}"
            }
            else -> tip = 0.0
        }
        total = floor((subTotal + tax + tip) * 100) / 100
        total_text.text = "$${mf.format(total)}"
        checkout.isEnabled = true
    }


    private fun getDeliveryInfo(deliveryInformation: JsonObject) {
        try {

            val pickupCounter =
                deliveryInformation.get("delivery").asJsonObject.get("pickupCounter").asBoolean
            val pickupSpot =
                deliveryInformation.get("delivery").asJsonObject.get("pickupSpot").asBoolean
            val buildingDelivery =
                deliveryInformation.get("delivery").asJsonObject.get("buildingDelivery").asBoolean
            val fullDelivery =
                deliveryInformation.get("delivery").asJsonObject.get("fullDelivery").asBoolean

            when (deliveryInformation.get("type").asInt) {
                0 -> {


                    if (pickupCounter and !(pickupSpot || buildingDelivery || fullDelivery)) {
                        delivery_title.text = getString(R.string.delivery_title_0)
                        delivery.text = getString(R.string.delivery_subtitle_0)
                    } else {
                        delivery_title.visibility = View.INVISIBLE
                        delivery.visibility = View.INVISIBLE
                        line1.visibility = View.INVISIBLE
                    }
                }
                1 -> {
                    val name =
                        deliveryInformation.get("pickupSpot").asJsonObject.get("displayName").asString
                    delivery_title.text = getString(R.string.delivery_title_1)
                    delivery.text = name
                }
                2 -> {
                    val fullDelivery =
                        deliveryInformation.get("fullDelivery").asJsonObject.get("address").asJsonObject
                    val address1 = fullDelivery.get("deliveryAddress1").asString
                    val address2 = fullDelivery.get("deliveryAddress2").asString
                    val city = fullDelivery.get("deliveryCity").asString
                    val state = fullDelivery.get("deliveryState").asString
                    val zip = fullDelivery.get("deliveryZipCode").asString
                    delivery_title.text = getString(R.string.delivery_title_2)
                    when {
                        address1 == "null" -> {
                            delivery.text = "$address2, $city,\n$state, $zip."
                        }
                        address2 == "null" -> {
                            delivery.text = "$address1, $city,\n$state, $zip."
                        }
                        else -> {
                            delivery.text = "$address1,\n$address2,$city,\n$state, $zip."
                        }
                    }
                }
                3 -> {
                    val building =
                        deliveryInformation.get("fullDelivery").asJsonObject.get("building").asJsonObject
                    val office = building.get("buildingPickupDescription").asString
                    delivery_title.text = getString(R.string.delivery_title_3)
                    delivery.text = office
                }
                else -> {
                }
            }

        } catch (e: Exception) {
            Log.d(TAG, e.toString())
        }
    }

    private fun setValuesTotalTaxTip(cartContent: ArrayList<JsonObject>, tipEnable: Int) {
        var subTotalAux = 0.00
        var taxAux = 0.00
        val mf = DecimalFormat("0.00")
        Log.d("cartContent", cartContent.toString())
        cartContent.forEachIndexed { index, jsonObject ->
            subTotalAux += jsonObject.get("TotalCost").asDouble
            val taxForItem = jsonObject.get("Item").asJsonObject.get("taxRate")
            taxAux += if (taxForItem.toString() != "null") {
                (jsonObject.get("Item").asJsonObject.get("taxRate").asDouble / 100) * jsonObject.get(
                    "TotalCost"
                ).asDouble
            } else {
                0.00
            }
        }
        subTotal = Math.floor(subTotalAux * 100) / 100
        subtotal_text.text = "$${mf.format(subTotal)}"
        tax = Math.floor(taxAux * 100) / 100
        tax_text.text = "$${mf.format(tax)}"

        when (tipEnable) {
            1 -> {
                if (custom_amount.text.toString() == "") {
                    tip = Math.floor((subTotal * tipSelected) * 100) / 100
                }
                tip_text.text = "$${mf.format(tip)}"
                custom_amount.hint = "$${mf.format(tip)}"
            }
            else -> tip = 0.0
        }
        total = Math.floor((subTotal + tax + tip) * 100) / 100
        total_text.text = "$${mf.format(total)}"

        checkout.isEnabled = true
    }

    private fun deleteMessage(message: String) {
        val builder = MaterialAlertDialogBuilder(this)
        builder.setTitle(getString(R.string.delet_confirm))
            .setMessage(message)
            .setNegativeButton(getString(android.R.string.cancel)) { dialog, id ->
                dialog.dismiss()
            }
            .setPositiveButton(android.R.string.yes) { dialog, id ->
                val baseData = DataBase(this)
                baseData.deleteAll(idMerchant!!, idLocation!!)
                onBackPressed()
            }
        val alert = builder.create()
        alert.show()
    }

    override fun onResume() {
        super.onResume()
        radioGroup.setOnCheckedChangeListener { radioGroup, radioButtonID ->
            val selectedRadioButton = radioGroup.findViewById<RadioButton>(radioButtonID)
            when (selectedRadioButton.id) {
                R.id.none -> {
                    tipSelected = 0.0
                    custom_amount.text.clear()
                    setValuesTotalTaxTip(arrayJson, tipEnable)
                }
                R.id.fifteen_percent -> {
                    tipSelected = 0.15
                    custom_amount.text.clear()
                    setValuesTotalTaxTip(arrayJson, tipEnable)
                }
                R.id.twenty_percent -> {
                    tipSelected = 0.20
                    custom_amount.text.clear()
                    setValuesTotalTaxTip(arrayJson, tipEnable)
                }
                R.id.twenty_five_percent -> {
                    tipSelected = 0.25
                    custom_amount.text.clear()
                    setValuesTotalTaxTip(arrayJson, tipEnable)
                }
            }
        }

        text_remove_all.setOnClickListener {
            deleteMessage(getString(R.string.delete_cuestion))
        }
        custom_amount.addTextChangedListener(
            object : TextWatcher {
                var aux = "0"
                var startWithPoint = false
                override fun afterTextChanged(p0: Editable?) {
                    if (!p0.isNullOrEmpty()) {
                        when (startWithPoint) {
                            true -> {
                                val final = aux + p0
                                if (final != "0.") {
                                    tip = final.toDouble()
                                }
                            }
                            false -> tip = p0.toString().toDouble()
                        }
                        setValuesTotalTaxTip(arrayJson, tipEnable)
                    } else {
                        tip = floor((subTotal * tipSelected) * 100) / 100
                        setValuesTotalTaxTip(arrayJson, tipEnable)
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    val filterArray = arrayOfNulls<InputFilter>(1)
                    if (!p0.isNullOrEmpty()) {
                        startWithPoint = (p0[0] == '.')
                        if (p0.contains('.')) {
                            filterArray[0] = InputFilter.LengthFilter(p0.indexOf('.') + 3)
                            custom_amount.filters = filterArray
                        } else {
                            filterArray[0] = InputFilter.LengthFilter(6)
                            custom_amount.filters = filterArray
                        }
                    }
                }

            })

        checkout.setOnClickListener {
            val intent = Intent(this, PayOrderActivity::class.java)
            startActivityForResult(intent, 12)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((requestCode == 12) and (resultCode == RESULT_OK)) {
            val result = data!!.extras!!.getInt("aa")
            val payload = if (result == 1234) {
                data.extras!!.getString(getString(R.string.key_card))
            } else {
                data.extras!!.getString(getString(R.string.key_card))
            }
            val payloadJSON = JSONObject(payload)
            val creditCardBase = payloadCreate(payloadJSON)
            val json = buildJson(creditCardBase)
            sendOrder(json)

        }
    }

    private fun sendOrder(json: JSONObject) {
        val http = JupiterNewHttp(this, "/quickOrderApi", "/createOrder")
        http.setOnSuccess {
            Log.d("success1", it.toString())
            if (it.getString("success") == "1") {
                val time = it.getJSONObject("quickOrderCreate").getJSONObject("order")
                    .getJSONObject("QuickMerchantLocation").getString("waitTime")
                val phone = it.getJSONObject("quickOrderCreate").getJSONObject("order")
                    .getJSONObject("QuickMerchantLocation").getString("phoneNumber")
                val orderId = it.getJSONObject("quickOrderCreate").getJSONObject("order")
                    .getJSONObject("QuickOrder").getString("externalOrderId")
                runOnUiThread {
                    errorAlert(this, "$title", "time:$time\nphone:$phone\norderId:$orderId") {
                        onBackPressed()
                    }
                }
            }
        }

        http.setOnFailure {
            Log.d("Card2", it.toString())
            runOnUiThread {
                errorAlert(
                    this,
                    "$title",
                    "${it!!.getString("message")}\n ${getString(R.string.error_an_occurred)}"
                ) {
                    onBackPressed()
                }
            }


        }
        http.setOnConnectFailure {
            Log.d("Card3", it.toString())
            runOnUiThread {
                errorAlert(this, "$title", "${getString(R.string.network_connection_not_found)}") {
                    onBackPressed()
                }
            }
        }
        val requestParams = ArrayList<Pair<String, String>>(9)
        Log.e("FinalJsonPay", "$json")
        requestParams.add(Pair("AccessToken", json.getString("AccessToken")))
        requestParams.add(Pair("merchantId", json.getString("merchantId")))
        requestParams.add(Pair("merchantLocationId", json.getString("merchantLocationId")))
        requestParams.add(Pair("items", json.getString("items")))
        requestParams.add(Pair("tax", json.getString("tax")))
        requestParams.add(Pair("tip", json.getString("tip")))
        requestParams.add(Pair("payment", json.getString("payload")))
        requestParams.add(Pair("orderNote", json.getString("orderNote")))
        requestParams.add(Pair("saveCardOnFile", "false"))
        requestParams.add(Pair("subTotal", json.getString("subTotal")))
        requestParams.add(Pair("total", json.getString("Total")))

        //Delivery

        requestParams.add(Pair("quickPickupSpotId", json.getString("quickPickupSpotId")))
        requestParams.add(
            Pair(
                "buildingPickupDescription",
                json.getString("buildingPickupDescription")
            )
        )
        requestParams.add(Pair("deliveryAddress1", json.getString("deliveryAddress1")))
        requestParams.add(Pair("deliveryAddress2", json.getString("deliveryAddress2")))
        requestParams.add(Pair("deliveryCity", json.getString("deliveryCity")))
        requestParams.add(Pair("deliveryState", json.getString("deliveryState")))
        requestParams.add(Pair("deliveryZipCode", json.getString("deliveryZipCode")))
        requestParams.add(Pair("deliveryCountry", json.getString("deliveryCountry")))
        http.doAsyncApiCallMenu(requestParams)
    }


    private fun getKey(): String? {
        val http = NewPublicKey(this, "/miscApi", "/publicKey")
        var key: String? = null
        http.setOnSuccess {
            key = it
        }
        http.setOnFailure {
            key = null
        }
        http.doAsyncApiCall()
        return key
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }

    override fun onBackPressed() {
        finish()
    }

    private fun changeToBaseSixtyFour(stringToEncode: String): String {
        return Base64.encodeToString(stringToEncode.toByteArray(), Base64.DEFAULT)
    }

    private fun payloadCreate(payload: JSONObject): String {
        val publicKey = getKey()
        val encrypt = NewRPEncrypter()
        val jsonCreditCard = JSONObject()

        if (payload.has("cardId")) {
            val cardId = encrypt.newEncrypt(publicKey!!, payload.getString("cardId"))
            val code = encrypt.newEncrypt(publicKey, payload.getString("code"))
            jsonCreditCard.put("cardId", cardId)
            jsonCreditCard.put("code", code)
        } else {
            val keys = payload.keys()
            keys.forEach {
                val text = encrypt.newEncrypt(publicKey!!, payload.getString(it) as String)
                jsonCreditCard.put(payload.getString(it), text)
            }
        }
        return changeToBaseSixtyFour(jsonCreditCard.toString())
    }


    override fun setValuesOnExpressOrder() {
        val db = DataBase(this)
        val arrayJson = db.getCart(idMerchant!!, idLocation!!)
        if (arrayJson.size > 0) {
            getOrders(arrayJson)
        } else {
            errorAlert(this, "$title", getString(R.string.not_elements)) { onBackPressed() }
        }
    }

    override fun editItem(item: JSONObject) {

    }


}