package com.realpaymentdevelop.app.activity

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.Config
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import com.realpaymentdevelop.app.utils.Utils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_perfil.*
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.lang.Exception

class PerfilActivity : JupiterNewActivity() {

    lateinit var currentPhotoPath: String
    private var photoName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setUpTheme()
        setContentView(R.layout.activity_perfil)
        title = resources.getString(R.string.title_activity_perfil)
        currentPhotoPath = ""
        val config = Config(this)
        user_name.text = config.name
        val image = civ_home_profile_activity_image_user
        Picasso.with(this).load(config.avatarURL).into(image)
    }

    override fun onResume() {
        super.onResume()
        swm_perfil_swich_activity.isChecked = config.isAutoLogoutEnabled
        swm_perfil_swich_activity.setOnClickListener(View.OnClickListener {
            config.isAutoLogoutEnabled = swm_perfil_swich_activity.isChecked
        })

        btn_perfil_activity_logout.setOnClickListener {
            finishAndRemoveTask()
            val config = Config(this)
            config.rememberMe = false
            config.avatarURL = null
            Utils.launch(this, LogInActivity::class.java)

        }
        civ_home_profile_activity_image_user.setOnClickListener {
            val options =
                arrayOf(resources.getString(R.string.camara), resources.getString(R.string.gallery))
            val materialAlertDialogBuilder = MaterialAlertDialogBuilder(this)
            materialAlertDialogBuilder.setItems(options) { _, i ->
                when (options[i]) {
                    resources.getString(R.string.camara) -> {
                        selectedCamera()
                    }
                    resources.getString(R.string.gallery) -> {
                        selectedGallery()
                    }
                }
            }
            materialAlertDialogBuilder.setNegativeButton(resources.getString(R.string.cancel)) { _, _ ->
            }
            val alertDialog = materialAlertDialogBuilder.create()
            alertDialog.show()
        }
    }

    private fun selectedGallery() {
        if (checkStatusPermissionGallery()) {
            val intent = Intent(Intent.ACTION_PICK).apply { type = "image/png" }
            if (intent.resolveActivity(packageManager) != null) {
                startActivityForResult(
                    Intent.createChooser(
                        intent,
                        resources.getString(R.string.gallery)
                    ), Companion.REQUEST_IMAGE_GET
                )
            }
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf<String>(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                Companion.REQUEST_PERMISSION_GALLERY
            )
        }
    }

    private fun selectedCamera() {
        if (!checkStatusPermissionCamara()) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf<String>(android.Manifest.permission.CAMERA),
                Companion.REQUEST_PERMISSION_GALLERY
            )
        } else {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(packageManager)?.also {
                    val photoFile = try {
                        createImageFile()
                    } catch (ex: IOException) {
                        Log.e("E", ex.toString())
                        null
                    }
                    if (photoFile != null) {
                        currentPhotoPath = photoFile.path
                    }
                    // Continue only if the File was successfully created
                    photoFile?.also {
                        val photoURI: Uri =
                            FileProvider.getUriForFile(this, "com.realpaymentdevelop.app", it)
                        photoName = it.name

                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                    }
                }
            }
        }
    }


    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile("profilepic", ".jpg", storageDir).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            when (resultCode) {
                RESULT_OK -> {
                    try {
                        galleryAddPic()
                        uploadProfilePhotoCamera(currentPhotoPath)
                    } catch (e: Exception) {
                        errorAlert(this, "$title", getString(R.string.error_an_occurred)) {}
                    }

                }
                RESULT_CANCELED -> {
                    Toast.makeText(this, getString(R.string.error_an_occurred), Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
        if (requestCode == Companion.REQUEST_IMAGE_GET) {
            when (resultCode) {
                RESULT_OK -> {
                    val uri: Uri? = data!!.data
                    val projection = arrayOf(MediaStore.Images.Media.DATA)
                    val cursor: Cursor? = contentResolver.query(uri!!, projection, null, null, null)
                    cursor?.moveToFirst()
                    val columnIndex: Int? = cursor?.getColumnIndex(projection[0]) ?: 60
                    val path: String? = columnIndex?.let { cursor?.getString(it) }
                    Log.e("TAG", "$path")
                    uploadProfilePhotoCamera(path!!)
                }
                RESULT_CANCELED -> {
                    Toast.makeText(this, getString(R.string.error_an_occurred), Toast.LENGTH_SHORT)
                        .show()
                }
            }

        }
    }

    private fun galleryAddPic() {
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            val f = File(currentPhotoPath)
            mediaScanIntent.data = Uri.fromFile(f)
            sendBroadcast(mediaScanIntent)
        }
    }


    private fun uploadProfilePhotoCamera(current: String) {
        var token = ""
        var userId = ""

        runOnUiThread {
            val dialog = MaterialAlertDialogBuilder(this)
            dialog.setTitle(getString(R.string.processing))
            dialog.setMessage(getString(R.string.please_wait))
            dialog.setCancelable(false)
            val dialogCreate = dialog.create()
            dialogCreate.show()
            val jupiter = JupiterNewHttp(this, "/uploadProfilePhoto")
            jupiter.setOnFailure {
                Log.e("PerfilAcConnectFail", it.toString())
                if (it != null) {
                    dialogCreate.dismiss()
                    runOnUiThread {
                        when (it.getInt("responseCode")) {

                            200 -> {
                                errorAlert(this, "$title", it.getString("message")) {}
                                config.avatarURL = it.getString("url")
                                val image = civ_home_profile_activity_image_user
                                Picasso.with(this).load(config.avatarURL).into(image)
                            }
                            1001 -> {
                                errorAlert(this, "$title", getString(R.string.image_exced)) {}
                            }
                        }
                    }
                }
            }

            jupiter.setOnConnectFailure {
                Log.e("PerfilAcConnectFail", it.toString())
                dialogCreate.dismiss()
                runOnUiThread {
                    errorAlert(this, "$title", getString(R.string.error_an_occurred)) {}
                }
            }
            jupiter.setOnSuccess {
                Log.e("PerfilAcConnectFail", it.toString())
                dialogCreate.dismiss()
                runOnUiThread {
                    if (it.getInt("responseCode") == 200) {
                        errorAlert(this, "$title", it.getString("message")) {}
                        config.avatarURL = it.getString("url")
                        val image = civ_home_profile_activity_image_user
                        Picasso.with(this).load(config.avatarURL).into(image)
                    } else {
                        errorAlert(this, "$title", it.getString("message")) {}
                    }
                }

            }
            val serialLogin = config["login", "undefined"]
            if (serialLogin !== "undefined") {
                try {
                    val user = JSONObject(serialLogin)
                    token = user.getString("token")
                    userId = user.getString("userId")
                } catch (e: JSONException) {
                }
            } else {
                config["login"] = "undefined"
                config["remember"] = "false"
                config["token"] = "undefined"
                Utils.launch(this, HomeActivity::class.java)
            }
            val params: ArrayList<Pair<String, Any>> = ArrayList()
            params.add(Pair("AccessToken", token))
            params.add(Pair("userId", userId))
            jupiter.updatePerfilFile(params, current)
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Companion.REQUEST_PERMISSION_GALLERY -> {
                if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "I need permission", Toast.LENGTH_LONG).show()
                } else {
                    val intent = Intent(Intent.ACTION_PICK).apply {
                        type = "image/png"
                    }
                    if (intent.resolveActivity(packageManager) != null) {
                        startActivityForResult(
                            Intent.createChooser(
                                intent,
                                resources.getString(R.string.gallery)
                            ), Companion.REQUEST_IMAGE_GET
                        )
                    }
                }
            }

            Companion.REQUEST_PERMISSION_CAMARA -> {
                if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf<String>(android.Manifest.permission.CAMERA),
                        Companion.REQUEST_PERMISSION_GALLERY
                    )
                } else {
                    Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                        takePictureIntent.resolveActivity(packageManager)?.also {
                            val photoFile = try {
                                createImageFile()
                            } catch (ex: IOException) {
                                Log.e("E", ex.toString())
                                null
                            }
                            // Continue only if the File was successfully created
                            photoFile?.also {
                                val photoURI: Uri = FileProvider.getUriForFile(
                                    this,
                                    "com.realpaymentdevelop.app",
                                    it
                                )
                                photoName = it.name
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                            }
                        }
                    }
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun checkStatusPermissionGallery()
            : Boolean {
        val result = ContextCompat.checkSelfPermission(
            this,
            android.Manifest.permission.READ_EXTERNAL_STORAGE
        )
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun checkStatusPermissionCamara()
            : Boolean {
        val result =
            ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
        return result == PackageManager.PERMISSION_GRANTED
    }

    companion object {
        private const val REQUEST_IMAGE_GET = 10
        private const val REQUEST_IMAGE_CAPTURE = 1
        private const val REQUEST_PERMISSION_GALLERY = 1
        private const val REQUEST_PERMISSION_CAMARA = 2
    }
}