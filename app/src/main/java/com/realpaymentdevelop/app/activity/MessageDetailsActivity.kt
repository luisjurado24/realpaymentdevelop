package com.realpaymentdevelop.app.activity

import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_message_details.*
import java.text.ParseException
import java.text.SimpleDateFormat


class MessageDetailsActivity : JupiterNewActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setUpTheme()
        setContentView(R.layout.activity_message_details)
        title = resources.getString(R.string.title_activity_messages)
        if (intent != null) {
            tv_activity_message_complete.text =
                intent.getStringExtra(resources.getString(R.string.key_message))

            tv_activity_message_title.text =
                intent.getStringExtra(resources.getString(R.string.key_title))

            intent.getStringExtra(resources.getString(R.string.key_subtitle))
            val buttonText = intent.getStringExtra(resources.getString(R.string.key_button))
            if (buttonText.isNullOrEmpty() or (buttonText == "null") or buttonText.isNullOrBlank())
                btn_activity_message_confirm_message.visibility = Button.INVISIBLE
            else
                btn_activity_message_confirm_message.text = buttonText

            var date = intent.getStringExtra(resources.getString(R.string.key_date))
            if (date == "null") {
                date = ""
            } else {
                val sdf = SimpleDateFormat("yyyy-MM-dd")
                try {
                    val dDate = sdf.parse(date)
                    val dateFormat = DateFormat.getMediumDateFormat(applicationContext)
                    date = dateFormat.format(dDate)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            }
            tv_activity_message_date.text = date

            val urlImage = intent.getStringExtra(resources.getString(R.string.key_url_image))

            Picasso.with(applicationContext).load(urlImage).into(iv_activity_message_photo)

        }

    }

    override fun onResume() {
        super.onResume()
        btn_activity_message_confirm_message.setOnClickListener {

            val url = intent.getStringExtra(resources.getString(R.string.key_button_url))
            if (url.isNullOrEmpty()) {
                btn_activity_message_confirm_message.visibility = MaterialButton.INVISIBLE
            } else {
                Log.d("MESSAGE_DETAILS", url)
                if(url.startsWith("www")){
                    /*val uri: Uri = Uri.parse(url)
                    val intent = Intent(Intent.ACTION_VIEW, uri)
                    startActivity(intent)*/
                    Toast.makeText(this,url,Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this,url,Toast.LENGTH_SHORT).show()
                }

            }

        }
    }
}