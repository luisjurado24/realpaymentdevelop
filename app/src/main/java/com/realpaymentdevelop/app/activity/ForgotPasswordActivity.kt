package com.realpaymentdevelop.app.activity

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.utils.ApiResult
import com.realpaymentdevelop.app.utils.Utils
import kotlinx.android.synthetic.main.activity_forgot_password.*
import java.util.*

class ForgotPasswordActivity : JupiterActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpTheme()
        setUpContext(this)
        setContentView(R.layout.activity_forgot_password)
        title = resources.getString(R.string.title_activity_reset_password)
    }
    private fun setUpTheme() {
        val configuration = resources.configuration
        val currentNightMode = configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        when (currentNightMode) {
            Configuration.UI_MODE_NIGHT_NO -> setTheme(R.style.Light)
            Configuration.UI_MODE_NIGHT_YES -> setTheme(R.style.DayNight)
        }
    }

    override fun onResume() {
        super.onResume()
        btn_forgot_pasword_register.setOnClickListener {
            val phoneNumberBoolean =
                til_forgot_password_phone_number.editText!!.text.isNullOrEmpty()
            if (phoneNumberBoolean) {
                til_forgot_password_phone_number.error =
                    resources.getString(R.string.error_phone_number)
            } else {
                val params = ArrayList<Pair<String, String>>()
                params.add(Pair("phone", "${til_forgot_password_phone_number.editText!!.text}"))
                doAsyncTask("/sendTextCode", params)
            }
        }
    }

    override fun doFinalCallback(result: ApiResult?) {
        val intentPassword = Intent(this, ConfirmMessageActivity::class.java)
        intentPassword.putExtra(getString(R.string.phone_extra),til_forgot_password_phone_number.editText!!.text.toString())
        intentPassword.putExtra(getString(R.string.key_title),true)
        startActivity(intentPassword)
    }
}