package com.realpaymentdevelop.app.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.activity.order.InitOrderActivity
import com.realpaymentdevelop.app.adapter.AdapterMerchantOrder
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.Config
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import kotlinx.android.synthetic.main.activity_order.*
import kotlinx.android.synthetic.main.card_view_topic.*
import org.json.JSONException
import org.json.JSONObject

class OrderActivity : JupiterNewActivity(),AdapterMerchantOrder.onClick{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpContext(this)
        title = getString(R.string.title_activity_get_merchants)
        setContentView(R.layout.activity_order)
        val http = JupiterNewHttp(this, "/quickOrderApi", "/getMerchants")
        http.setOnSuccess {
            // Log.d("Merchant",it.toString())
            val array = it.getJSONArray("data")
            if (array.length() > 0) {
                val adapter = AdapterMerchantOrder(this, array)
                runOnUiThread {
                    listMerchants.adapter = adapter
                    val linear = LinearLayoutManager(this@OrderActivity)
                    linear.orientation = LinearLayoutManager.VERTICAL
                    listMerchants.layoutManager= linear
                }
            } else {
                // LANG: translation
                val msg =  getString(R.string.error_an_occurred)
                runOnUiThread {
                    errorAlert(this, "$title", msg) { onBackPressed() }
                }
            }

        }

        http.setOnFailure {
            Log.d("Merchant", it.toString())
            val msg = getString(R.string.error_an_occurred)
            runOnUiThread {
                errorAlert(this, "$title", msg) { onBackPressed() }
            }
        }

        http.setOnConnectFailure {
            val msg = getString(R.string.unfortunately_timeout_message)
            runOnUiThread {
                errorAlert(this, "$title", msg) { onBackPressed() }
            }
        }
        val requestParams: ArrayList<Pair<String, String>> = ArrayList()
        val config = Config(this)
        val token: String? = config["token", "undefined"]
        requestParams.add(Pair("AccessToken", token!!))
        http.doAsyncApiCallMenu(requestParams)
    }

    private fun showLocation(selected: JSONObject) {

        val imageUrl = "${selected.getJSONObject("Image").getString("path")}${
            selected.getJSONObject("Image").getString("name")
        }"

        val merchantName = selected.getJSONObject("Merchant").getString("name")
        val idMerchant = selected.getJSONObject("Merchant").getString("merchantId")
        val slogan = selected.getJSONObject("Merchant").optString("slogan", "SLOGAN")
        val description = selected.getJSONObject("Merchant").optString("slug", "SLUG")
        val intent = Intent(this, InitOrderActivity::class.java)
        intent.putExtra(getString(R.string.key_url_image), imageUrl)
        intent.putExtra(getString(R.string.key_name), merchantName)
        intent.putExtra(getString(R.string.key_merchants), idMerchant)
        intent.putExtra(getString(R.string.key_slogan), slogan)
        intent.putExtra(getString(R.string.key_description), description)
        startActivity(intent)
    }

    override fun merchantSelect(selected: JSONObject) {
        showLocation(selected)
    }

}