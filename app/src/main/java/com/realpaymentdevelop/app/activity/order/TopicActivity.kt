package com.realpaymentdevelop.app.activity.order

import android.annotation.SuppressLint
import android.app.Activity
import android.content.*
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import berlin.volders.badger.BadgeShape
import berlin.volders.badger.Badger
import berlin.volders.badger.CountBadge
import berlin.volders.badger.TextBadge
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.AdapterRecyclerTopic
import com.realpaymentdevelop.app.adapter.order.AdapterTopic
import com.realpaymentdevelop.app.customviews.ExtendableListView
import com.realpaymentdevelop.app.db.DataBase
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.interfaces.sendDataFragments
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_topic.*
import org.json.JSONObject
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.ArrayList

class TopicActivity : JupiterNewActivity(), sendDataFragments {

    private var itemSelected: JsonObject? = null
    private var extras: JsonArray? = JsonArray()
    private var needExtras = false
    private var count = 1
    private var tipEnable = 0
    private var totalPriceDouble: Double = 0.00
    private var acumulatePrice: Double = 0.00
    private var total: Double = 0.00
    private var toEdit = false
    private var merchantImage = String()
    private lateinit var allQuickModifiers: MutableList<JsonObject>
    private var badge :CountBadge?= null
    private var orderOfExtrasGroup: JsonArray? = null

    lateinit var adapter: AdapterRecyclerTopic
    lateinit var listRecord: RecyclerView
    private var jsonDelivery: String? = ""
    private var locationName: String? = ""
    private var nameMerchant: String? = ""
    private var idMerchant: String? = ""
    private var cartSize = 0
    private var merchantLocationId: String? = ""

    private val listBoolean: ArrayList<Pair<String, Pair<Int, Int>>> = ArrayList()

    private var positionInListOfExtrasSelected = ArrayList<Int>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpContext(this)
        setContentView(R.layout.activity_topic)
        val jsonParser = JsonParser()
        nameMerchant = intent.getStringExtra(getString(R.string.key_name))
        val location = intent.getStringExtra(getString(R.string.key_localization))
        locationName = intent.getStringExtra(getString(R.string.key_localization_name))
        jsonDelivery = intent.getStringExtra(getString(R.string.key_json_null))
        tipEnable = intent.getIntExtra(getString(R.string.key_enable), 0)
        idMerchant = intent.getStringExtra(getString(R.string.key_merchants))
        merchantLocationId = intent.getStringExtra(getString(R.string.key_localization))

        cartSize = DataBase(this).getCart(idMerchant!!,merchantLocationId!!).size
        val items = jsonParser.parse(intent.getStringExtra("key")).asJsonObject
        val arrayWithOrderExtrasGroup =
            jsonParser.parse(intent.getStringExtra(getString(R.string.key_products))).asJsonArray
        val urlImagen = intent.getStringExtra(getString(R.string.key_url_image))
        title = items.getAsJsonObject("QuickItem").get("name").asString
        setValues(items, false, urlImagen!!, arrayWithOrderExtrasGroup)
        setElementsOnDisplay()

    }

    private fun setButtons(enable: Boolean) {
        add_cart_bottom.isEnabled = enable
    }

    override fun sendDatatoAdapter(total: Double, flag: Boolean, extras: JsonArray) {
        this.extras = extras
        totalPriceDouble = totalPriceDouble.plus(total)
        acumulatePrice = totalPriceDouble
        displayTotal()
        val checButton = ArrayList<Boolean>()
        listBoolean.forEach {
            val quickModifider = it.first
            val minSelect = it.second.first
            val maxSelect = it.second.second
            var count = 0
            extras.forEach {
                val json = it.asJsonObject
                if (json["quickModifierGroupId"].asString == quickModifider) {
                    count += 1
                }
            }
            Log.e("count", count.toString())
            if (maxSelect > 0 && minSelect > 0) {
                checButton.add(maxSelect == count)
            }
            if (maxSelect == 0 && minSelect > 0) {
                checButton.add(count>= minSelect)
            }
            if(maxSelect>0 && minSelect==0){
                checButton.add(maxSelect>=count)
            }
        }
        if(checButton.contains(false)){
            setButtons(false)
        }else{
            setButtons(true)
        }
        Log.e("buttonCheck",checButton.toString())

    }
    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val factory = CountBadge.Factory(this, BadgeShape.circle(0.5f, Gravity.TOP))
        val menuItem = menu?.findItem(R.id.cart_menu)
        badge = Badger.sett(menuItem!!, factory)
        badge?.count= cartSize
        return true
    }

    override fun onResume() {
        super.onResume()
        if(badge != null){
            val data= DataBase(this).getCart(idMerchant!!,merchantLocationId!!).size
            badge?.count = data
            Log.e("TAG","NOTmenuItemNull")
        }else{
            Log.e("TAG","menuItemNull")
        }
    }


    private fun setValues(json: JsonObject, isToEdit: Boolean = false, imageUrl: String, arrayWithOrderExtrasGroup: JsonArray?) {
        try {
            itemSelected = json
            toEdit = false
            merchantImage = imageUrl
            orderOfExtrasGroup = arrayWithOrderExtrasGroup
            val modifierGroups = itemSelected!!.get("ModifierGroups").asJsonObject
            allQuickModifiers = mutableListOf()

            val keys = modifierGroups.keySet()
            keys.forEach {
                if (modifierGroups.get(it) is JsonObject) {

                    val parent = modifierGroups.get(it).asJsonObject
                    allQuickModifiers.add(parent)
                }
            }

            sortModifierGroups()

            needExtras = true

        } catch (e: Exception) {
            needExtras = false
        }


    }

    private fun sortModifierGroups() {

        if (orderOfExtrasGroup != null) {
            allQuickModifiers.forEach {
                for (i in 0 until orderOfExtrasGroup!!.size()) {
                    val quickModifider = it.get("QuickModifierGroup").asJsonObject
                    val quickModifierGroupId = orderOfExtrasGroup!!.get(i).asJsonObject
                    val quickItemId = itemSelected!!.get("QuickItem").asJsonObject
                    if ((quickModifider.get("quickModifierGroupId").asInt == quickModifierGroupId.get(
                            "quickModifierGroupId"
                        ).asInt)
                        && (quickModifierGroupId.get("quickItemId").asInt == quickItemId.get("quickItemId").asInt)
                    ) {
                        it.addProperty(
                            "sequenceOfDisplay",
                            quickModifierGroupId.get("sequenceOfDisplay").asInt
                        )
                    }
                }
            }
            allQuickModifiers.sortBy {
                it.get("sequenceOfDisplay").asInt
            }
        } else {
            allQuickModifiers.sortBy {
                val quickModifierGroupId = it.get("QuickModifierGroup").asJsonObject
                quickModifierGroupId.get("quickModifierGroupId").asInt
            }
        }
    }

    private fun getModifierGroup(): JsonArray {

        val list = JsonArray()

        allQuickModifiers.forEach {
            list.add(it.get("QuickModifierGroup").asJsonObject)
        }

        return list
    }

    private fun getModifiers(): JsonArray {

        val list = JsonArray()

        allQuickModifiers.forEach { it ->
            val quickModified = it.get("QuickModifier").asJsonObject
            val aux = JsonArray()

            val keysQM = quickModified.keySet()
            keysQM.forEach {
                if (quickModified.get(it) is JsonObject) {
                    val child = quickModified.get(it).asJsonObject
                    aux.add(child)
                }
            }
            list.add(aux)
        }
        return list

    }


    private fun buildJsonToSendCart(): JsonObject {

        val jsonQuickGroup = itemSelected?.get("QuickItem")?.asJsonObject
        val jsonToSend = JsonObject()

        jsonToSend.add("Item", jsonQuickGroup)

        jsonToSend.addProperty("Quantity", count)
        jsonToSend.addProperty("TotalCost", total)

        if (extras?.size()!! > 0) {
            jsonToSend.add("Extras", extras)
            jsonToSend.add("ModifierGroups", itemSelected!!.get("ModifierGroups").asJsonObject)
            jsonToSend.add("Image", itemSelected!!.get("Image").asJsonObject)
        } else {
            jsonToSend.add("Extras", JsonArray())
            jsonToSend.add("Image", itemSelected!!.get("Image").asJsonObject)
        }

        if (toEdit) {
            jsonToSend.addProperty("IdLocal", itemSelected!!.get("IdLocal").asString)
        } else {
            jsonToSend.addProperty("IdLocal", ('a'..'z').randomString(6))

        }
        return jsonToSend
    }

    @SuppressLint("SetTextI18n")
    private fun displayTotal() {
        val mf = DecimalFormat("0.00")
        total = acumulatePrice * count
        total = total.plus(0.0001)
        total_price.text = "$${mf.format(total)}"

    }

    private fun setElementsOnDisplay() {

        listRecord = menu_extras
        var isNoNecessary = false
        var idExtrasArrayListInt = ArrayList<Int>()
        val linear = LinearLayoutManager(this)
        linear.orientation = LinearLayoutManager.VERTICAL
        listRecord.layoutManager = linear
        var url = ""
        runOnUiThread {
            val builder = Picasso.with(this)
            try {
                try {
                    val image: JsonObject? = itemSelected!!.get("Image").asJsonObject
                    url =
                        "${getString(R.string.static_hostname)}/${image!!.get("path").asString}/${
                        image.get("name").asString}"
                    builder.load(url)
                        .error(R.drawable.placeholder)
                        .resize(250, 250)
                        .centerCrop()
                        .into(item_icon)

                } catch (e: Exception) {
                    builder.load(merchantImage)
                        .error(R.drawable.placeholder)
                        .resize(250, 250)
                        .centerCrop()
                        .into(item_icon)
                } finally {
                    try {
                        val quickItem: JsonObject? =
                            itemSelected!!.get("QuickItem").asJsonObject
                        totalPriceDouble = quickItem!!.get("unitPrice").asString.toDouble()
                        description_item.text = quickItem.get("description").asString.toString()
                        acumulatePrice = totalPriceDouble
                    } catch (e: Exception) {
                        description_item.text = ""
                        acumulatePrice = totalPriceDouble

                    }


                    if (toEdit) {
                        count = itemSelected!!.get("Quantity").asInt
                        add_cart_bottom.text = "Modify"
                        setButtons(true)
                    }

                    text_quantity.text = count.toString()
                    displayTotal()


                    button_less.setOnClickListener {
                        if (count == 1) {
                            count = 1
                        } else {
                            count--
                        }
                        text_quantity.text = count.toString()
                        displayTotal()
                    }

                    button_more.setOnClickListener {
                        count++
                        text_quantity.text = count.toString()
                        displayTotal()

                    }

                    if (needExtras) {
                        val modifiers = getModifiers()
                        val headers = getModifierGroup()
                        Log.e("headers", headers.toString())
                        adapter = AdapterRecyclerTopic(this, headers, modifiers, extras)
                        headers.forEach {
                            val json = it.asJsonObject
                            val minSelected = json.get("minSelect").asInt
                            val maxSelected = json.get("maxSelect").asInt
                            val groupId = json.get("quickModifierGroupId").asString
                            if (minSelected > 0 || maxSelected > 0) {
                                listBoolean.add(Pair(groupId, Pair(minSelected, maxSelected)))
                            }
                        }
                        setButtons(listBoolean.isEmpty())
                        Log.e("listBoolean", listBoolean.toString())
                        listRecord.adapter = adapter
                    } else {
                        setButtons(true)
                    }
                }

                add_cart_bottom.setOnClickListener {
                    val intent = Intent(this, ReviewOrderActivity::class.java)
                    intent.putExtra(getString(R.string.key_merchants), merchantImage)
                    intent.putExtra(getString(R.string.key_url_image), url)
                    intent.putExtra(getString(R.string.key_localization), jsonDelivery)
                    intent.putExtra(getString(R.string.key_name), nameMerchant)
                    intent.putExtra(getString(R.string.key_localization_name), locationName)
                    intent.putExtra(getString(R.string.key_json_null), buildJsonToSendCart().toString())
                    startActivityForResult(intent, 5)
                }
            } catch (e: Exception) {

                val msg = getString(R.string.error_an_occurred)
                runOnUiThread { errorAlert(this, "$title", msg) { onBackPressed() } }
            }
        }
    }

    private fun addCartNew(jsonToSendCart: JsonObject){
        val contentValuesCart = ContentValues()
        val contentValues = ContentValues()
        val base = DataBase(this)
        val id = jsonToSendCart["IdLocal"].asString
        val json = base.localizationCart(merchantLocationId!!,idMerchant!!)
        if(!json.has("idLocation")){
            contentValues.run {
                put(DataBase.ColumnsName.FeedEntry.TABLE_LOCATION_ID, merchantLocationId)
                put(DataBase.ColumnsName.FeedEntry.TABLE_LOCATION_DELIVERY, jsonDelivery)
                put(DataBase.ColumnsName.FeedEntry.TABLE_LOCATION_NAME, locationName)
                put(DataBase.ColumnsName.FeedEntry.TABLE_LOCATION_MERCHANT, nameMerchant)
                put(DataBase.ColumnsName.FeedEntry.TABLE_LOCATION_MERCHANT_ID, idMerchant )
                put(DataBase.ColumnsName.FeedEntry.TABLE_LOCATION_MERCHANT_URL, merchantImage)
            }
            contentValuesCart.run {
                put(DataBase.ColumnsName.FeedEntry.TABLE_CART_ID, id)
                put(DataBase.ColumnsName.FeedEntry.TABLE_CART_OBJECT, jsonToSendCart.toString())
                put(DataBase.ColumnsName.FeedEntry.TABLE_CART_ID_LOCATION,merchantLocationId)
                put(DataBase.ColumnsName.FeedEntry.TABLE_CART_ID_MERCHANT,idMerchant)
            }
            base.setJson(contentValuesCart, contentValues)
        }else{
            if(json == JSONObject(jsonDelivery)){
                contentValuesCart.run {
                    put(DataBase.ColumnsName.FeedEntry.TABLE_CART_ID, id)
                    put(DataBase.ColumnsName.FeedEntry.TABLE_CART_OBJECT, jsonToSendCart.toString())
                    put(DataBase.ColumnsName.FeedEntry.TABLE_CART_ID_LOCATION,merchantLocationId)
                    put(DataBase.ColumnsName.FeedEntry.TABLE_CART_ID_MERCHANT,idMerchant)
                }
                base.setJson(contentValuesCart, null)
            }else{
                base.deleteLocation(idMerchant!!,merchantLocationId!!)
                contentValues.run {
                    put(DataBase.ColumnsName.FeedEntry.TABLE_LOCATION_ID, merchantLocationId)
                    put(DataBase.ColumnsName.FeedEntry.TABLE_LOCATION_DELIVERY, jsonDelivery)
                    put(DataBase.ColumnsName.FeedEntry.TABLE_LOCATION_NAME, locationName)
                    put(DataBase.ColumnsName.FeedEntry.TABLE_LOCATION_MERCHANT, nameMerchant)
                    put(DataBase.ColumnsName.FeedEntry.TABLE_LOCATION_MERCHANT_ID, idMerchant )
                    put(DataBase.ColumnsName.FeedEntry.TABLE_LOCATION_MERCHANT_URL, merchantImage)
                }
                contentValuesCart.run {
                    put(DataBase.ColumnsName.FeedEntry.TABLE_CART_ID, id)
                    put(DataBase.ColumnsName.FeedEntry.TABLE_CART_OBJECT, jsonToSendCart.toString())
                    put(DataBase.ColumnsName.FeedEntry.TABLE_CART_ID_LOCATION,merchantLocationId)
                    put(DataBase.ColumnsName.FeedEntry.TABLE_CART_ID_MERCHANT,idMerchant)
                }
                base.setJson(contentValuesCart, contentValues)
            }

        }

        Toast.makeText(this@TopicActivity, getString(R.string.add_cart_correct), Toast.LENGTH_SHORT).show()
    }

    private fun setLocalValues() {

        itemSelected = itemSelected
        cartSize = cartSize
        extras = extras
        needExtras = needExtras
        count = count
        totalPriceDouble = totalPriceDouble
        acumulatePrice = acumulatePrice
        total = total
        toEdit = toEdit
        merchantImage = merchantImage
        orderOfExtrasGroup = orderOfExtrasGroup

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_card, menu)
        val factory = CountBadge.Factory(this, BadgeShape.circle(0.5f, Gravity.TOP))
        val menuItem = menu?.findItem(R.id.cart_menu)
        badge = Badger.sett(menuItem!!, factory)
        badge?.count = cartSize
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.cart_menu -> {
                if (cartSize != 0) {
                    val factory = CountBadge.Factory(this, BadgeShape.circle(0.5f, Gravity.TOP))
                    val intent = Intent(this, ConfirmOrderActivity::class.java)
                    intent.putExtra(getString(R.string.key_json_null), jsonDelivery)
                    intent.putExtra(getString(R.string.key_enable), tipEnable)
                    intent.putExtra(getString(R.string.key_merchants), idMerchant)
                    intent.putExtra(getString(R.string.key_localization), merchantLocationId)
                    intent.putExtra(getString(R.string.key_name), nameMerchant)
                    intent.putExtra(getString(R.string.key_localization_name), locationName)
                    Badger.sett(item, factory).count = cartSize
                    startActivity(intent)

                } else {
                    Toast.makeText(this, getString(R.string.not_elements), Toast.LENGTH_SHORT)
                        .show()
                }

            }
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return true
    }

    //This function generate a local key to identify the item
    private fun ClosedRange<Char>.randomString(lenght: Int) =
        (1..lenght)
            .map { (Random().nextInt(endInclusive.toInt() - start.toInt()) + start.toInt()).toChar() }
            .joinToString("")

    override fun onStop() {
        positionInListOfExtrasSelected = ArrayList()
        setLocalValues()
        super.onStop()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 5) {
            when (resultCode) {
                RESULT_OK -> {
                    addCartNew(buildJsonToSendCart())
                    setResult(Activity.RESULT_OK)
                    finish()
                }
                RESULT_CANCELED -> Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        setResult(Activity.RESULT_CANCELED)
        onBackPressed()
        return false
    }

}