package com.realpaymentdevelop.app.activity.order

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import kotlinx.android.synthetic.main.activity_add_card.*
import kotlinx.android.synthetic.main.activity_address_location.*
import org.json.JSONObject

class AddressLocationActivity : JupiterNewActivity() {
    val data = JSONObject("{data:[" +
            "{name:\"Alaska\",id:\"AK\"}," +
            "{name:\"Alabama\",id:\"AL\"}," +
            "{name:\"Arkansas\",id:\"AR\"}," +
            "{name:\"American Samoa\",id:\"AS\"}," +
            "{name:\"Arizona\",id:\"AZ\"}," +
            "{name:\"California\",id:\"CA\"}," +
            "{name:\"Colorado\",id:\"CO\"}," +
            "{name:\"Connecticut\",id:CT}," +
            "{name:\"District of Columbia\",id:\"DC\"}," +
            "{name:\"Delaware\",id:\"DE\"}," +
            "{name:\"Florida\",id:\"FL\"}," +
            "{name:\"Georgia\",id:\"GA\"}," +
            "{name:\"Guam\",id:\"GU\"}," +
            "{name:\"Hawaii\",id:\"HI\"}," +
            "{name:\"Iowa\",id:\"IA\"}," +
            "{name:\"Idaho\",id:\"ID\"}," +
            "{name:\"Illinois\",id:\"IL\"}," +
            "{name:\"Indiana\",id:\"IN\"}," +
            "{name:\"Kansas\",id:\"KS\"}," +
            "{name:\"Kentucky\",id:\"KY\"}," +
            "{name:\"Louisiana\",id:\"LA\"}," +
            "{name:\"Massachusetts\",id:\"MA\"}," +
            "{name:\"Maryland\",id:\"MD\"}," +
            "{name:\"Maine\",id:\"ME\"}," +
            "{name:\"Michigan\",id:\"MI\"}," +
            "{name:\"Minnesota\",id:\"MN\"}," +
            "{name:\"Missouri\",id:\"MO\"}," +
            "{name:\"Mississippi\",id:\"MS\"}," +
            "{name:\"Montana\",id:\"MT\"}," +
            "{name:\"North Carolina\",id:\"NC\"}," +
            "{name:\"North Dakota\",id:\"ND\"}," +
            "{name:\"Nebraska\",id:\"NE\"}," +
            "{name:\"New Hampshire\",id:\"NH\"}," +
            "{name:\"New Jersey\",id:\"NJ\"}," +
            "{name:\"New Mexico\",id:\"NM\"}," +
            "{name:\"Nevada\",id:\"NV\"}," +
            "{name:\"New York\",id:\"NY\"}," +
            "{name:\"Ohio\",id:\"OH\"}," +
            "{name:\"Oklahoma\",id:\"OK\"}," +
            "{name:\"Oregon\",id:\"OR\"}," +
            "{name:\"Pennsylvania\",id:\"PA\"}," +
            "{name:\"Puerto Rico\",id:\"PR\"}," +
            "{name:\"Rhode Island\",id:\"RI\"}," +
            "{name:\"South Carolina\",id:\"SC\"}," +
            "{name:\"South Dakota\",id:\"SD\"}," +
            "{name:\"Tennessee\",id:\"TN\"}," +
            "{name:\"Texas\",id:\"TX\"}," +
            "{name:\"Utah\",id:\"UT\"}," +
            "{name:\"Virginia\",id:\"VA\"}," +
            "{name:\"Virgin Islands\",id:\"VI\"}," +
            "{name:\"Vermont\",id:\"VT\"}," +
            "{name:\"Washington\",id:\"WA\"}," +
            "{name:\"Wisconsin\",id:\"WI\"}," +
            "{name:\"West Virginia\",id:\"WV\"}," +
            "{name:\"Wyoming\",id:\"WY\"}" +
            "]}")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_address_location)
        val statesNames  = ArrayList<String>()
        val prepatePair = data.getJSONArray("data")
        for (i in 0 until prepatePair.length()) {
            val json = prepatePair.getJSONObject(i)
            val state = json.getString("name")
            statesNames.add(state)
        }
        title = getString(R.string.title_add_address_2)
        val statesAdapter = ArrayAdapter(this,R.layout.list_item,statesNames)
        (tv_state.editText as? AutoCompleteTextView)?.setAdapter(statesAdapter)
    }

    override fun onResume() {
        super.onResume()
        val merchantId = intent.getStringExtra(getString(R.string.key_merchants))
        val token = intent.getStringExtra(getString(R.string.key_token))
        val merchantName = intent.getStringExtra(getString(R.string.key_name))
        val merchantLocationId = intent.getStringExtra(getString(R.string.key_merchants_localization))
        val buildingDelivery = intent.getBooleanExtra(getString(R.string.key_building_delivery),false)
        val locationName = intent.getStringExtra(getString(R.string.key_localization_name))
        val url = intent.getStringExtra(getString(R.string.key_url_image))
        val locationJson = intent.getStringExtra(getString(R.string.key_localization))
        val tipEnable = intent.getIntExtra(getString(R.string.key_enable),0)
        val pickupSpot = intent.getBooleanExtra(getString(R.string.key_pickupSpot),false)
        val pickupCounter = intent.getBooleanExtra(getString(R.string.key_pickupCounter),false)
        val fullDelivery = intent.getBooleanExtra(getString(R.string.key_full_delivery),false)
        btn_add_address.setOnClickListener {
            if(tv_address_one.editText!!.text.isNullOrEmpty() && tv_address_two.editText!!.text.isNullOrEmpty()){
                val msj = getString(R.string.message_error_address)
                Toast.makeText(this,msj, Toast.LENGTH_SHORT).show()
            }else if(tv_city.editText!!.text.isNullOrEmpty()){
                val msj = getString(R.string.message_error_city)
                Toast.makeText(this,msj, Toast.LENGTH_SHORT).show()
            }else if(tv_zip.editText!!.text.isNullOrEmpty()){
                val msj = getString(R.string.message_error_zip)
                Toast.makeText(this,msj, Toast.LENGTH_SHORT).show()
            }else{

                val stateSelect = tv_state.editText!!.text
                val addressOne = if(!tv_address_one.editText!!.text.isNullOrEmpty()) tv_address_one.editText!!.text.toString() else "null"
                val addressTwo =if(!tv_address_two.editText!!.text.isNullOrEmpty()) tv_address_two.editText!!.text.toString() else "null"
                val city = if(!tv_city.editText!!.text.isNullOrEmpty()) tv_city.editText!!.text.toString() else "null"
                val zip = if(!tv_zip.editText!!.text.isNullOrEmpty()) tv_zip.editText!!.text.toString() else "null"
                val data = "{\"deliveryAddress1\":\"$addressOne\",\"deliveryAddress2\":\"$addressTwo\",\"deliveryCity\":\"$city\"," +
                        "\"deliveryState\":\"$stateSelect\",\"deliveryZipCode\":\"$zip\",\"deliveryCountry\":\"US\"}"
                val json= "{\"location\":$locationJson," +
                        "\"delivery\":{" +
                        "\"pickupCounter\":false," +
                        "\"pickupSpot\":false," +
                        "\"buildingDelivery\":false," +
                        "\"fullDelivery\":$fullDelivery}," +
                        "\"type\":2," +
                        "\"fullDelivery\":{\"address\":$data}}"
                val intentExpress = Intent(this,CategoriesActivity::class.java)
                intentExpress.putExtra(getString(R.string.key_building_delivery),buildingDelivery)
                intentExpress.putExtra(getString(R.string.key_localization),locationJson)
                intentExpress.putExtra(getString(R.string.key_localization_name),locationName)
                intentExpress.putExtra(getString(R.string.key_url_image),url)
                intentExpress.putExtra(getString(R.string.key_enable),tipEnable)
                intentExpress.putExtra(getString(R.string.key_pickupSpot),pickupSpot)
                intentExpress.putExtra(getString(R.string.key_pickupCounter),pickupCounter)
                intentExpress.putExtra(getString(R.string.key_full_delivery),fullDelivery)
                intentExpress.putExtra(getString(R.string.key_json_null),json)
                intentExpress.putExtra(getString(R.string.key_merchants),merchantId)
                intentExpress.putExtra(getString(R.string.key_token),token)
                intentExpress.putExtra(getString(R.string.key_name),merchantName)
                intentExpress.putExtra(getString(R.string.key_merchants_localization),merchantLocationId)
                startActivity(intentExpress)
            }
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }

    override fun onBackPressed() {
        finish()
    }

}