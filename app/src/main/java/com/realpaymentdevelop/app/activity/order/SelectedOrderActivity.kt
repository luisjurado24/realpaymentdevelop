package com.realpaymentdevelop.app.activity.order

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import kotlinx.android.synthetic.main.activity_selected_order.*
import org.json.JSONObject

class SelectedOrderActivity : JupiterNewActivity() {
    var pickupCounter = false
    var fullDelivery = false
    var pickupSpot = false
    var buildingDelivery = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpContext(this)
        setContentView(R.layout.activity_selected_order)
        title = intent.getStringExtra(getString(R.string.key_localization_name))
        pickupSpot = intent.getBooleanExtra(getString(R.string.key_pickupSpot), false)
        pickupCounter = intent.getBooleanExtra(getString(R.string.key_pickupCounter), false)
        fullDelivery = intent.getBooleanExtra(getString(R.string.key_full_delivery), false)
        buildingDelivery = intent.getBooleanExtra(getString(R.string.key_building_delivery), false)

        btn_pickupSpot.visibility = if (pickupSpot) View.VISIBLE else View.INVISIBLE
        btn_pickupCounter.visibility = if (pickupCounter) View.VISIBLE else View.INVISIBLE
        btn_delivery.visibility = if (fullDelivery) View.VISIBLE else View.INVISIBLE
        btn_fulldelivery.visibility = if(buildingDelivery) View.VISIBLE else View.INVISIBLE
    }

    override fun onResume() {
        super.onResume()
        val merchantId = intent.getStringExtra(getString(R.string.key_merchants))
        val token = intent.getStringExtra(getString(R.string.key_token))
        val merchantName = intent.getStringExtra(getString(R.string.key_name))
        val merchantLocationId =
            intent.getStringExtra(getString(R.string.key_merchants_localization))
        val locationName = intent.getStringExtra(getString(R.string.key_localization_name))
        val url = intent.getStringExtra(getString(R.string.key_url_image))
        val tipEnable = intent.getIntExtra(getString(R.string.key_enable), 0)
        val locationJson = intent.getStringExtra(getString(R.string.key_localization))
        val jsonNull = "{\"location\":$locationJson," +
                "\"delivery\":{" +
                "\"pickupCounter\":$pickupCounter," +
                "\"pickupSpot\":$pickupSpot," +
                "\"buildingDelivery\":$buildingDelivery," +
                "\"fullDelivery\":$fullDelivery}," +
                "\"type\":0," +
                "\"counterPickup\":\"noDeliveryInformation\"}"
        btn_pickupSpot.setOnClickListener {
            val jsonCounter = "{\"location\":$locationJson," +
                    "\"delivery\":{" +
                    "\"pickupCounter\":$pickupCounter," +
                    "\"pickupSpot\":false," +
                    "\"buildingDelivery\":false," +
                    "\"fullDelivery\":false}," +
                    "\"type\":0," +
                    "\"counterPickup\":\"noDeliveryInformation\"}"
            val intentExpress = Intent(this, CategoriesActivity::class.java)
            intentExpress.putExtra(getString(R.string.key_merchants), merchantId)
            intentExpress.putExtra(getString(R.string.key_token), token)
            intentExpress.putExtra(getString(R.string.key_name), merchantName)
            intentExpress.putExtra(getString(R.string.key_merchants_localization), merchantLocationId)
            intentExpress.putExtra(getString(R.string.key_localization_name), locationName)
            intentExpress.putExtra(getString(R.string.key_url_image), url)
            intentExpress.putExtra(getString(R.string.key_enable), tipEnable)
            intentExpress.putExtra(getString(R.string.key_json_null), jsonCounter)

            startActivity(intentExpress)

        }
        btn_pickupCounter.setOnClickListener {

            val intentCounter = Intent(this, SelectedStoreActivity::class.java)

            intentCounter.putExtra(getString(R.string.key_merchants), merchantId)
            intentCounter.putExtra(getString(R.string.key_token), token)
            intentCounter.putExtra(getString(R.string.key_name), merchantName)
            intentCounter.putExtra(getString(R.string.key_merchants_localization), merchantLocationId)
            intentCounter.putExtra(getString(R.string.key_building_delivery), buildingDelivery)
            intentCounter.putExtra(getString(R.string.key_localization_name), locationName)
            intentCounter.putExtra(getString(R.string.key_url_image), url)
            intentCounter.putExtra(getString(R.string.key_enable), tipEnable)
            intentCounter.putExtra(getString(R.string.key_localization),locationJson)
            startActivity(intentCounter)
        }

        btn_delivery.setOnClickListener {
            val intentDelivery = Intent(this,AddDetailsStoreOfficeActivity::class.java)
            intentDelivery.putExtra(getString(R.string.key_localization), locationJson)
            intentDelivery.putExtra(getString(R.string.key_merchants), merchantId)
            intentDelivery.putExtra(getString(R.string.key_token), token)
            intentDelivery.putExtra(getString(R.string.key_name), merchantName)
            intentDelivery.putExtra(getString(R.string.key_merchants_localization), merchantLocationId)
            intentDelivery.putExtra(getString(R.string.key_building_delivery), buildingDelivery)
            intentDelivery.putExtra(getString(R.string.key_localization_name), locationName)
            intentDelivery.putExtra(getString(R.string.key_url_image), url)
            intentDelivery.putExtra(getString(R.string.key_enable), tipEnable)
            intentDelivery.putExtra(getString(R.string.key_pickupSpot), pickupSpot)
            intentDelivery.putExtra(getString(R.string.key_pickupCounter), pickupCounter)
            intentDelivery.putExtra(getString(R.string.key_full_delivery), fullDelivery)
            startActivity(intentDelivery)
        }

        btn_fulldelivery.setOnClickListener {
            val intentDelivery = Intent(this,AddressLocationActivity::class.java)
            intentDelivery.putExtra(getString(R.string.key_localization), locationJson)
            intentDelivery.putExtra(getString(R.string.key_merchants), merchantId)
            intentDelivery.putExtra(getString(R.string.key_token), token)
            intentDelivery.putExtra(getString(R.string.key_name), merchantName)
            intentDelivery.putExtra(getString(R.string.key_merchants_localization), merchantLocationId)
            intentDelivery.putExtra(getString(R.string.key_building_delivery), buildingDelivery)
            intentDelivery.putExtra(getString(R.string.key_localization_name), locationName)
            intentDelivery.putExtra(getString(R.string.key_url_image), url)
            intentDelivery.putExtra(getString(R.string.key_enable), tipEnable)
            intentDelivery.putExtra(getString(R.string.key_pickupSpot), pickupSpot)
            intentDelivery.putExtra(getString(R.string.key_pickupCounter), pickupCounter)
            intentDelivery.putExtra(getString(R.string.key_full_delivery), fullDelivery)
            startActivity(intentDelivery)
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }
    override fun onBackPressed() {
        finish()
    }

}