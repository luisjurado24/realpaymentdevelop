package com.realpaymentdevelop.app.activity

import android.content.res.Configuration
import android.os.Bundle
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.utils.ApiResult
import com.realpaymentdevelop.app.utils.Utils
import kotlinx.android.synthetic.main.activity_confirm_message.*
import java.util.*

class ConfirmMessageActivity : JupiterActivity() {
    var titleBool:Boolean=false
    override fun onCreate(savedInstanceState: Bundle?) {
        setUpTheme()
        setUpContext(this)
        super.onCreate(savedInstanceState)
        titleBool =intent.getBooleanExtra(resources.getString(R.string.key_title), false)
        title = if(titleBool) getString(R.string.title_activity_reset_password) else getString(R.string.title_activity_signup_confirm)
        setContentView(R.layout.activity_confirm_message)
        tv_confirm_message_phone.text =
            "${intent.getStringExtra(getString(R.string.phone_extra))}"
    }

    private fun setUpTheme() {
        val configuration = resources.configuration
        val currentNightMode = configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        when (currentNightMode) {
            Configuration.UI_MODE_NIGHT_NO -> setTheme(R.style.Light)
            Configuration.UI_MODE_NIGHT_YES -> setTheme(R.style.DayNight)
        }
    }

    override fun onResume() {
        super.onResume()
        btn_confirm_message_cancel.setOnClickListener {
            Utils.launch(this, LogInActivity::class.java)
        }
        btn_confirm_message_register.setOnClickListener {
            val bundle = Bundle()
            bundle.putString(
                resources.getString(R.string.key_phone_number),
                tv_confirm_message_phone.text.toString()
            )
            val password = til_confirm_message_password.editText!!.text.toString()
            val password2 = til_confirm_message_password2.editText!!.text.toString()

            val passwordBoolean = til_confirm_message_password.editText!!.text.isNullOrBlank()
            val password2Boolean = til_confirm_message_password2.editText!!.text.isNullOrBlank()
            val codeTextboolean = til_confirm_message_code_reg.editText!!.text.isNullOrBlank()

            if (passwordBoolean or password2Boolean or codeTextboolean) {
                if (codeTextboolean) til_confirm_message_code_reg.error =
                    resources.getString(R.string.error_code_confirmation)
                else til_confirm_message_code_reg.error = null
                if (passwordBoolean) til_confirm_message_password.error =
                    resources.getString(R.string.error_password)
                else til_confirm_message_password.error = null
                if (password2Boolean) til_confirm_message_password2.error =
                    resources.getString(R.string.error_password)
                else til_confirm_message_password2.error = null
            } else {
                if (password.equals(password2)) {
                    if (password.length > 5) {
                        val signupDisplayPhone = tv_confirm_message_phone
                        val registrationCode = til_confirm_message_code_reg.editText!!
                        val params = ArrayList<Pair<String, String>>()
                        params.add(Pair<String, String>("phone", signupDisplayPhone.text.toString()))
                        params.add(Pair<String, String>("textCode", registrationCode.text.toString()))
                        params.add(Pair<String, String>("password", password.toString()))
                        params.add(Pair<String, String>("password2", password2.toString()))
                        if(titleBool ){
                            doAsyncTask("/setPasswordWithTextCode", params)
                        }else{
                            doAsyncTask("/completeRegisterUserByPhone", params)
                        }
                    } else {
                        til_confirm_message_password.error =
                            resources.getString(R.string.error_invalid_password_long)
                        til_confirm_message_password2.error =
                            resources.getString(R.string.error_invalid_password_long)
                    }
                } else {
                    til_confirm_message_code_reg.error = null
                    til_confirm_message_password.error =
                        resources.getString(R.string.error_password_not_equals)
                    til_confirm_message_password2.error =
                        resources.getString(R.string.error_password_not_equals)
                }
            }
        }
    }

    override fun onBackPressed() {
        if (title == resources.getString(R.string.title_activity_reset_password)) {
            Utils.launch(this, ForgotPasswordActivity::class.java)
            finish()
        } else {
            Utils.launch(this, SignUpActivity::class.java)
            finish()
        }
    }

    override fun doFinalCallback(result: ApiResult?) {
        val usernameExtra = resources.getString(R.string.username_extra)
        Utils.launch(
            activity,
            LogInActivity::class.java,
            usernameExtra,
            tv_confirm_message_phone.text.toString()
        )
        finish()
    }

    override fun onNavigateUp(): Boolean {
        onBackPressed()
        return super.onNavigateUp()
    }
}

