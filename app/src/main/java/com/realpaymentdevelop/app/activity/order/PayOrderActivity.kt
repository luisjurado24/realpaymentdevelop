package com.realpaymentdevelop.app.activity.order

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.AdapterView
import android.widget.Toast
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.activity.AddCardActivity
import com.realpaymentdevelop.app.activity.HomeActivity
import com.realpaymentdevelop.app.adapter.AdapterCreditCardNow
import com.realpaymentdevelop.app.customviews.ExtendableListView
import com.realpaymentdevelop.app.data.CreditCards
import com.realpaymentdevelop.app.dialog.DialogPersonalizate
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import com.realpaymentdevelop.app.utils.Utils
import kotlinx.android.synthetic.main.activity_pay_order.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class PayOrderActivity : JupiterNewActivity(), DialogPersonalizate.BackCvvFragment {
    private val translatedCards = ArrayList<CreditCards>()
    private var arreglo: CreditCards? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pay_order)
        title = getString(R.string.title_activity_view_credit_card)
        val http = JupiterNewHttp(this, "/listUserCards")

        http.setOnSuccess {
            Log.d("MainCreditFragment", "credit card $it")
            runOnUiThread {
                cardAdapter(it)
            }
        }

        http.setOnFailure {
            val message =
                if (it == null) getString(R.string.error_an_occurred) else it.optString(
                    "message",
                    getString(R.string.error_an_occurred)
                )
            runOnUiThread { errorAlert(this, "Login", message) { } }
        }

        http.setOnConnectFailure {
            val msg = getString(R.string.unfortunately_timeout_message)
            runOnUiThread { Toast.makeText(this, msg, Toast.LENGTH_SHORT).show() }
        }
        var token = ""
        var userId = ""
        val serialLogin = config["login", "undefined"]
        if (serialLogin !== "undefined") {
            try {
                val user = JSONObject(serialLogin)
                token = user.getString("token")
                userId = user.getString("userId")
            } catch (e: JSONException) {
            }
        } else {
            config["login"] = "undefined"
            config["remember"] = "false"
            config["token"] = "undefined"
            Utils.launch(this, HomeActivity::class.java)
        }
        val requestParams: ArrayList<Pair<String, String>> = ArrayList()

        requestParams.add(Pair("ApiKey", getString(R.string.apikey)))
        requestParams.add(Pair("userId", userId))
        requestParams.add(Pair("AccessToken", token))
        http.doAsyncApiCall(requestParams)
    }

    private fun cardAdapter(response: JSONObject) {
        try {

            val cards = response.getInt("size")
            if (cards == 0) {
                val msg = getString(R.string.no_cards_to_show)
                runOnUiThread { Toast.makeText(this, msg, Toast.LENGTH_SHORT).show() }
                return
            }
            // Put cards in different format
            translateCards(response.getJSONArray("body"))
            displayCards()
        } catch (e: JSONException) {
            e.printStackTrace()
            Toast.makeText(this, getString(R.string.error_an_occurred), Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun displayCards() {
        val listRecords = listCreditCardsActivity as ExtendableListView

        listRecords.adapter = AdapterCreditCardNow(this, translatedCards)
        registerForContextMenu(listRecords)

        listRecords.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                var dialog = DialogPersonalizate()
                dialog.show(supportFragmentManager, "customDialog")
                arreglo = translatedCards[position]
            }
    }

    private fun translateCards(creditCards: JSONArray) {
        // remove everything
        translatedCards.clear()
        for (i in 0 until creditCards.length()) {
            try {
                val jsonCreditCard = creditCards.getJSONObject(i)
                val id = Integer.valueOf(jsonCreditCard.getString("cardId"))
                val lastFour = jsonCreditCard.getString("last4")
                val isDefault = jsonCreditCard.getBoolean("defaultCard")
                val network = jsonCreditCard.getString("network")
                translatedCards.add(CreditCards(isDefault, id, lastFour, network))
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_cart_add, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.btn_add_cart -> {
                val intent = Intent(this, AddCardActivity::class.java)
                intent.putExtra("title", getString(R.string.title_activity_view_credit_card))
                startActivityForResult(intent, 3)
            }
            android.R.id.home-> onBackPressed()
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == RESULT_OK) and (requestCode == 3)) {
            val string = data!!.extras!!.getString(getString(R.string.key_card))
            val aa = data.extras!!.getInt("aa")
            var intent = intent.putExtra(getString(R.string.key_card), string)
            intent = intent.putExtra("aa", aa)
            setResult(RESULT_OK, intent)
            finish()

        }
    }

    override fun returnBackCvv(cvv: String) {
        val jsonPayload = "{\"cardId\":${arreglo!!.cardId}," +
                "\"isDefault\":${arreglo!!.isDefault}," +
                "\"lastFour\":${arreglo!!.lastFour}," +
                "\"code\":${cvv}," +
                "\"network\":${arreglo!!.network}}"
        var intent = intent.putExtra(getString(R.string.key_card), jsonPayload)
        intent = intent.putExtra("aa", 1234)
        setResult(RESULT_OK, intent)
        finish()
    }

    override fun onBackPressed() {
        setResult(RESULT_CANCELED)
        finish()
    }

}
