package com.realpaymentdevelop.app.activity.order

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import berlin.volders.badger.BadgeShape
import berlin.volders.badger.Badger
import berlin.volders.badger.CountBadge
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.order.AdapterDish
import com.realpaymentdevelop.app.customviews.ExtendableListView
import com.realpaymentdevelop.app.db.DataBase
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import kotlinx.android.synthetic.main.activity_dish.*
import kotlinx.android.synthetic.main.cardview_history.*
import org.json.JSONObject

class DishActivity : JupiterNewActivity(),AdapterDish.OnClick{
    private var tipEnable = 0
    private var jsonDelivery: String? = ""
    private var nameMerchant: String? = ""
    private var locationName: String? = ""
    private var data = JsonObject()
    private var accessToken: String? = ""
    private var merchantId : String? = ""
    private var merchantLocationId:String? = ""
    private var badge:CountBadge?=null
    private var cartSize = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpContext(this)
        merchantId= intent.getStringExtra(getString(R.string.key_merchants))
        merchantLocationId = intent.getStringExtra(getString(R.string.key_localization))
        setContentView(R.layout.activity_dish)
        accessToken = intent.getStringExtra(getString(R.string.key_token))
        title = intent.getStringExtra(getString(R.string.key_title))
        tipEnable = intent.getIntExtra(getString(R.string.key_enable), -1)
        jsonDelivery = intent.getStringExtra(getString(R.string.key_json_null))
        nameMerchant = intent.getStringExtra(getString(R.string.key_name))
        locationName = intent.getStringExtra(getString(R.string.key_localization_name))
        val categories = intent.getStringExtra(getString(R.string.key_categories))
        val jsonParser: JsonParser = JsonParser()
        val itemsMenu = intent.getIntExtra(getString(R.string.key_dish), 0)

        val url = intent.getStringExtra(getString(R.string.key_url_image))
        Log.e("categories", "$categories")
        try{
            data = jsonParser.parse(categories).asJsonObject
        }catch (e:Exception){
            Log.e("exceptionDish",e.toString())
        }
        cartSize = DataBase(this).getCart(merchantId!!,merchantLocationId!!).size
        val items = buildItemsMenu(itemsMenu, data)
        setElementsOnDisplay(items, url!!)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val factory = CountBadge.Factory(this, BadgeShape.circle(0.5f, Gravity.TOP))
        val menuItem = menu?.findItem(R.id.cart_menu)
        badge = Badger.sett(menuItem!!, factory)
        badge?.count = cartSize
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_card, menu)
        val factory = CountBadge.Factory(this, BadgeShape.circle(0.5f,Gravity.TOP))
        val menuItem = menu?.findItem(R.id.cart_menu)
        badge = Badger.sett(menuItem!!, factory)
        badge?.count = cartSize
        return super.onCreateOptionsMenu(menu)

    }
    override fun onResume() {
        super.onResume()
        if(badge != null){
            val data= DataBase(this).getCart(merchantId!!,merchantLocationId!!).size
            badge?.count = data
            Log.e("TAG","NOTmenuItemNull")
        }else{
            Log.e("TAG","menuItemNull")
        }
    }

    private fun setElementsOnDisplay(items: MutableList<JSONObject>, imageUrl: String) {
        val listRecord = itemsMenuActivity
        runOnUiThread {
            listRecord.adapter = AdapterDish(this, items, imageUrl)
            val linearLayout = LinearLayoutManager(this@DishActivity)
            linearLayout.orientation = LinearLayoutManager.VERTICAL
            listRecord.layoutManager = linearLayout
            registerForContextMenu(listRecord)
        }

    }

    override fun onStop() {
        Log.e("Dish","Stop")
        tipEnable = tipEnable
        jsonDelivery = jsonDelivery
        nameMerchant = nameMerchant
        locationName = locationName
        data = data
        accessToken = accessToken
        super.onStop()

    }

    private fun buildItemsMenu(idCategory: Int, data: JsonObject): MutableList<JSONObject> {

        val items: MutableList<JSONObject> = mutableListOf()
        val itemsLocal = data.getAsJsonObject("Items")
        val keys = itemsLocal.keySet()

        keys.forEach {
            if (itemsLocal.get(it) is JsonObject) {
                val itemToSelect = JSONObject(itemsLocal.get(it).toString())
                if (itemToSelect.getJSONObject("QuickItem").get("quickCategoryId").toString()
                        .toInt() == idCategory
                    && itemToSelect.getJSONObject("QuickItem").get("available").toString() != "null"
                    && itemToSelect.getJSONObject("QuickItem").get("available").toString()
                        .toInt() == 1
                ) {
                    items.add(itemToSelect)
                }
            }
        }
        items.sortBy {
            it.getJSONObject("QuickItem").getInt("sequenceOfDisplay")
        }
        return items
    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.cart_menu -> {
                val data = DataBase(this).getCart(merchantId!!,merchantLocationId!!).size
                if(data!=0){
                    val factory = CountBadge.Factory(this,BadgeShape.circle(0.5f,Gravity.TOP))
                    val merchantId = intent.getStringExtra(getString(R.string.key_merchants))
                    val merchantLocationId = intent.getStringExtra(getString(R.string.key_localization))
                    val intent = Intent(this, ConfirmOrderActivity::class.java)
                    intent.putExtra(getString(R.string.key_json_null), jsonDelivery)
                    intent.putExtra(getString(R.string.key_name), nameMerchant)
                    intent.putExtra(getString(R.string.key_localization_name), locationName)
                    intent.putExtra(getString(R.string.key_token), accessToken)
                    intent.putExtra(getString(R.string.key_merchants), merchantId)
                    intent.putExtra(getString(R.string.key_localization), merchantLocationId)
                    intent.putExtra(getString(R.string.key_enable), tipEnable)
                    startActivity(intent)
                    Badger.sett(item, factory).count = data
                }else{
                    Toast.makeText(this,getString(R.string.not_elements), Toast.LENGTH_SHORT).show()
                }

            }
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun onClick(items:JSONObject,imageUrl: String) {
        val modifiedItemGroup = intent.getStringExtra(getString(R.string.key_products))

        val intent = Intent(this, TopicActivity::class.java)

        intent.putExtra(getString(R.string.key_json_null), jsonDelivery)
        intent.putExtra(getString(R.string.key_enable), tipEnable)
        intent.putExtra(getString(R.string.key_name), nameMerchant)
        intent.putExtra(getString(R.string.key_localization_name), locationName)
        intent.putExtra(getString(R.string.key_token), accessToken)
        intent.putExtra(getString(R.string.key_merchants), merchantId)
        intent.putExtra(getString(R.string.key_localization), merchantLocationId)
        intent.putExtra("key", items.toString())
        intent.putExtra(getString(R.string.key_products), modifiedItemGroup)
        intent.putExtra(getString(R.string.key_url_image), imageUrl)
        startActivityForResult(intent, 10 )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(resultCode){
            Activity.RESULT_OK->{
                if(requestCode == 10){
                    Log.e("BackPressedDish", "Dish" )
                    finish()
                }
            }
            Activity.RESULT_CANCELED->{}

        }

    }


}
