package com.realpaymentdevelop.app.activity.order

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import androidx.recyclerview.widget.LinearLayoutManager
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.order.AdapterListPickup
import com.realpaymentdevelop.app.customviews.ExtendableListView
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import kotlinx.android.synthetic.main.activity_selected_store.*
import org.json.JSONException
import org.json.JSONObject

class SelectedStoreActivity : JupiterNewActivity(),AdapterListPickup.onClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpContext(this)
        setContentView(R.layout.activity_selected_store)
        val locationName = intent.getStringExtra(getString(R.string.key_localization_name))
        val locationJson = intent.getStringExtra(getString(R.string.key_localization))
        title = locationName
        try {
            val locationJSON = JSONObject(locationJson)
            val array = locationJSON.getJSONArray("QuickPickupSpot")
            val listRecord = pickup_list
            val linear = LinearLayoutManager(this)
            linear.orientation = LinearLayoutManager.VERTICAL
            listRecord.layoutManager = linear
            listRecord.adapter = AdapterListPickup(context!!, array, locationJSON)
        }catch (e : JSONException){
            Log.e("ExceptionSpot",e.toString())
        }
    }

    private fun spotSelected(jsonSpotSelected: JSONObject?, locationJson:JSONObject) {
        val pickupCounter = intent.getBooleanExtra(getString(R.string.key_pickupCounter), false)
        val fullDelivery = intent.getBooleanExtra(getString(R.string.key_full_delivery), false)
        val pickupSpot = intent.getBooleanExtra(getString(R.string.key_pickupSpot), false)
        val buildingDelivery = intent.getBooleanExtra(getString(R.string.key_building_delivery),false)
        val merchantId = intent.getStringExtra(getString(R.string.key_merchants))
        val merchantName = intent.getStringExtra(getString(R.string.key_name))
        val token = intent.getStringExtra(getString(R.string.key_token))
        val merchantLocationId =
            intent.getStringExtra(getString(R.string.key_merchants_localization))
        val url = intent.getStringExtra(getString(R.string.key_url_image))
        val tipEnable = intent.getIntExtra(getString(R.string.key_enable), 0)
        val building = intent.getBooleanExtra(getString(R.string.key_building_delivery), false)
        val pickupJSON = "{\"location\":$locationJson,"+
                "\"delivery\":{" +
                "\"pickupCounter\":false," +
                "\"pickupSpot\":$pickupSpot," +
                "\"buildingDelivery\":false," +
                "\"fullDelivery\":false}," +
                "\"type\":1," +
                "\"pickupSpot\":$jsonSpotSelected}"
        val intentCounter = Intent(this, CategoriesActivity::class.java)
        intentCounter.putExtra(getString(R.string.key_merchants), merchantId)
        intentCounter.putExtra(getString(R.string.key_token), token)
        intentCounter.putExtra(getString(R.string.key_name), merchantName)
        intentCounter.putExtra(getString(R.string.key_merchants_localization), merchantLocationId)
        intentCounter.putExtra(getString(R.string.key_building_delivery),building)
        intentCounter.putExtra(getString(R.string.key_localization_name), "$title")
        intentCounter.putExtra(getString(R.string.key_url_image), url)
        intentCounter.putExtra(getString(R.string.key_enable), tipEnable)
        intentCounter.putExtra(getString(R.string.key_json_null), pickupJSON)
        intentCounter.putExtra(getString(R.string.key_localization),locationJson.toString())
        startActivity(intentCounter)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }
    override fun onBackPressed() {
        finish()
    }

    override fun listener(jsonObject: JSONObject,locationJson: JSONObject) {
        spotSelected(jsonObject, locationJson)
    }

}
