package com.realpaymentdevelop.app.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.realpaymentdevelop.app.R;
import com.realpaymentdevelop.app.ScreenTouchInterceptorEventHandler;
import com.realpaymentdevelop.app.application.RealPaymentApplication;
import com.realpaymentdevelop.app.interfaces.ActivityChangeEventListener;
import com.realpaymentdevelop.app.services.ApiParams;
import com.realpaymentdevelop.app.utils.ApiResult;
import com.realpaymentdevelop.app.utils.Config;
import com.realpaymentdevelop.app.utils.JupiterNewHttp;
import com.realpaymentdevelop.app.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

/**
 * Base activity
 */
abstract public class JupiterActivity extends AppCompatActivity implements ActivityChangeEventListener {
    // Making everything public. I have no need to protect myself from myself.

    public Activity activity;
    public Context context;
    static public Config config;
    public String userId;
    protected Resources res;
    public String apikey;
    public String token;
    public String hostname;
    public JSONObject user;
    public ProgressDialog pd = null;
    protected boolean hideProgress = false;
    public String lastEndpoint = "";

    public void setUpContext(Activity act) {
        if (act != null) {
            hostname = getResources().getString(R.string.hostname);
            Log.d("hostname", hostname);
            apikey = getResources().getString(R.string.apikey);
            context = act.getApplicationContext();
            activity = act;
            config = new Config(context);
            res = context.getResources();
        } else {
            throw new NullPointerException("Must Define an Activity to set up Context");
        }
    }

    /**
     * Get the user data from the saved JSON.
     * This would get called from the child classes as needed.
     */
    public void makeUser() {
        String serial_login = config.get("login", "undefined");
        if (serial_login != "undefined") {
            try {
                user = new JSONObject(serial_login);
                token = user.getString("token");
                userId = user.getString("userId");
            } catch (JSONException e) {
            }
        } else {
            config.set("login", "undefined");
            config.set("remember", "false");
            config.set("token", "undefined");
            Utils.launch(activity, LogInActivity.class);
        }
    }


    public void doAsyncTask(final String endpoint, final ArrayList<Pair<String, String>> in) {
        Log.d("doAsyncTask: ", "Called.");
        lastEndpoint = endpoint;
        Log.e("EndPoint", endpoint);
        if (!isNetworkConnected()) {
            errorAlert(getString(R.string.unable_to_connect), getString(R.string.network_connection_not_found));
            return;
        }

        AsyncTask<String, Void, ApiResult> task = new AsyncTask<String, Void, ApiResult>() {

            public ApiResult r = null;

            @Override
            protected void onPreExecute() {
                if (hideProgress) {
                    return;
                }
                Log.d("onPreExecute: ", "Called.");

                /*pd = new ProgressDialog(context);
                // LANG: translation
                pd.setTitle(getString(R.string.processing));
                pd.setMessage(getString(R.string.please_wait));
                pd.setCancelable(false);
                pd.setIndeterminate(true);
                pd.show();*/
            }

            @Override
            protected ApiResult doInBackground(String... arg) {
                Log.d("doInBackground: ", "Called.");

                ApiResult result = new ApiResult();
                String accessToken = config.get("token", "undefined");
                JupiterNewHttp client = new JupiterNewHttp(activity, "/api", endpoint);
                ArrayList<Pair<String, String>> params = in;

                params.add(new Pair<>(ApiParams.deviceId, config.getDeviceUUID()));
                params.add(new Pair<>(ApiParams.build, config.getBuildNumber()));
                params.add(new Pair<>(ApiParams.appId, config.getAppId()));
                params.add(new Pair<>(ApiParams.appVersion, config.getAppVersion()));
                params.add(new Pair<>(ApiParams.lang, config.getLanguage()));
                params.add(new Pair<>(ApiParams.region, config.getRegion()));

                if (!accessToken.equals("undefined")) {
                    // If we already have an access token, add it
                    Log.d("AccessToken check: ", "Exists. Adding to params.");
                    params.add(new Pair<>("AccessToken", accessToken));
                } else {
                    Log.d("AccessToken check: ", "Not found. Continuing without it.");
                }

                client.setOnSuccess(new Function1<JSONObject, Unit>() {
                    @Override
                    public Unit invoke(JSONObject it) {
                        Log.d("onSuccess: ", "Called.");
                        Log.d(endpoint + " response", it.toString());
                        // Look at the response and handle accordingly
//                            {"actionCode":"80","message":"OK","responseCode":500,"result":[],
//                            "in":{"active":true,"merchantId":"10018","name":"Rice and Dough"},"success":1}
                        int responseCode = 0;
                        String message = null;
                        int success = 9;

                        try {
                            responseCode = it.getInt("responseCode");
                            message = it.getString("message");
                            success = it.getInt("success");

                            Log.d("onSuccess: ", "success: " + success);
                            if (success == 1) {

                                // So far so good. The system at least processed our request.
                                Log.d("onSuccess: ", "responseCode: " + responseCode);
                                r = new ApiResult();
                                r.success = 1;
                                r.response = it;
                                r.responseCode = responseCode;
                                r.message = message;

                            } else {
                                // Success != 1
                                Log.d("onSuccess: ", "success: Not == 1.");
                                r = new ApiResult();
                                r.success = 0;
                                r.responseCode = 69996;
                                r.message = getString(R.string.declined);
                            }
                        } catch (JSONException e) {
                            Log.d("onSuccess: ", "JSONException caught: " + e.getMessage());
                            e.printStackTrace();
                            // JSON did not have what we needed.
                            r = new ApiResult();
                            r.success = 0;
                            r.message = "Bad JSON response";
                            r.responseCode = 69997;
                        }

                        return null;
                    }
                });

                client.setOnFailure(new Function1<JSONObject, Unit>() {
                    @Override
                    public Unit invoke(JSONObject it) {
                        Log.d("onFailure 1: ", "response: " + it);
                        r = new ApiResult();
                        r.success = -1;
                        try {
                            r.message = it.getString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });

                client.setOnConnectFailure(new Function1<JSONObject, Unit>() {
                    @Override
                    public Unit invoke(JSONObject it) {
                        //Log.d("onFailure 1: ", "response: " + it);
                        r = new ApiResult();
                        r.success = -1;
                        try {
                            r.message = it.getString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                });

                client.doApiCall(params);


                if (r != null) {
                    // ApiResult not null
                    Log.d("ApiResult: ", "Not Null.");
                    result = r;
                } else {
                    // ApiResult was null, create a generic one
                    Log.d("ApiResult: ", "Null. Creating generic error response.");
                    r = new ApiResult();
                    r.success = 0;
                    r.responseCode = 98765;
                    // LANG: translation
                    r.message = "Error. Please try again.";

                    result = r;
                }

                return result;
            } // doInBackground

            @Override
            protected void onPostExecute(ApiResult result) {
                Log.d("onPostExecute: ", "Called.");

                // Kill progress dialog spinner
                /*if (pd != null) {
                    pd.dismiss();
                }
*/
                switch (result.success) {
                    // Transaction completed (may have failed)
                    case 1:
                        if (result.responseCode == 100) {
                            doFinalCallback(result);
                        } else {
                            // Transaction was declined.
                            errorAlert(activity.getTitle().toString(), result.message);

                        }
                        break;
                    case -1:
                        errorAlert(activity.getTitle().toString(), result.message);
                        break;
                    default:  // Should never be reached
                    case 0:   // This means we got a response but the system was unable to process
                        errorAlert(activity.getTitle().toString(), getString(R.string.generic_error_message));
                }
            }
        };
        String[] params = {};
        task.execute(params);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setCurrentActivity();
        ScreenTouchInterceptorEventHandler.getInstance().startInactivityTimer();
    }

    @Override
    public void setCurrentActivity() {
        RealPaymentApplication.getInstance().setCurrentActivity(this);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        ScreenTouchInterceptorEventHandler.getInstance().handleScreenTouchInterceptorEvent();
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        ScreenTouchInterceptorEventHandler.getInstance().stopInactivityTimer();
    }

    // Use this to change screens etc.
    public abstract void doFinalCallback(ApiResult result);

    public void doFinalError() {
        Log.d("jupiterActivity", "dofinalError - Override this if you need a certain behavior");
        return;
    }

    // Check network connection
    public boolean isNetworkConnected() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void errorAlert(String title, String displayMessage) {

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity);
        builder.setTitle(title)
                .setMessage(displayMessage)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        doFinalError();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}