package com.realpaymentdevelop.app.activity

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.ShowDetailsAdapter
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import com.realpaymentdevelop.app.utils.Utils
import kotlinx.android.synthetic.main.activity_show_details_order.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.DecimalFormat

class ShowDetailsOrderActivity : JupiterNewActivity() {
    lateinit var item: String
    lateinit var jsonItem: JSONObject
    private val jsonWithElementsToShow = JSONObject()

    private val unable = "unable"
    val TAG = "ShowOrderDet"
    private val arrayWithItems = JSONArray()
    var totalForItem = 0.0
    var accessToken = ""
    var merchantId: Int? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_details_order)
        setUpContext(this)
        title = intent.getStringExtra("title")
        getContents()
        makeUser()

        if (item == unable) {
            errorAlert("Unable to process", getString(R.string.generic_error_message))
        }
    }

    override fun onResume() {
        super.onResume()
        getOrderDetails()
    }

    private fun getContents() {
        val intent = intent
        val bundle = intent.extras

        if (bundle != null) {
            item = bundle.getString("com.realpayment.app.item", unable)
            accessToken = bundle.getString("com.realpayment.app.token", unable)
            val auxMerchantId = bundle.get("com.realpayment.app.merchantId")
            if (auxMerchantId != null) {
                merchantId = auxMerchantId as Int
            }
        } else {
            Toast.makeText(this, "Error in Extras", Toast.LENGTH_SHORT).show()
        }
    }

    private fun getOrderDetails() {
        jsonItem = JSONObject(item)
        getItems()
        setOnDisplay()
    }

    private fun getItems() {

        if (jsonItem.has("items")) {
            try {
                //Log.d(TAG, jsonItem.toString())
                jsonWithElementsToShow.put("externalOrderId", jsonItem.getString("externalOrderId"))
                jsonWithElementsToShow.put("created", jsonItem.getString("created"))
                jsonWithElementsToShow.put(
                    "merchantName",
                    jsonItem.optString("merchantName", "No merchant information available")
                )
                jsonWithElementsToShow.put(
                    "locationName",
                    jsonItem.optString("merchantLocationName", "No location information available")
                )
                jsonWithElementsToShow.put("subTotal", jsonItem.getString("subTotal"))
                jsonWithElementsToShow.put("tax", jsonItem.getString("tax"))
                jsonWithElementsToShow.put("tip", jsonItem.getString("tip"))
                jsonWithElementsToShow.put("totalAmount", jsonItem.getString("totalAmount"))
                jsonWithElementsToShow.put("note", jsonItem.getString("note"))

                val items = jsonItem.getJSONObject("items")
                val keys = items.keys()
                while (keys.hasNext()) {
                    val key = keys.next() as String
                    if (items.get(key) is JSONObject) {
                        val parent = JSONObject(items.get(key).toString())
                        val jsonLocal = JSONObject()
                        totalForItem = parent.getDouble("unitPrice")
                        jsonLocal.put("quantity", parent.getInt("quantity"))
                        jsonLocal.put("name", parent.getString("name"))
                        if (parent.has("modifiers")) {
                            val namesModifiers =
                                createStringWithModifiersNames(parent.getJSONObject("modifiers"))
                            jsonLocal.put("modifiers", namesModifiers)
                        }
                        jsonLocal.put("totalItem", totalForItem)
                        arrayWithItems.put(jsonLocal)
                    }
                }
                jsonWithElementsToShow.put("items", arrayWithItems)
                Log.d(TAG, jsonItem.toString())


                Log.d("ShowDetails", jsonWithElementsToShow.toString())
            } catch (e: JSONException) {
                errorAlert("Show Order", "We are sorry an error has occurred")
            }
        }
    }

    private fun createStringWithModifiersNames(modifiers: JSONObject): String {

        val keysModifiers = modifiers.keys()
        var namesModifiers = ""
        val arrayToCreateNamesModifiers = ArrayList<String>()
        while (keysModifiers.hasNext()) {
            val keyModifiers = keysModifiers.next() as String
            if (modifiers.get(keyModifiers) is JSONObject) {
                val parentModifiers = JSONObject(modifiers.get(keyModifiers).toString())
                arrayToCreateNamesModifiers.add(parentModifiers.getString("name"))
                totalForItem += parentModifiers.getDouble("price")
            }
        }
        for (i in 0 until arrayToCreateNamesModifiers.size) {
            namesModifiers += arrayToCreateNamesModifiers[i]
            if (i != arrayToCreateNamesModifiers.size - 1) {
                namesModifiers += "\n"
            }
        }

        return namesModifiers
    }

    @SuppressLint("SetTextI18n")
    private fun setOnDisplay() {
        val mf = DecimalFormat("0.00")

        runOnUiThread {
            title_order.text =
                "${getString(R.string.order)}: ${jsonWithElementsToShow.getString("externalOrderId")}"
            name_merchant.text =
                if (jsonWithElementsToShow.getString("merchantName") != "null") jsonWithElementsToShow.getString(
                    "merchantName"
                ) else "No merchant information"
            name_location.text =
                if (jsonWithElementsToShow.getString("locationName") != "null") jsonWithElementsToShow.getString(
                    "locationName"
                ) else "No location information"
            time.text = "${
                Utils.formatDateWithYear(jsonWithElementsToShow.getString("created"))
                    .trim { it <= ' ' }
            },\n" +
                    Utils.formatTimeFromDate(jsonWithElementsToShow.getString("created"))
                        .trim { it <= ' ' }
            note.text = "Note: ${jsonWithElementsToShow.getString("note")}"
            subtotal.text = mf.format(jsonWithElementsToShow.getDouble("subTotal"))
            tax.text = mf.format(jsonWithElementsToShow.getDouble("tax"))
            tip.text = mf.format(jsonWithElementsToShow.getDouble("tip"))
            total.text = mf.format(jsonWithElementsToShow.getDouble("totalAmount"))

            //Delivery

            if (jsonItem.getString("quickPickupSpotId") != "null") {

                val quickMerchantLocationId = jsonItem.getInt("quickMerchantLocationId")
                val quickPickupSpotId = jsonItem.getInt("quickPickupSpotId")

                getSpotPickup(merchantId, quickMerchantLocationId, quickPickupSpotId)
            } else if (jsonItem.getString("buildingPickupDescription") != "null") {
                delivery_information.text =
                    "${getString(R.string.title_delivery)}: ${jsonItem.getString("buildingPickupDescription")} ${
                        getString(R.string.label_building_selected)
                    }"
            } else if (jsonItem.getString("deliveryAddress1") != "null" || jsonItem.getString("deliveryAddress2") != "null") {

                val arrayWithAddressInformation = arrayListOf(
                    jsonItem.getString("deliveryAddress1"),
                    jsonItem.getString("deliveryAddress2"),
                    jsonItem.getString("deliveryCity"),
                    jsonItem.getString("deliveryState"),
                    jsonItem.getString("deliveryZipCode"),
                    jsonItem.getString("deliveryCountry")
                )

                val address = getAddressString(arrayWithAddressInformation)

                delivery_information.text =
                    "${getString(R.string.title_delivery)}: $address ${getString(R.string.label_your_address_selected)}"
            } else {
                delivery_information.text =
                    "${getString(R.string.title_delivery)}: ${getString(R.string.label_counter_pickup)}"
            }

            val adapter =
                ShowDetailsAdapter(context!!, jsonWithElementsToShow.getJSONArray("items"))
            detail_order.adapter = adapter
            adapter.notifyDataSetChanged()

        }
    }

    @SuppressLint("SetTextI18n")
    private fun getSpotPickup(merchantId: Int?, locationId: Int, spotId: Int) {

        val http = JupiterNewHttp(this, "/quickOrderApi", "/getLocations")
        var nameSpotPickup = "No delivery information"

        http.setOnSuccess {
            val location = getLocation(it.getJSONObject("data"), locationId)
            val array = location!!.getJSONArray("QuickPickupSpot")

            for (i in 0 until array.length()) {
                val inTurn = array.getJSONObject(i)
                if (inTurn.getInt("quickPickupSpotId") == spotId) {
                    nameSpotPickup = inTurn.getString("name")
                    break
                }
            }

            runOnUiThread {
                delivery_information.text =
                    "${getString(R.string.title_delivery)}: $nameSpotPickup ${getString(R.string.label_pickup_spot_selected)}"
            }
        }
        http.setOnFailure {
            val msg = "Sorry an error as occurred"
            errorAlert("Details", msg)
        }
        http.setOnConnectFailure {
            val msg = getString(R.string.unfortunately_timeout_message)
            errorAlert("Details", msg)
        }

        if (merchantId != null) {
            val requestParams = java.util.ArrayList<Pair<String, String>>(2)
            requestParams.add(Pair("AccessToken", accessToken))
            requestParams.add(Pair("merchantId", merchantId.toString()))
            http.doAsyncApiCallMenu(requestParams)
        }
    }

    private fun getLocation(locations: JSONObject, quickMerchantLocationId: Int): JSONObject? {

        var location: JSONObject? = null

        val keys = locations.keys()
        while (keys.hasNext()) {
            val key = keys.next() as String
            if (locations.get(key) is JSONObject) {
                val parent = JSONObject(locations.get(key).toString())
                if (parent.getJSONObject("QuickMerchantLocation")
                        .optInt("quickMerchantLocationId", 0) == quickMerchantLocationId
                ) {
                    location = parent
                    break
                }
            }
        }
        return location
    }

    private fun getAddressString(array: ArrayList<String>): String {

        var address = ""

        for (i in 0 until array.size) {
            if (array[i] != "null") {
                address += array[i]
                if (i < array.size - 1) {
                    address += ", "
                }
            }
        }
        address += "."

        return address
    }

    private fun errorAlert(title: String, displayMessage: String) {

        runOnUiThread {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(title)
                .setMessage(displayMessage)
                .setCancelable(false)
                .setPositiveButton("OK") { dialog, id -> doFinalError() }
            val alert = builder.create()
            alert.show()
        }
    }

    private fun doFinalError() {
        finish()
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }
    override fun onBackPressed() {
        finish()
    }
}