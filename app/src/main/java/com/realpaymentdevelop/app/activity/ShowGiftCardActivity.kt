package com.realpaymentdevelop.app.activity

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.TextView
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.jupiter.JupiterNewActivity
import com.realpaymentdevelop.app.utils.Config
import com.realpaymentdevelop.app.utils.Utils
import kotlinx.android.synthetic.main.activity_show_gift_card.*

class ShowGiftCardActivity : JupiterNewActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = resources.getString(R.string.giftcards)
        setUpContext(this)
        setContentView(R.layout.activity_show_gift_card)
        config = Config(applicationContext)
        // Is Logged in
        val token = config["token", "undefined"]
        if (token == "undefined") {
            config["remember"] = "false"
            config["token"] = "undefined"
            Utils.launch(this, LogInActivity::class.java)
        } else {
            makeUser()
        }
        val tvName = findViewById<TextView>(R.id.tv_activity_show_merchantName)
        val tvToken = findViewById<TextView>(R.id.tokenText)
        val tvType = findViewById<TextView>(R.id.tv_activity_show_cardType)
        val tvBalance = findViewById<TextView>(R.id.tv_activity_show_cardBalance)
        val tvExpiry = findViewById<TextView>(R.id.tv_activity_show_expiry)

        val merchant = intent.getStringExtra(getString(R.string.key_merchants))
        val card = intent.getStringExtra(getString(R.string.key_card))
        val balance = intent.getStringExtra(getString(R.string.key_balance))
        val expiry = intent.getStringExtra(getString(R.string.key_expiry))
        val tokenString = intent.getStringExtra(getString(R.string.key_token))

        if(!merchant.isNullOrBlank()){
            tvName.text = merchant
            tvToken.text = tokenString
            tvType.text = card
            tvBalance.text = balance
            tvExpiry.text = expiry
        }

        try {
            val width = 400
            val height = 400
            val qwriter = QRCodeWriter()
            val qrcode = qwriter.encode("$tokenString", BarcodeFormat.QR_CODE, width, height)
            val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
            for (x in 0 until width) {
                for (y in 0 until height) {
                    bmp.setPixel(x, y, if (qrcode[x, y]) Color.BLACK else Color.WHITE)
                }
            }
            iv_activity_show_imageToken.setImageBitmap(bmp)
        } catch (e: WriterException) {
            Log.e("Error", e.toString())
        }

    }

}