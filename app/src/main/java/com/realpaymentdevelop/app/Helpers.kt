package com.realpaymentdevelop.app

import android.content.Context
import com.google.android.material.dialog.MaterialAlertDialogBuilder

fun errorAlert(context: Context, title: String, displayMessage: String, callback: () -> Unit) {
    val builder = MaterialAlertDialogBuilder(context)
    builder.setTitle(title)
        .setMessage(displayMessage)
        .setCancelable(false)
        .setPositiveButton("OK") { dialog, id -> callback() }
    val alert = builder.create()
    alert.show()
}
