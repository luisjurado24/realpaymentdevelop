package com.realpaymentdevelop.app.jupiter

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.ScreenTouchInterceptorEventHandler
import com.realpaymentdevelop.app.activity.LogInActivity
import com.realpaymentdevelop.app.application.RealPaymentApplication
import com.realpaymentdevelop.app.interfaces.ActivityChangeEventListener
import com.realpaymentdevelop.app.utils.Config
import com.realpaymentdevelop.app.utils.Utils
import org.json.JSONException
import org.json.JSONObject
import kotlin.reflect.KProperty


/**
 * Base activity
 */
abstract class JupiterNewActivity : AppCompatActivity(), ActivityChangeEventListener {
    // Making everything public. I have no need to protect myself from myself.

    var config: Config by lazy {
        Config(this)
    }
    var context: Context? = null
    var userId: String? = null
    var token: String? = null
    var user: JSONObject? = null

    //abstract fun doFinalCallback(result: ApiResult?)
    fun setUpContext(act: Activity) {
        context = act
    }

    // Check network connection
    val isNetworkConnected: Boolean
        get() {
            val connectivityManager =
                getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

    /**
     * Get the user data from the saved JSON.
     * This would get called from the child classes as needed.
     */
    open fun makeUser() {
        val serialLogin = config["login", "undefined"]
        if (serialLogin == "undefined") {
            logout()
        } else {
            try {
                user = JSONObject(serialLogin)
                token = user!!.getString("token")
                userId = user!!.getString("userId")
            } catch (e: JSONException) {
                logout()
            }

        }
    }

    fun setupParams(params: ArrayList<Pair<String, String>>): List<Pair<String, String>> {
        var params = params
        params.add(Pair("ApiKey", getString(R.string.apikey)))
        params.add(Pair("userId", userId!!))


        val accessToken: String? = config["token", "undefined"]
        if (accessToken.equals("undefined")) {
            Log.d("AccessToken check: ", "Not found. Continuing without it.")
        } else {
            // If we already have an access token, add it
            Log.d("AccessToken check: ", "Exists. Adding to params.")
            //params.put("AccessToken", accessToken)
            params.add(Pair("AccessToken", accessToken!!))
        }
        return params
    }

    fun logout() {
        config["login"] = "undefined"
        config["remember"] = "false"
        config["token"] = "undefined"
        Utils.launch(this, LogInActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpTheme()
        config = Config(this)
        /*AppCenter.start(
            application, "8592aa68-8add-4dbf-b4f6-481434e16b0c",
            Analytics::class.java, Crashes::class.java
        )*/
    }

    fun setUpTheme() {
        val configuration = resources.configuration
        when (configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_NO -> {
                setTheme(R.style.Light)
            }
            Configuration.UI_MODE_NIGHT_YES -> {
                Log.d("themeRewards", "UI_MODE_NIGHT_YES")
                setTheme(R.style.DayNight)
            }
        }

    }

    override fun setCurrentActivity() {
        RealPaymentApplication.getInstance().currentActivity = this
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        ScreenTouchInterceptorEventHandler.getInstance().handleScreenTouchInterceptorEvent()
    }

    override fun onUserLeaveHint() {
        super.onUserLeaveHint()
        ScreenTouchInterceptorEventHandler.getInstance().stopInactivityTimer()
    }

}
private operator fun Any.setValue(
        jupiterNewActivity: JupiterNewActivity,
        property: KProperty<*>,
        config: Config
) {
}
