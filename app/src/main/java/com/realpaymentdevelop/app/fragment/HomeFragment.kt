package com.realpaymentdevelop.app.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.activity.MessageActivity
import com.realpaymentdevelop.app.activity.OrderActivity
import com.realpaymentdevelop.app.activity.PerfilActivity
import com.realpaymentdevelop.app.activity.StoreCardsActivity
import com.realpaymentdevelop.app.utils.Config
import com.realpaymentdevelop.app.utils.Utils
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : Fragment() {

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val config = Config(activity!!.applicationContext)
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        val textViewName = view.findViewById<TextView>(R.id.user_name)
        textViewName.text = "${resources.getString(R.string.hello)} ${config.name}"
        val imageView = view.findViewById<CircleImageView>(R.id.civ_home_profile_image_user)
        Picasso.with(activity).load(config.avatarURL).into(imageView)
        view.civ_home_profile_image_user.setOnClickListener {
            Utils.launch(activity,PerfilActivity::class.java,"url",config.avatarURL)
        }
        view.btn_fragment_home_order.setOnClickListener{
            Utils.launch(activity,OrderActivity::class.java)

        }
        view.btn_fragment_home_message.setOnClickListener{
            Utils.launch(activity,MessageActivity::class.java)

        }
        view.btn_fragment_home_storecards.setOnClickListener {
            Utils.launch(activity, StoreCardsActivity::class.java)
        }
        return view
    }


}