package com.realpaymentdevelop.app.fragment

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.AdapterRewards
import com.realpaymentdevelop.app.customviews.ExtendableListView
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.utils.Config
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import kotlinx.android.synthetic.main.fragment_rewards.view.*
import org.json.JSONObject

class RewardsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_rewards, container, false)
        val phone = view.phone_number
        val bitMap = view.qrPhone
        val listRewards = view.listRewards
        getRewards(listRewards)
        setQRCode(phone,bitMap)
        return view
    }

    fun getRewards(listRewards: ExtendableListView) {
        val http = JupiterNewHttp(context!!, "/getPoints")
        http.setOnSuccess {
            val points = it.getJSONObject("body")
            var msg: String
            if (points.length() > 0) {
                Log.d("REWARDS", it.toString())
                val adapter = AdapterRewards(context, points)
                activity!!.runOnUiThread {
                    listRewards.adapter = adapter
                }
            } else {
                msg = "No Rewards to show"
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
            }
        }
        http.setOnFailure {
            Log.e("Error","$it")
            val msg = "Oops, something went wrong. Please try again in a few minutes."
            //message(msg)

        }

        http.setOnConnectFailure {
            val msg = getString(R.string.unfortunately_timeout_message)
            activity!!.runOnUiThread {
                errorAlert(context!!, "${activity!!.title}", msg) { }
            }
        }

        val requestParams = ArrayList<Pair<String, String>>()
        val config = Config(activity!!.applicationContext)
        val login = config.get("login","null")
        val jsonLogin = JSONObject(login)
        requestParams.add(Pair("AccessToken",jsonLogin.getString("token")))
        requestParams.add(Pair("userId", jsonLogin.getString("userId")))
        http.doAsyncApiCallMenu(requestParams)
    }

    private fun setQRCode(phone: TextView, bitMap: ImageView) {
        val config = Config(activity!!.applicationContext)
        val userName = config.name

        if (userName!!.isEmpty()) {
            bitMap.visibility = View.GONE
        }

        val width = 400
        val height = 400

        val qrwrite = QRCodeWriter()
        val qrcode = qrwrite.encode(userName, BarcodeFormat.QR_CODE, width, height)
        val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)

        for (x in 0 until width) {
            for (y in 0 until height) {
                bmp.setPixel(x, y, if (qrcode.get(x, y)) Color.BLACK else Color.WHITE)
            }
        }
        bitMap.setImageBitmap(bmp)
        phone.text = "$userName"

    }
}