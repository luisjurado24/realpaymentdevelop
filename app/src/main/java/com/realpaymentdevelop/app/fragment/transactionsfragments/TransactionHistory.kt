package com.realpaymentdevelop.app.fragment.transactionsfragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.AdapterTransaction
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import kotlinx.android.synthetic.main.fragment_transactions_show_history.*
import org.json.JSONArray
import java.util.*


class TransactionHistory : Fragment() {
    val TAG = "TransHis"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_transactions_show_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val accessToken = arguments!!.getString(getString(R.string.key_token))
        val userId = arguments!!.getString(getString(R.string.key_token))
        if (accessToken.isNullOrBlank() or userId.isNullOrBlank()) {
            Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
        } else {
            getHistory(userId,accessToken)
        }

    }

    private fun getHistory(userId: String?, accessToken: String?) {
        val params = ArrayList<Pair<String, String>>(3)
        val http = JupiterNewHttp(context!!, "/transactionHistory")
        params.add(Pair("userId", userId!!))
        params.add(Pair("AccessToken", accessToken!!))
        setText(getString(R.string.please_wait))
        central_message_history.visibility = View.VISIBLE

        http.setOnSuccess {
            Log.d(TAG, "${it.getJSONArray("body")}")
            val body = it.getJSONArray("body")
            if (body.length() > 0) {
                displayInformation(body)
            } else {
                setText(getString(R.string.generic_error_message))
            }
        }

        http.setOnFailure {
            Log.d(TAG, " Failure $it")
            val body = it!!.getJSONArray("body")
            if (body.length() > 0) {
                displayInformation(body)
            } else {
                setText(getString(R.string.generic_error_message))
            }
        }

        http.setOnConnectFailure {
            val msg = getString(R.string.unfortunately_timeout_message)
            activity!!.runOnUiThread {
                errorAlert(activity!!, "${activity!!.title}", msg) { activity!!.onBackPressed() }
            }
        }

        http.doAsyncApiCallMenu(params)
    }

    private fun displayInformation(list: JSONArray) {
        activity!!.runOnUiThread {
            try {
                if (list.length() > 0) {
                    val adapter = AdapterTransaction(context!!, list)
                    list_transactions.adapter = adapter
                    adapter.notifyDataSetChanged()
                    list_transactions.addOnLayoutChangeListener { view, a, b, c, d, e, f, g, h ->
                        central_message_history.visibility = View.GONE

                    }
                } else {

                    central_message_history.text = getString(R.string.not_elements)
                    Toast.makeText(context, getString(R.string.not_elements), Toast.LENGTH_LONG).show()
                }
            } catch (e: Exception) {
                Log.e(TAG, e.toString())
                central_message_history.text = getString(R.string.generic_error_message)

                Toast.makeText(
                    context,
                    getString(R.string.generic_error_message),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun setText(message: String) {
        activity!!.runOnUiThread {
            central_message_history.text = message
        }
    }

}