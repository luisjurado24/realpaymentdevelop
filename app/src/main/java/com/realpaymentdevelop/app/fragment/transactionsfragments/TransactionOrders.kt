package com.realpaymentdevelop.app.fragment.transactionsfragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.activity.ShowDetailsOrderActivity
import com.realpaymentdevelop.app.adapter.AdapterTransactionOrders
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import kotlinx.android.synthetic.main.fragment_transactions_show_orders.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class TransactionOrders : Fragment() {

    private val TAG = "TransOrder"

    companion object {
        private var arrayJSON = JSONArray()
        private var merchantsArray = JSONArray()
        private var orderHistoryReady = false
        private var merchantListReady = false
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_transactions_show_orders, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val accessToken = arguments!!.getString(getString(R.string.key_token))
        val userId = arguments!!.getString(getString(R.string.key_token))
        if (accessToken.isNullOrBlank() or userId.isNullOrBlank()) {
            Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
        } else {
            getOrderHistory(accessToken!!, userId!!)
            getMerchantList(accessToken)
        }

    }

    private fun getOrderHistory(token: String, userId: String) {
        val params = ArrayList<Pair<String, String>>(2)
        val http = JupiterNewHttp(context!!, "/quickOrderApi", "/getUsersOrders")
        params.add(Pair("userId", userId))
        params.add(Pair("AccessToken", token))

        setText(getString(R.string.please_wait))
        central_message_orders.visibility = View.VISIBLE

        http.setOnSuccess {
            if (it.has("body")) {
                arrayJSON = it.getJSONArray("body")
                Log.d("TransOrder", "${arrayJSON}")
                parametersSet(true, merchantListReady, token)
            } else {
                setText(getString(R.string.generic_error_message))
            }
        }

        http.setOnFailure {
            Log.d(TAG, " Failure $it")

            if (it!!.has("body")) {
                displayInformation(it.getJSONArray("body"), token)
            } else {
                setText(getString(R.string.generic_error_message))
            }
        }

        http.setOnConnectFailure {
            //central_message_orders.text = getString(R.string.unable_to_connect)
            val msg = getString(R.string.unfortunately_timeout_message)
            activity!!.runOnUiThread {
                errorAlert(activity!!, "${activity!!.title}", msg) { activity!!.onBackPressed() }
            }
        }

        http.doAsyncApiCallMenu(params)
    }


    private fun getMerchantList(token: String) {
        val http = JupiterNewHttp(activity!!, "/quickOrderApi", "/getMerchants")

        http.setOnSuccess {
            if (it.has("data")) {
                merchantsArray = it.getJSONArray("data")
                parametersSet(orderHistoryReady, true, token)
            } else {
                setText(getString(R.string.generic_error_message))
            }
        }
        http.setOnFailure {
            Log.d(TAG, " Failure $it")
            setText(getString(R.string.error_an_occurred))
        }
        http.setOnConnectFailure {
            setText(getString(R.string.unable_to_connect))

        }
        val requestParams = ArrayList<Pair<String, String>>(1)
        requestParams.add(Pair("AccessToken", token))
        http.doAsyncApiCall(requestParams)
    }

    private fun parametersSet(
        isSetOrderHistory: Boolean,
        isSetMerchantList: Boolean,
        token: String
    ) {
        orderHistoryReady = isSetOrderHistory
        merchantListReady = isSetMerchantList
        if (orderHistoryReady && merchantListReady) {
            displayInformation(arrayJSON, token)
        }
    }


    private fun displayInformation(list: JSONArray, accessToken: String) {

        activity!!.runOnUiThread {
            try {
                if (list.length() > 0) {
                    val adapter = AdapterTransactionOrders(context, list)
                    list_orders.adapter = adapter
                    list_orders.addOnLayoutChangeListener { view, L, T, R, B, Or, Ot, _, _ ->
                        central_message_orders.visibility = View.GONE
                    }
                    adapter.notifyDataSetChanged()
                    list_orders.setOnItemClickListener { adapterView, view, i, l ->
                        val intent = Intent(context, ShowDetailsOrderActivity::class.java)
                        val merchantId = getMerchantId(list.getJSONObject(i))
                        intent.putExtra("com.realpayment.app.merchantId", merchantId)
                        intent.putExtra("com.realpayment.app.item", list.getJSONObject(i).toString())
                        intent.putExtra("com.realpayment.app.token", accessToken)
                        intent.putExtra("title", list.getJSONObject(i).getString("externalOrderId"))
                        startActivity(intent)
                    }
                } else {
                    central_message_orders.text = getString(R.string.not_elements)
                    Toast.makeText(context, getString(R.string.not_elements), Toast.LENGTH_SHORT)
                        .show()
                }
            } catch (e: Exception) {
                central_message_orders.text = getString(R.string.generic_error_message)
                Toast.makeText(
                    context,
                    getString(R.string.generic_error_message),
                    Toast.LENGTH_SHORT
                ).show()
            }

        }
    }

    private fun getMerchantId(json: JSONObject): Int? {

        var merchantId: Int? = null
        if (json.has("merchantName")) {
            for (i in 0 until merchantsArray.length()) {
                val jsonMerchantInTurn = merchantsArray.getJSONObject(i).getJSONObject("Merchant")
                if (jsonMerchantInTurn.getString("name") == json.getString("merchantName")) {
                    merchantId = jsonMerchantInTurn.getInt("merchantId")
                    break
                }
            }
        }

        return merchantId
    }

    private fun setText(message: String) {
        activity!!.runOnUiThread {
            central_message_orders.text = message
        }
    }

}
