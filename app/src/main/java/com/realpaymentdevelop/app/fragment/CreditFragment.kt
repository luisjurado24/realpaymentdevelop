package com.realpaymentdevelop.app.fragment

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.AdapterView
import android.widget.ImageButton
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.activity.AddCardActivity
import com.realpaymentdevelop.app.adapter.AdapterCreditCardNow
import com.realpaymentdevelop.app.customviews.ExtendableListView
import com.realpaymentdevelop.app.data.CreditCards
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.utils.Config
import com.realpaymentdevelop.app.utils.JupiterNewHttp
import com.realpaymentdevelop.app.utils.Utils
import kotlinx.android.synthetic.main.fragment_credit.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class CreditFragment : Fragment() {
    private val TAG = "MainCreditFragment"

    private val translatedCards = ArrayList<CreditCards>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_credit, container, false)
        val button = view.findViewById<ImageButton>(R.id.btn_add_new_credit_card)
        button.setOnClickListener {
            Utils.launch(activity, AddCardActivity::class.java)
        }
        // Inflate the layout for this fragment
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val http = JupiterNewHttp(activity!!, "/listUserCards")

        http.setOnSuccess {
            Log.d("MainCreditFragment", "credit card $it")
            activity!!.runOnUiThread {
                cardAdapter(it)
            }
        }

        http.setOnFailure {
            val message = if (it == null) getString(R.string.error_an_occurred) else it.optString(
                "message",
                getString(R.string.error_an_occurred)
            )
            activity!!.runOnUiThread { errorAlert(activity!!, "${activity!!.title}", message) { } }
        }

        http.setOnConnectFailure {
            val msg = getString(R.string.unfortunately_timeout_message)
            activity!!.runOnUiThread { Toast.makeText(context, msg, Toast.LENGTH_SHORT).show() }
        }

        val user = Utils.makeUser(activity, activity)

        val requestParams: ArrayList<Pair<String, String>> = ArrayList()
        setupParams(requestParams)
        requestParams.add(Pair("userId", user.userId))
        http.doAsyncApiCallMenu(requestParams)

    }

    private fun setupParams(params: ArrayList<Pair<String, String>>) {
        val config = Config(activity!!)
        val token: String? = config.get("token", "undefined")
        params.add(Pair("AccessToken", token!!))
    }

    private fun cardAdapter(response: JSONObject) {
        try {
            val cards = response.getJSONArray("body")
            if (cards.length() == 0) {
                val msg = getString(R.string.no_cards_to_show)
                activity!!.runOnUiThread { Toast.makeText(context, msg, Toast.LENGTH_SHORT).show() }
                return
            }

            translateCards(cards)

            displayCards()
        } catch (e: JSONException) {
            e.printStackTrace()
            Toast.makeText(activity!!, getString(R.string.error_an_occurred), Toast.LENGTH_LONG).show()
        }
    }

    private fun translateCards(creditCards: JSONArray) {
        // remove everything
        translatedCards.clear()
        for (i in 0 until creditCards.length()) {
            try {
                val jsonCreditCard = creditCards.getJSONObject(i)
                val id = Integer.valueOf(jsonCreditCard.getString("cardId"))
                val lastFour =jsonCreditCard.getString("last4")
                val isDefault = jsonCreditCard.getBoolean("defaultCard")
                val network = jsonCreditCard.getString("network")
                translatedCards.add(CreditCards(isDefault,id,lastFour,network))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }

    private fun displayCards() {
        val listRecords = listCreditCardsFragment as ExtendableListView

        listRecords.adapter = AdapterCreditCardNow(activity!!, translatedCards)
        registerForContextMenu(listRecords)

        listRecords.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val row = translatedCards[position]
                // Already default, do nothing.
                if (row.isDefault) return@OnItemClickListener

                Log.d("getCreditcards", "Clicked row $position")

                sendSetDefault(row)
            }
    }

    private fun sendSetDefault(record: CreditCards) {
        val http = JupiterNewHttp(activity!!, "/selectDefaultCard")
        http.setOnSuccess {
            Log.d("MainCreditFragment", "credit card $it")
            for (i in translatedCards.indices) {
                translatedCards.get(i).isDefault = translatedCards.get(i).cardId == record.cardId
            }
            activity!!.runOnUiThread {
                displayCards()
            }
        }
        http.setOnFailure {
            val message = if (it == null) getString(R.string.error_an_occurred) else it.optString(
                "message",
                getString(R.string.error_an_occurred)
            )
            activity!!.runOnUiThread { errorAlert(activity!!, "Login", message, { }) }
        }

        val user = Utils.makeUser(activity, activity)

        val requestParams = ArrayList<Pair<String, String>>(2)
        setupParams(requestParams)
        requestParams.add(Pair("userId", user.userId))
        requestParams.add(Pair("cardId", record.cardId.toString()))
        http.doAsyncApiCall(requestParams)

    }

    private fun sendDelete(position: Int) {
        val record = translatedCards[position]
        val http = JupiterNewHttp(activity!!, "/removeCard")
        http.setOnSuccess {
            Log.d("MainCreditFragment", "credit card deleted")
            translatedCards.removeAt(position)

            activity!!.runOnUiThread {
                displayCards()
            }
        }
        http.setOnFailure {
            val message = if (it == null) getString(R.string.error_an_occurred) else it.optString(
                "${activity!!.title}",
                getString(R.string.error_an_occurred)
            )
            activity!!.runOnUiThread { errorAlert(activity!!, "${activity!!.title}", message, { }) }
        }

        val user = Utils.makeUser(activity, activity)
        val requestParams = java.util.ArrayList<Pair<String, String>>(2)
        setupParams(requestParams)
        requestParams.add(Pair("userId", user.userId))
        requestParams.add(Pair("cardId", record.cardId.toString()))
        http.doAsyncApiCall(requestParams)

    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        Log.d(TAG, "onCreateContextMenu: ")
        super.onCreateContextMenu(menu, v, menuInfo)
        if (v.id == R.id.listCreditCardsFragment) {
            val inflater = activity!!.menuInflater
            inflater.inflate(R.menu.menu_card_credit_context, menu)
        }
        Log.d(TAG, resources.getResourceEntryName(v.id))
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
        val position = info.position

        Log.d(TAG, "onContextItemSelected: $position")
        when (item.itemId) {
            R.id.action_default -> {
                // read stuff here
                sendSetDefault(translatedCards.get(position))
                return true
            }
            R.id.action_delete -> {
                // delete stuff here
                sendDelete(position)
                return true
            }

            else -> return super.onContextItemSelected(item)
        }
    }


}