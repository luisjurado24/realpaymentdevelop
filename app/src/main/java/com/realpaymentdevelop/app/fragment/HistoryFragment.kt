package com.realpaymentdevelop.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.PageAdaterTransaction
import com.realpaymentdevelop.app.fragment.transactionsfragments.TransactionHistory
import com.realpaymentdevelop.app.fragment.transactionsfragments.TransactionOrders
import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.android.synthetic.main.fragment_history.view.*

class HistoryFragment : Fragment() {

    private val fragHistory = TransactionHistory()
    private val fragOrders = TransactionOrders()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_history, container, false)
        val viewPager = view.findViewById<ViewPager>(R.id.vp_transaction_fragment_security)
        if(viewPager != null){
            val token = arguments!!.getString(getString(R.string.key_token))
            val userId = arguments!!.getString("userId")
            val bundle = Bundle()

            bundle.putString(resources.getString(R.string.key_token), token)
            bundle.putString("userId", userId)
            fragHistory.arguments = bundle
            fragOrders.arguments = bundle

            viewPager.adapter = PageAdaterTransaction(activity!!.supportFragmentManager, addFragments())
            view.tl_transaction_fragment.setupWithViewPager(viewPager)
            view.tl_transaction_fragment.getTabAt(0)!!.text =
                getString(R.string.button_text_transaction_history)
            view.tl_transaction_fragment.getTabAt(1)!!.text = getString(R.string.button_text_order_history)
        }else{
            Toast.makeText(activity,"error", Toast.LENGTH_SHORT).show()
        }
        return view
    }

    private fun addFragments(): ArrayList<Fragment> {
        val fragments: ArrayList<Fragment> = ArrayList()
        fragments.add(fragHistory)
        fragments.add(fragOrders)
        return fragments
    }

}