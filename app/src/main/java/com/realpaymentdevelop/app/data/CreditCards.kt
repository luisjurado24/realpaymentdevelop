package com.realpaymentdevelop.app.data

import android.os.Parcel
import android.os.Parcelable

data class CreditCards(var isDefault: Boolean, val cardId: Int, val lastFour: String?, val network: String?):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readByte() != 0.toByte(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (isDefault) 1 else 0)
        parcel.writeInt(cardId)
        parcel.writeString(lastFour)
        parcel.writeString(network)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CreditCards> {
        override fun createFromParcel(parcel: Parcel): CreditCards {
            return CreditCards(parcel)
        }

        override fun newArray(size: Int): Array<CreditCards?> {
            return arrayOfNulls(size)
        }
    }
}
