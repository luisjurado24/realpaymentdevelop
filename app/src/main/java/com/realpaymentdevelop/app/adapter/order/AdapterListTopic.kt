package com.realpaymentdevelop.app.adapter.order

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.db.DataBase
import com.realpaymentdevelop.app.errorAlert
import com.realpaymentdevelop.app.interfaces.SendDataToRemoveOrEdit
import kotlinx.android.synthetic.main.adapter_card_list.view.*
import java.text.DecimalFormat

class AdapterListTopic(
    private val context: Context,
    private val jsonItemExtras: ArrayList<JsonObject>,
    private val iSendDataToRemoveOrEdit: SendDataToRemoveOrEdit
) : BaseAdapter() {
    override fun getCount(): Int {
        return jsonItemExtras.size
    }

    override fun getItem(p0: Int): Any {
        return jsonItemExtras[p0]
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    @SuppressLint("SetTextI18n")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val mInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var list = p1
        val mf = DecimalFormat("0.00")
        val holder: ViewHolder

        if (p1 == null) {
            list = mInflater.inflate(R.layout.adapter_card_list, null)
            holder = ViewHolder()

            holder.quantity = list.total_quantity
            holder.itemName = list.item_selected
            holder.extras = list.extras_selected
            holder.total = list.total_item_extras
            holder.remove = list.remove

            list?.tag = holder

        } else {
            holder = list?.tag as ViewHolder
        }

        val arrayJson = jsonItemExtras[p0].asJsonObject
        holder.itemName!!.text = arrayJson.get("Item").asJsonObject.get("name").asString
        holder.quantity!!.text = arrayJson.get("Quantity").asInt.toString() + "X"
        holder.total!!.text = "$${mf.format(arrayJson.get("TotalCost").asDouble)}"
        holder.extras!!.visibility = View.VISIBLE
        if (arrayJson.get("Extras") != null) {
            if (arrayJson.get("Extras").asJsonArray.size() > 0) {
                holder.extras!!.text = listExtras(arrayJson.get("Extras").asJsonArray)
            }
        } else {
            holder.extras!!.visibility = View.GONE
        }

        holder.remove!!.setOnClickListener {
            val id = arrayJson.get("IdLocal").asString
            val db = DataBase(context)
            val result = db.deleteJson(id)
            when (result) {
                1 -> iSendDataToRemoveOrEdit.setValuesOnExpressOrder()
                else -> errorAlert(context, "title", "error $result", {})
            }
        }

        return list!!
    }

    private fun listExtras(array: JsonArray): String {
        var extras = ""

        for (x in 0 until array.size()) {

            extras += array.get(x).asJsonObject.get("name").asString
            if (x != array.size() - 1) {
                extras += "\n"
            }
        }
        return extras
    }

    inner class ViewHolder {

        var quantity: TextView? = null
        var itemName: TextView? = null
        var total: TextView? = null
        var remove: TextView? = null
        var edit: TextView? = null
        var extras: TextView? = null

    }
}