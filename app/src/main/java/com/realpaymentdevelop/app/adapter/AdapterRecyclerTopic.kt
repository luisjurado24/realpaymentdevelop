package com.realpaymentdevelop.app.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.net.http.Headers
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.order.AdapterTopic
import com.realpaymentdevelop.app.customviews.ExtendableListView
import com.realpaymentdevelop.app.interfaces.sendDataFragments
import java.util.*

class AdapterRecyclerTopic(
    private val context: Context, private val headers: JsonArray, private val modifiers: JsonArray,
    private val extras: JsonArray?
) : RecyclerView.Adapter<AdapterRecyclerTopic.viewHolder>() {

    inner class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val extendableListView: ExtendableListView = itemView.findViewById(R.id.extendList)
        val mustPick: TextView = itemView.findViewById(R.id.must_pick)
        val quantity: TextView = itemView.findViewById(R.id.quantity)
        val quickModifierGroup: TextView = itemView.findViewById(R.id.quick_modifier_group)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.card_view_topics, parent, false)
        return viewHolder(view)
    }

    override fun getItemCount(): Int {
        return headers.size()
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val list = headers.get(position).asJsonObject
        val maxSelected = list.get("maxSelect").asInt
        val minSelected = list.get("minSelect").asInt

        holder.quickModifierGroup.text = list.get("name").asString
        if (maxSelected > 0) {
            holder.quantity.text = "0/$maxSelected"
            if (minSelected > 0) {
                holder.mustPick.text =
                    context.getString(R.string.message_must_pick) + " " + maxSelected
                holder.mustPick.setTextColor(Color.RED)

            } else {
                holder.mustPick.text =
                    context.getString(R.string.message_pick_up) + " " + maxSelected

            }
        } else if (minSelected > 0) {
            holder.quantity.text = "0/$minSelected"
            holder.mustPick.text =
                context.getString(R.string.message_must_pick_at) + " " + minSelected
            holder.mustPick.setTextColor(Color.RED)

        } else {
            holder.quantity.text = "0/-"
            holder.mustPick.text = context.getString(R.string.message_pick_any)
        }
        val quickModifider = list["quickModifierGroupId"].asString
        val adapter = AdapterTopic(context, modifiers[position].asJsonArray, holder,
            Pair(minSelected, maxSelected),extras!!, quickModifider)
        holder.extendableListView.adapter = adapter
    }

}