package com.realpaymentdevelop.app.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.utils.RoundedTransformation
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat

class AdapterStoreGift(val context: Context, private val giftCards: JSONArray) : BaseAdapter() {
    override fun getCount(): Int {
        return giftCards.length()
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var list: View? = null
        val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (convertView == null) {
            try {
                list = View(context)
                list = inflater.inflate(R.layout.card_view_store_fragment, null)
                val record: JSONObject = giftCards.getJSONObject(position)
                Log.d("Giftcard", record.toString())

                // Pull the values from the json
                val merchantName = record.getString("merchantName")
                val token = record.getString("token")
                // LANG: number formatting
                @SuppressLint("DefaultLocale") val balance = "$" + String.format(
                    "%.2f",
                    record.getDouble("balance")
                )
                var date = record.getString("issueDate")
                val staticHostname  = context.getString(R.string.static_hostname)
                val giftCardImagePath = record.getString("giftcardImagePath")
                val giftCardImageName = record.getString("giftcardImageName")
                val completeImagePath: String = staticHostname + giftCardImagePath + giftCardImageName

                if (date == "null") {
                    date = ""
                } else {
                    val sdf = SimpleDateFormat("yyyy-MM-dd")
                    try {
                        val dDate = sdf.parse(date)
                        val dateFormat = DateFormat.getMediumDateFormat(context)
                        date = dateFormat.format(dDate)
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }
                }

                // Grab the required views
                val balanceTextView = list.findViewById<TextView>(R.id.tv_card_view_store_price)
                val merchantNameTextView =
                    list.findViewById<TextView>(R.id.tv_card_view_store_title)
                val merchantImageView = list.findViewById<ImageView>(R.id.iv_card_view_store_logo)
                val tvErrorLetter = list.findViewById<TextView>(R.id.tv_card_view_store_logo)
                val textViewDate = list.findViewById<TextView>(R.id.tv_card_view_store_date)

                // Set the text views with the values pulled from the json
                merchantNameTextView.text = merchantName
                balanceTextView.text = balance
                textViewDate.text = date
                Picasso.with(context)
                    .load(completeImagePath)
                    .transform(RoundedTransformation(20, 0))
                    .placeholder(R.drawable.bg_btn_green)
                    .error(R.drawable.bg_btn_green)
                    .into(merchantImageView, object : Callback {
                        val tv: TextView = tvErrorLetter
                        override fun onSuccess() {}
                        override fun onError() {
                            //mImageView.setVisibility(View.GONE);
                            Log.d("Picasso", "Error loading image")
                            tv.text = merchantName.substring(0, 1)
                        }
                    })
            } catch (e: JSONException) {
                Log.d("Exception transaction", e.message!!)
                // TODO: Implement
            }
        } else {
            list = convertView
            // TODO: Implement
        }
        return list
    }

    /*inner class StoreGiftViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        private val textViewTitle = itemView.findViewById<TextView>(R.id.tv_card_view_store_title)
        private val textViewDate = itemView.findViewById<TextView>(R.id.tv_card_view_store_date)
        private val textViewPrice = itemView.findViewById<TextView>(R.id.tv_card_view_store_price)
        private val imageViewLogo = itemView.findViewById<ImageView>(R.id.iv_card_view_store_logo)

        fun bin(giftCards: GiftCards,context: Context) {
            textViewTitle.text = giftCards.title
            textViewDate.text = giftCards.date
            textViewPrice.text = giftCards.price.toString()
            imageViewLogo.setImageResource(giftCards.photo)
            itemView.setOnClickListener {
                Toast.makeText(context,"HOLA ${giftCards.title}", Toast.LENGTH_SHORT).show()
            }
            
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreGiftViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        return StoreGiftViewHolder(layoutInflater.inflate(R.layout.card_view_store_fragment,parent,false))
    }

    override fun getItemCount(): Int {
        return giftCards.size
    }

    override fun onBindViewHolder(holder: StoreGiftViewHolder, position: Int) {
        val item = giftCards[position]
        holder.bin(item,context)
    }*/
}