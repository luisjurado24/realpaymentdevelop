package com.realpaymentdevelop.app.adapter.order

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.realpaymentdevelop.app.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONObject

class AdapterDish(private val context: Context, private val records: MutableList<JSONObject>, private val imageUrl: String) : RecyclerView.Adapter<AdapterDish.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val image: CircleImageView = itemView.findViewById<CircleImageView>(R.id.iv_card_view_merchants_logo)
        val name: TextView = itemView.findViewById<TextView>(R.id.tv_card_view_merchants_title)
        val layout = itemView.findViewById<ConstraintLayout>(R.id.layout_card_view_merchant)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_view_merchants,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val nameString = records[position].getJSONObject("QuickItem").getString("name")
        val path = records[position].getJSONObject("Image").getString("path")
        val nameUrl = records[position].getJSONObject("Image").getString("name")
        val imageRoute = "$path/$nameUrl"

        if (path.isNullOrBlank() or nameUrl.isNullOrBlank()) {
            Picasso.with(context).load(imageUrl)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .resize(250, 250)
                .centerCrop()
                .into(holder.image)
        } else {
            Picasso.with(context).load(context.getString(R.string.static_hostname) + imageRoute)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .resize(250, 250)
                .centerCrop()
                .into(holder.image, object : Callback {
                    override fun onSuccess() {
                    }

                    override fun onError() {
                        Picasso.with(context).load(imageUrl)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            .resize(250, 250)
                            .centerCrop()
                            .into(holder.image)
                    }

                })
        }
        holder.name.text = nameString
        holder.layout.setOnClickListener {
            if(context is OnClick){
                context.onClick(records[holder.adapterPosition],imageUrl)
            }
        }
    }
    interface OnClick{
        fun onClick(item:JSONObject,imageUrl: String)
    }

    override fun getItemCount(): Int {
        return records.size
    }
    /*override fun getCount(): Int {
        return records.size
    }

    override fun getItem(p0: Int): Any {
        return records.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        var list = p1
        if (p1 == null) {
            val inflater: LayoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            list = inflater.inflate(R.layout.card_view_merchants, null)
        }
        val nameString = records[p0].getJSONObject("QuickItem").getString("name")
        val path = records[p0].getJSONObject("Image").getString("path")
        val nameUrl = records[p0].getJSONObject("Image").getString("name")
        val imageRoute = "$path/$nameUrl"
        Log.e("ImageRoute","$imageRoute")
        val image = list?.findViewById<CircleImageView>(R.id.iv_card_view_merchants_logo)
        val name = list?.findViewById<TextView>(R.id.tv_card_view_merchants_title)

        if (path.isNullOrBlank() or nameUrl.isNullOrBlank()) {
            Picasso.with(context).load(imageUrl)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .resize(250, 250)
                .centerCrop()
                .into(image)
        } else {
            Picasso.with(context).load(context.getString(R.string.static_hostname) + imageRoute)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .resize(250, 250)
                .centerCrop()
                .into(image, object : Callback {
                    override fun onSuccess() {
                    }

                    override fun onError() {
                        Picasso.with(context).load(imageUrl)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.placeholder)
                            .resize(250, 250)
                            .centerCrop()
                            .into(image)
                    }

                })
        }
        name?.text = nameString

        return list!!
    }*/

}

