package com.realpaymentdevelop.app.adapter.order

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.adapter.AdapterRecyclerTopic
import com.realpaymentdevelop.app.interfaces.sendDataFragments
import kotlinx.android.synthetic.main.card_view_topic.view.*
import kotlin.math.max

class AdapterTopic(
    private val context: Context, private val array: JsonArray,
    private val view: AdapterRecyclerTopic.viewHolder,
    private val pair: Pair<Int, Int>, private val extras: JsonArray?, private val quickModifider: String
) : BaseAdapter() {

    @SuppressLint("SetTextI18n")
    private fun addItem(item: JsonObject?, total: Double) {
        extras?.add(item)
        var count =0
        extras?.forEach {
            val json = it.asJsonObject
            if(json["quickModifierGroupId"].asString== quickModifider){
                count +=1
            }
        }
        val maxSelect = pair.second
        val minSelect = pair.first
        if(maxSelect>0){
            view.quantity.text = "$count/$maxSelect"
            if(minSelect>0){

                view.quantity.text = "$count/$maxSelect"
            }else{
                view.quantity.text = "$count/$maxSelect"
            }
        }else{
            if(minSelect>0){
                view.quantity.text = "$count/$minSelect"
            }else{
                view.quantity.text = "$count/-"
            }
        }

        val isNecesary = minSelect>0 && maxSelect>0

        if(isNecesary){
            if (pair.second == count) {

                view.itemView.alpha = 0.5F
            } else {
                view.itemView.alpha = 1F

            }
        }else{
            if (pair.second == count) {

                view.itemView.alpha = 0.5F
            } else {
                view.itemView.alpha = 1F
            }
        }

        if (context is sendDataFragments) {
            context.sendDatatoAdapter(total,true, extras!!)
        }
        //notifyDataSetChanged()
    }

    private fun deleteItem(item: JsonObject, total: Double) {
        extras?.remove(item)
        var count =0
        extras?.forEach {
            val json = it.asJsonObject
            if(json["quickModifierGroupId"].asString== quickModifider){
                count +=1
            }
        }
        val maxSelect = pair.second
        val minSelect = pair.first

        if(maxSelect>0){
            if(minSelect>0){
                view.quantity.text = "$count/$maxSelect"
            }else{
                view.quantity.text = "$count/$maxSelect"
            }
        }else{
            if(minSelect>0){
                view.quantity.text = "$count/$minSelect"
            }else{
                view.quantity.text = "$count/-"
            }
        }

        if(pair.second>0){
            if (pair.second == count) {
                view.itemView.alpha = 0.5F
            } else {
                view.itemView.alpha = 1F
            }
        }else{
            view.itemView.alpha = 1F
        }

        if (context is sendDataFragments) {
            context.sendDatatoAdapter(total, false, extras!!)
        }
    }

    override fun getCount(): Int {
        return array.size()
    }

    override fun getItem(p0: Int): Any? {
        return array.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }


    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var list: View? = null

        if (list == null) {
            try {

                val json = array.get(position).asJsonObject

                val inflater: LayoutInflater =
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                list = View(context)
                list = inflater.inflate(R.layout.card_view_topic, null)


                list?.extra?.text = json.get("name").asString
                if (json.get("available").asString != "0") {
                    list?.price?.text = "$" + json.get("price").asString
                    list?.select?.isEnabled = true

                } else {
                    list?.price?.text = context.getString(R.string.not_available)
                    list?.select?.isEnabled = false
                }

                list?.select?.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        val json = array[position].asJsonObject
                        addItem(json, json.get("price").asString.toDouble())
                    } else {
                        deleteItem(json, json.get("price").asString.toDouble())
                    }
                }
            } catch (e: Exception) {
                /*list?.price?.text = context.getString(R.string.not_available)
                list?.select?.isEnabled = false*/
            }
        } else {
            list = convertView
        }

        return list
    }

}