package com.realpaymentdevelop.app.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.realpaymentdevelop.app.R
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class AdapterMerchantOrder(private val mContext: Context, val records: JSONArray) :
    RecyclerView.Adapter<AdapterMerchantOrder.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.findViewById<CircleImageView>(R.id.iv_card_view_merchants_logo)
        val text = itemView.findViewById<TextView>(R.id.tv_card_view_merchants_title)
        val layout = itemView.findViewById<ConstraintLayout>(R.id.layout_card_view_merchant)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.card_view_merchants, parent, false)
        return ViewHolder(view)
    }

    interface onClick {
        fun merchantSelect(selected: JSONObject)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            val record: JSONObject? = records.getJSONObject(position)
            var merchant = record!!.getJSONObject("Merchant").getString("name")
            val imageName = record.getJSONObject("Image").optString("name")
            val imagePath = record.getJSONObject("Image").optString("path")
            val spliLocalization = merchant.split(" ")
            if (spliLocalization.size > 3) {
                merchant = ""
                spliLocalization.forEachIndexed { index, s ->
                    merchant += if (index % 3 == 0) {
                        "$s\n"
                    } else {
                        "$s "
                    }
                }
            }
            val url = mContext.getString(R.string.static_hostname) + imagePath + imageName

            if (imageName == "null" || imagePath == "null") {
                holder.image.setImageResource(R.drawable.placeholder)
            } else {
                Picasso.with(mContext).load(url).placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .resize(250, 250)
                    .centerCrop()
                    .into(holder.image)
            }

            holder.text.text = merchant
            holder.layout.setOnClickListener {
                if (mContext is onClick) {
                    mContext.merchantSelect(record)
                }
            }

        } catch (e: JSONException) {
            Log.e("Error", "OrderMerchantAdapter.kt")
        }

    }

    override fun getItemCount(): Int {
        return records.length()
    }


    /*override fun getCount(): Int {
        return records.length()
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }
    override fun getItem(p0: Int): Any? {
        return null
    }


    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View? {
        var list: View?=null
        if(p1 == null){
            try {
                list= LayoutInflater.from(mContext).inflate(R.layout.card_view_merchants, null, false)
                val record: JSONObject?=records.getJSONObject(p0)
                var merchant = record!!.getJSONObject("Merchant").getString("name")
                val imageName = record.getJSONObject("Image").optString("name")
                val imagePath = record.getJSONObject("Image").optString("path")
                *//*val spliLocalization = merchant.split(" ")
                if(spliLocalization.size>3){
                    merchant=""
                    spliLocalization.forEachIndexed { index, s ->
                        merchant += if(index%3==0 ){
                            "$s\n"
                        }else{
                            "$s "
                        }
                    }
                }*//*
                val url = mContext!!.getString(R.string.static_hostname) + imagePath + imageName
                val image = list!!.findViewById<CircleImageView>(R.id.iv_card_view_merchants_logo)
                if(imageName=="null"|| imagePath=="null"){
                    image.setImageResource(R.drawable.placeholder)
                }else{
                    Picasso.with(mContext).load(url).placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .resize(250, 250)
                        .centerCrop()
                        .into(image)
                }
                val text = list.findViewById<TextView>(R.id.tv_card_view_merchants_title)
                text.text=merchant

            }catch (e: JSONException){
                Log.e("Error","OrderMerchantAdapter.kt")
            }
        }else{
            list=p1
        }
        return list
    }*/
}