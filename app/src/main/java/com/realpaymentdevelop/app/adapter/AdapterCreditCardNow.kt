package com.realpaymentdevelop.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.data.CreditCards
import com.realpaymentdevelop.app.utils.RoundedTransformation
import com.squareup.picasso.Picasso

class AdapterCreditCardNow(private val context: Context,private val arrayList: ArrayList<CreditCards>): BaseAdapter() {

    override fun getCount(): Int {
        return arrayList.size
    }

    override fun getItem(position: Int): Any? {
        return arrayList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(row: Int, convertView: View?, parent: ViewGroup?): View? {
        var list: View? = null
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (convertView == null) {
            list = inflater.inflate(R.layout.card_view_credit_card, null)
            val creditCard: CreditCards = arrayList.get(row)
            // Pull the values from the json
            val cardLastFour: String = creditCard.lastFour!!
            val isDefault: Boolean = creditCard.isDefault
            val network: String = creditCard.network!!
            val cardId: Int = creditCard.cardId
            var networkFullName = "Credit card"
            var creditCardImagePath = "other_logo_badge"
            when (network) {
                "VISA" -> {
                    networkFullName = "Visa"
                    creditCardImagePath = "visa_logo_badge"
                }
                "AMEX" -> {
                    networkFullName = "American Express"
                    creditCardImagePath = "amex_logo_badge"
                }
                "DNRS" -> networkFullName = "Diners"
                "MSTR" -> {
                    networkFullName = "Master Card"
                    creditCardImagePath = "mstr_logo_badge"
                }
                "DCVR" -> {
                    networkFullName = "Discover Card"
                    creditCardImagePath = "dcvr_logo_badge"
                }
                "UKWN" -> {
                }
                else -> {
                }
            }

            // Grab the required views
            val networkNameTextView = list!!.findViewById<TextView>(R.id.tv_card_view_credit_text)
            val creditCardTextView = list.findViewById<TextView>(R.id.tv_card_view_credit_network)
            val networkImageView = list.findViewById<ImageView>(R.id.iv_card_view_credit_type)

            // Set the text views with the values pulled from the json
            creditCardTextView.text = cardLastFour
            networkNameTextView.text = networkFullName
            val creditCardImageId: Int = context.resources
                .getIdentifier(creditCardImagePath, "drawable", context.packageName)

            // Get and set the giftcard image
            Picasso.with(context)
                .load(creditCardImageId)
                .transform(RoundedTransformation(20, 0))
                .placeholder(R.drawable.other_logo_badge)
                .error(R.drawable.other_logo_badge)
                .into(networkImageView)

            if (isDefault) {
                val ivDefault = list.findViewById<ImageView>(R.id.iv_card_view_credit_default)
                ivDefault.setImageResource(R.drawable.ic_baseline_check_24)
            }
        } else {
            list = convertView
        }
        return list
    }
}