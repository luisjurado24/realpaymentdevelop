package com.realpaymentdevelop.app.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.realpaymentdevelop.app.R
import org.json.JSONArray
import kotlinx.android.synthetic.main.adapter_show_details_order_list.view.*
import java.text.DecimalFormat

class ShowDetailsAdapter(private val context: Context, private val items: JSONArray) : BaseAdapter() {

    @SuppressLint("SetTextI18n")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View? {
        var list = p1
        val holder: ViewHolder
        val mf = DecimalFormat("0.00")
        val mInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if (p1 == null) {
            list = mInflater.inflate(R.layout.adapter_show_details_order_list, null)
            holder = ViewHolder()
            holder.quantity = list.show_details_quantity
            holder.itemName = list.show_details_name
            holder.extras = list.show_details_extras
            holder.total = list.show_details_total
            list?.tag = holder
        } else {
            holder = list?.tag as ViewHolder
        }

        val inTurn = items.getJSONObject(p0)
        holder.quantity!!.text = "${inTurn.getString("quantity")}x"
        holder.itemName!!.text = inTurn.getString("name")
        holder.extras!!.visibility = View.VISIBLE

        if (inTurn.has("modifiers")) {
            holder.extras!!.text = inTurn.getString("modifiers")
        } else {
            holder.extras!!.visibility = View.GONE
        }
        holder.total!!.text = "$${mf.format(inTurn.getDouble("totalItem"))}"

        return list
    }

    override fun getItem(p0: Int): Any {
        return items.getJSONObject(p0)
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return items.length()
    }

    private class ViewHolder {

        var quantity: TextView? = null
        var itemName: TextView? = null
        var total: TextView? = null
        var extras: TextView? = null

    }

}




