package com.realpaymentdevelop.app.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.utils.Utils
import org.json.JSONArray
import org.json.JSONException
import java.text.DecimalFormat
import java.text.NumberFormat

class AdapterTransaction(private val context: Context, private val records: JSONArray)  : BaseAdapter(){

    var numberFormatter: NumberFormat = DecimalFormat("#0.00")
    var amount: String? = null

    override fun getCount(): Int {
        return records.length()
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var list: View? = null
        val inflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (convertView == null) {
            try {
                list = View(context)
                list = inflater.inflate(R.layout.cardview_history, null)
                val record = records.getJSONObject(Integer.valueOf(position))

                // Grab the values we need
                var merchantName = record.getJSONObject("Merchant").getString("name").toString()
                val transactionObject = record.getJSONObject("Transaction")
                val category = record.optJSONObject("0")
                val typeObj = record.getJSONObject("TransactionType")
                val createdDate = transactionObject.getString("created")
                val amountString = transactionObject.getString("amount")
                val typeName = typeObj.optString("name", "")
                val resourceId: Int
                resourceId = if (category == null) {
                    1 // default
                } else {
                    when (category.getInt("category")) {
                        1 -> 1
                        2 -> 2
                        else -> 1
                    }
                }
                amount = if (amountString != "null" && !amountString.isEmpty()) {
                    val amountDouble = java.lang.Double.valueOf(amountString)
                    if (resourceId == 2) {
                        val dollarFormatter: NumberFormat = DecimalFormat("#,###,##0")
                        dollarFormatter.format(Math.abs(amountDouble))
                    } else {
                        val dollarFormatter: NumberFormat = DecimalFormat("$#,###,##0.00")
                        dollarFormatter.format(Math.abs(amountDouble))
                    }
                } else {
                    "Error"
                }
                val ivIcon = list.findViewById<View>(R.id.typeIcon) as ImageView
                ivIcon.setImageResource(R.mipmap.paycard_transactionsa_foreground)
                val createdArray = createdDate.split(" ".toRegex()).toTypedArray()
                val createdTime = createdArray[1]

                // Grab the view elements we'll need
                val mMerchant = list.findViewById<View>(R.id.merchant) as TextView
                val mAmount = list.findViewById<View>(R.id.amount) as TextView
                val tvType = list.findViewById<View>(R.id.transactionName) as TextView
                val transactionTime = list.findViewById<View>(R.id.transactionTime) as TextView
                val spliLocalization = merchantName.split(" ")
                // Set the data
                // LANG: translation and date formatting
                if(spliLocalization.size>3){
                    merchantName=""
                    spliLocalization.forEachIndexed { index, s ->
                        merchantName += if(index%3==0 ){
                            "$s\n"
                        }else{
                            "$s "
                        }
                    }
                }
                mMerchant.text = merchantName // Seriously? Refactor this.
                mAmount.text = amount
                tvType.text = typeName
                transactionTime.text = Utils.formatDateWithYear(createdDate).trim()
            } catch (e: JSONException) {
                Log.d("Exception transaction", e.message!!)
            }
        } else {
            list = convertView
        }
        return list
    }
}