package com.realpaymentdevelop.app.adapter.order

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.realpaymentdevelop.app.R
import org.json.JSONArray
import org.json.JSONObject

class AdapterListPickup(private val context: Context,private  val spotArray:JSONArray, private val locationJson:JSONObject) :
    RecyclerView.Adapter<AdapterListPickup.viewHolder>() {

    inner class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val spotSelected = itemView.findViewById<TextView>(R.id.spotSelected)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val list =
            LayoutInflater.from(context).inflate(R.layout.adapter_pickup_list, parent, false)
        return viewHolder(list)
    }

    override fun getItemCount(): Int {
        return spotArray.length()
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val nameString = spotArray.getJSONObject(position).getString("displayName")
        val descriptionString = spotArray.getJSONObject(position).getString("description")

        if (nameString != "null" && descriptionString != "null") {
            holder.spotSelected.text = "$nameString \n$descriptionString"
        }
        holder.itemView.setOnClickListener {
            if(context is onClickListener){
                context.listener(spotArray.getJSONObject(position),locationJson)
            }
        }
    }

    interface onClickListener{
        fun listener(jsonObject: JSONObject,locationJson:JSONObject)
    }




}