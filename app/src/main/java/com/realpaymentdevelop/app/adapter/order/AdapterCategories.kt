package com.realpaymentdevelop.app.adapter.order

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.realpaymentdevelop.app.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONObject

class AdapterCategories(
    val context: Context, private val records: MutableList<JSONObject>,
    private val merchantImage: String
) : RecyclerView.Adapter<AdapterCategories.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val name = itemView.findViewById<TextView>(R.id.tv_category_name)
        val image = itemView.findViewById<CircleImageView>(R.id.civ_category_icon)
        val layout = itemView.findViewById<ConstraintLayout>(R.id.layout_category_select)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.card_category, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val enabled = records[position].getJSONObject("QuickCategory").getBoolean("enabled")
        if (enabled){
            val imageRoute = "${
            records[position].getJSONObject("Image").getString("path")
            }/${records[position].getJSONObject("Image").getString("name")}"

            if (imageRoute.isNullOrEmpty()) {
                Picasso.with(context).load(merchantImage)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .resize(250, 250)
                    .centerCrop()
                    .into(holder.image)
            } else {
                Picasso.with(context).load(context.getString(R.string.static_hostname) + imageRoute)
                    .placeholder(R.drawable.placeholder)
                    .resize(250, 250)
                    .centerCrop()
                    .into(holder.image, object : Callback {
                        override fun onSuccess() {
                        }

                        override fun onError() {
                            Picasso.with(context).load(merchantImage)
                                .placeholder(R.drawable.placeholder)
                                .error(R.drawable.placeholder)
                                .resize(250, 250)
                                .centerCrop()
                                .into(holder.image)
                        }

                    })
            }

            holder.name!!.text = records[position].getJSONObject("QuickCategory").getString("name")
            holder.layout.setOnClickListener {
                if(context is onClickCategories){
                    context.clickCategories(records, position,merchantImage)
                }
            }
        }

    }
    interface onClickCategories {
        fun clickCategories(records: MutableList<JSONObject>, position: Int,merchantImage: String)
    }

    override fun getItemCount(): Int {
        return records.size
    }

}
