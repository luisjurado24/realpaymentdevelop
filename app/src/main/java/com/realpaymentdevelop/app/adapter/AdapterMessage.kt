package com.realpaymentdevelop.app.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.marginTop
import androidx.recyclerview.widget.RecyclerView
import com.realpaymentdevelop.app.R
import org.json.JSONArray
import org.json.JSONException
import java.text.ParseException
import java.text.SimpleDateFormat


class AdapterMessage(val context: Context?, private val messages: JSONArray) :
        BaseAdapter() {
    override fun getCount(): Int {
        return messages.length()
    }

    override fun getItem(p0: Int): Any {
        return messages.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    @SuppressLint("SimpleDateFormat")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var list: View? = null

        if (convertView == null) {
            try {
                val inflater:LayoutInflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                list= View(context)
                list = inflater.inflate(R.layout.card_view_message,null)


                // JSON
                var messageObject = messages.getJSONObject(position)
                messageObject = messageObject.getJSONObject("Message")

                // Grab the values we need
                val text = messageObject.getString("text")
                val title = messageObject.getString("title")
                val read = messageObject.getBoolean("read")
                var date = messageObject.getString("created")
                if (date == "null") {
                    date = ""
                } else {
                    val sdf = SimpleDateFormat("yyyy-MM-dd")
                    try {
                        val dDate = sdf.parse(date)
                        val dateFormat = DateFormat.getMediumDateFormat(context)
                        date = dateFormat.format(dDate)
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }
                }

                // Grab the view elements
                val mTitle = list.findViewById<View>(R.id.tv_card_view_message_title) as TextView
                val mText = list.findViewById<View>(R.id.tv_card_view_message_text) as TextView
                val mDate = list.findViewById<View>(R.id.tv_card_view_message_date) as TextView

                // Set the data

                val replaceTex= if(text.length >= 50){
                    "${text.subSequence(0,40)}..."
                }else{
                    text
                }
                mTitle.text = title
                mText.text = replaceTex
                mDate.text = date

                // Change the icon if the message is marked "Read"
                if (read) {
                    val mIcon = list.findViewById<ImageView>(R.id.iv_card_view_message)
                    mIcon.setImageResource(R.mipmap.message_icon_foreground)
                }
            } catch (e: JSONException) {
                Log.d("Message", e.localizedMessage)
            }
        } else {
            list = convertView
        }
        return list!!
    }

}
