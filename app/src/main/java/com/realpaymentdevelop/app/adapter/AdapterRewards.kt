package com.realpaymentdevelop.app.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.realpaymentdevelop.app.R
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*

class AdapterRewards(private val mContext: Context?, private val rewards: JSONObject) : BaseAdapter() {
    override fun getCount(): Int {
        return rewards.length()
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }
    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var list: View? = null
        val inflater = mContext!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (convertView == null) {
            try {
                list = View(mContext)
                list = inflater.inflate(R.layout.card_view_rewards, null)
                val record = rewards.getJSONObject("$position")
                var merchant = record.getJSONObject("Merchant").getString("name").toString()
                val spliLocalization = merchant.split(" ")
                if(spliLocalization.size>2){
                    merchant = spliLocalization[0]+" "+spliLocalization[1]+"..."
                }
                val pointsValue = Integer.valueOf(record.getString("points"))
                val points = NumberFormat.getNumberInstance(Locale.getDefault()).format(pointsValue)
                val mMerchant = list.findViewById<TextView>(R.id.tv_card_view_rewards_name)
                val mPoints = list.findViewById<View>(R.id.tv_card_view_rewards_points) as TextView
                mMerchant.text = merchant
                mPoints.text = "$points ${mContext.resources.getString(R.string.points)}"
            } catch (e: JSONException) {
            }
        } else {
            list = convertView
        }
        return list
    }
}