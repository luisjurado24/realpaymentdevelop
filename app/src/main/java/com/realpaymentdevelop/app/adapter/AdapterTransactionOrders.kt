package com.realpaymentdevelop.app.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.utils.Utils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_view_history.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.DecimalFormat

class AdapterTransactionOrders(private val context: Context?, private val records: JSONArray):BaseAdapter() {
    override fun getItem(p0: Int): Any {
        return records.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return records.getJSONObject(p0).getInt("quickOrderId").toLong()
    }

    override fun getCount(): Int {
        return records.length()
    }

    @SuppressLint("SetTextI18n")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View? {
        val mf = DecimalFormat("0.00")
        var list = p1
        val holder :ViewHolder
        val mInflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if(p1==null){
            holder = ViewHolder()
            list = mInflater.inflate(R.layout.card_view_history, null)
            holder.externalOrderId = list.order_title
            holder.location = list.order_location
            holder.totalAmount = list.order_amount
            holder.date = list.order_date
            list?.tag = holder
        }else{
            holder = list?.tag as ViewHolder
        }
        try{
            val record = records.getJSONObject(p0)
            holder.externalOrderId!!.text = "${record.getString("externalOrderId")}"
            val location = record.optString("merchantLocationName","No location information available")
            holder.location!!.text = if(location ==  "null") "No location information available" else location
            holder.totalAmount!!.text = "$${mf.format(record.getDouble("totalAmount"))}"
            holder.date!!.text = "${Utils.formatDateWithYear(record.getString("created")).trim { it <= ' ' }}"
        }catch(e:JSONException){
            Log.e("Error","Error in adapter")
        }
        return list
    }

    class ViewHolder {
        var externalOrderId: TextView? = null
        var location: TextView? = null
        var totalAmount: TextView? = null
        var date: TextView? = null
    }
}