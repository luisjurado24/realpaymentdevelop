package com.realpaymentdevelop.app.adapter.order

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.realpaymentdevelop.app.R
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONArray
import org.json.JSONObject

class AdapterLocalization(
    private val context: Activity,
    private val records: JSONArray,
    private val urlImage: String
) : RecyclerView.Adapter<AdapterLocalization.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val time: TextView = itemView.findViewById<TextView>(R.id.time)
        val image: CircleImageView = itemView.findViewById<CircleImageView>(R.id.icon_locations)
        val location: TextView = itemView.findViewById<TextView>(R.id.location)
        val layout: ConstraintLayout = itemView.findViewById<ConstraintLayout>(R.id.contrain_layout_location)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val list =
            LayoutInflater.from(context).inflate(R.layout.card_view_localitation, parent, false)
        return ViewHolder(list)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val record: JSONObject? = records.getJSONObject(position)
        val location = record!!.getJSONObject("MerchantLocation").getString("name")
        val url = urlImage

        val acceptingOrder =
            record.getJSONObject("QuickMerchantLocation").getString("acceptingOrders")
        holder.time.text = if (acceptingOrder == "1") {
            holder.layout.alpha = 1.0F
            "est. wait: ${record.getJSONObject("QuickMerchantLocation").get("waitTime")} minutes"
        } else {
            holder.layout.alpha = 0.5F
            context.getString(R.string.closed_opening)
        }

        Picasso.with(context).load(url)
            .placeholder(R.drawable.placeholder)
            .error(R.drawable.placeholder)
            .resize(250, 250)
            .centerCrop()
            .into(holder.image)

        holder.location.text = location

        holder.layout.setOnClickListener {
            if(context is onClickLocation){
                context.onClickLocation(record,"${context.title}",(acceptingOrder=="1"))
            }
        }
    }


    override fun getItemCount(): Int {
        return records.length()
    }

    interface onClickLocation{
        fun onClickLocation(location:JSONObject,title:String, abiable:Boolean)
    }

}