package com.realpaymentdevelop.app.interfaces

import org.json.JSONObject

interface SendDataToRemoveOrEdit {

    fun setValuesOnExpressOrder()
    fun editItem(item:JSONObject)

}
