package com.realpaymentdevelop.app.interfaces;

public interface ActivityChangeEventListener {
    void setCurrentActivity();
}
