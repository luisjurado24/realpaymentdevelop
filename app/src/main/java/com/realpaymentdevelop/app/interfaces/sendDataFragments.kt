package com.realpaymentdevelop.app.interfaces

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import org.json.JSONArray
import org.json.JSONObject

interface sendDataFragments{

  fun sendDatatoAdapter(total:Double,flag:Boolean,extras:JsonArray)

}
