package com.realpaymentdevelop.app;

import android.util.Log;

import com.realpaymentdevelop.app.activity.LogInActivity;
import com.realpaymentdevelop.app.annotations.AutoLogoutEnabled;
import com.realpaymentdevelop.app.application.RealPaymentApplication;
import com.realpaymentdevelop.app.utils.Utils;

import java.util.concurrent.Executors;

public class ScreenTouchInterceptorEventHandler {

    private static final int TICK_MILLISECONDS = 1000;
    private static final int TIMER_TOTAL_MILLISECONDS = RealPaymentApplication.getInstance().getResources().getInteger(R.integer.auto_logout_timeout);
    private static ScreenTouchInterceptorEventHandler screenTouchInterceptorEventHandler;
    private static int INACTIVITY_TIMER_SLEEP_MILLISECONDS;
    private InactivityTimer inactivityTimer;

    private ScreenTouchInterceptorEventHandler() {
    }

    public static ScreenTouchInterceptorEventHandler getInstance() {
        if (screenTouchInterceptorEventHandler == null) {
            screenTouchInterceptorEventHandler = new ScreenTouchInterceptorEventHandler();
        }
        return screenTouchInterceptorEventHandler;
    }

    public void handleScreenTouchInterceptorEvent() {
        refreshInactivityTimer();
    }

    private void refreshInactivityTimer() {
        INACTIVITY_TIMER_SLEEP_MILLISECONDS = 0;
    }

    public void startInactivityTimer() {
        if (inactivityTimer != null) {
            inactivityTimer.running = false;
            inactivityTimer = null;
        }
        inactivityTimer = new InactivityTimer();
        refreshInactivityTimer();
        Executors.newSingleThreadExecutor().submit(inactivityTimer);
    }

    public void stopInactivityTimer() {
        if (inactivityTimer != null && inactivityTimer.running) {
            inactivityTimer.stopTimer();
            inactivityTimer = null;
        }
    }

    class InactivityTimer implements Runnable {
        private boolean running = true;
        private boolean isStopped = false;

        private InactivityTimer() {
        }

        public void stopTimer() {
            this.isStopped = true;
            setRunning(false);
            refreshInactivityTimer();
        }

        public void setRunning(boolean running) {
            this.running = running;
        }

        @Override
        public void run() {
            while (running && !isStopped && RealPaymentApplication.getInstance().getCurrentActivity().getClass().isAnnotationPresent(AutoLogoutEnabled.class)) {
                try {
                    Thread.sleep(TICK_MILLISECONDS);
                    INACTIVITY_TIMER_SLEEP_MILLISECONDS += TICK_MILLISECONDS;

                    if (INACTIVITY_TIMER_SLEEP_MILLISECONDS >= TIMER_TOTAL_MILLISECONDS) {

                        RealPaymentApplication.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (RealPaymentApplication.getInstance().getConfig().isAutoLogoutEnabled()) {
                                    stopInactivityTimer();
                                    RealPaymentApplication.getInstance().getConfig().logoutUser();
                                    Utils.launch(RealPaymentApplication.getInstance().getCurrentActivity(), LogInActivity.class, false);
                                } else {
                                    setRunning(true);
                                    refreshInactivityTimer();
                                }
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    Log.e(this.getClass().getName(), "Error Occured : ", e);
                }
            }
        }
    }
}
