package com.realpaymentdevelop.app.dialog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.realpaymentdevelop.app.R
import kotlinx.android.synthetic.main.dialog_view.view.*

class DialogPersonalizate : DialogFragment() {
    var mListenner: BackCvvFragment? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BackCvvFragment)
            mListenner = context
        else null

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView: View = inflater.inflate(R.layout.dialog_view, container, false)
        val text = rootView.cvv_confirm_card
        rootView.confirm_card_no.setOnClickListener {
            dismiss()
        }
        rootView.confirm_card_yes.setOnClickListener {
            if(mListenner != null ){
                if(text.editText!!.text.length==3){
                    mListenner!!.returnBackCvv("${text.editText!!.text}")
                }else{
                    text.editText!!.error = getString(R.string.error_password_bank)
                }
            }
        }
        return rootView
    }

    interface BackCvvFragment {
        fun returnBackCvv(cvv: String)
    }
}