package com.realpaymentdevelop.app.db

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.util.Log
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import org.json.JSONObject

class DataBase(val context: Context) :
    SQLiteOpenHelper(context, ColumnsName.FeedEntry.DATABASE_NAME, null, ColumnsName.FeedEntry.DATABASE_VERSION) {

    override fun onCreate(p0: SQLiteDatabase?) {
        p0!!.execSQL(Companion.CREATE_CART)
        p0.execSQL(Companion.CREATE_LOCATION)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        val query = "DROP TABLE IF EXISTS "
        p0!!.execSQL(query + ColumnsName.FeedEntry.TABLE_CART)
        p0.execSQL(query + ColumnsName.FeedEntry.TABLE_LOCATION)
        onCreate(p0)
    }
    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }


    fun getCart(idMerchant:String,idLocation:String): ArrayList<JsonObject> {
        val query = "SELECT * FROM ${ColumnsName.FeedEntry.TABLE_CART} WHERE ${ColumnsName.FeedEntry.TABLE_CART_ID_MERCHANT}=${idMerchant} AND ${ColumnsName.FeedEntry.TABLE_CART_ID_LOCATION}=${idLocation}"
        val cartJsonArray: ArrayList<JsonObject> = ArrayList()
        val db = this.writableDatabase
        val jsonParser = JsonParser()
        val registros = db.rawQuery(query,null)
        while (registros.moveToNext()) {
            cartJsonArray.add(jsonParser.parse(registros.getString(1)).asJsonObject)
        }
        db.close()
        return cartJsonArray
    }

    fun localizationCart(idLocation: String,idMerchant: String):JSONObject{
        val db= writableDatabase
        val query = "SELECT * FROM ${ColumnsName.FeedEntry.TABLE_LOCATION} WHERE ${ColumnsName.FeedEntry.TABLE_LOCATION_ID}= $idLocation and ${ColumnsName.FeedEntry.TABLE_LOCATION_MERCHANT_ID}= $idMerchant"
        val registros = db.rawQuery(query,null)
        Log.e("registros",registros.columnNames.toString())
        while(registros.moveToNext()){
            val jsonObject=JSONObject()
            jsonObject.run {
                put("idLocation",registros.getString(0))
                put("jsonDelivery",JSONObject(registros.getString(1)))
                put("nameLocation",registros.getString(2))
                put("merchantLocation",registros.getString(3))
                put("idMerchantLocation",registros.getString(4))
                put("urlImageMerchant",registros.getString(5))
            }
            db.close()
            return jsonObject
        }
        db.close()
        return JSONObject()
    }

    fun deleteLocation(idMerchant: String,idLocation: String){
        val db = this.writableDatabase
        db.delete(ColumnsName.FeedEntry.TABLE_LOCATION,
            "${ColumnsName.FeedEntry.TABLE_LOCATION_ID}=$idLocation AND " +
                    "${ColumnsName.FeedEntry.TABLE_LOCATION_MERCHANT_ID}=$idMerchant",null)
        db.close()
    }
    fun deleteAll(idMerchant: String,idLocation: String){
        val db = this.writableDatabase
        val whereClauseLocation ="${ColumnsName.FeedEntry.TABLE_LOCATION_ID}=$idLocation AND " +
                "${ColumnsName.FeedEntry.TABLE_LOCATION_MERCHANT_ID}=$idMerchant"
        val whereClauseCart ="${ColumnsName.FeedEntry.TABLE_CART_ID_LOCATION}=$idLocation AND " +
                "${ColumnsName.FeedEntry.TABLE_CART_ID_MERCHANT}=$idMerchant"
        db.delete(ColumnsName.FeedEntry.TABLE_CART, whereClauseCart,null)
        db.delete(ColumnsName.FeedEntry.TABLE_LOCATION,whereClauseLocation,null)
        db.close()
    }

    fun setJson(contentValues: ContentValues, contentValuesLocation: ContentValues?) {
        val db = writableDatabase
        if (contentValuesLocation == null) {
            db.insert(ColumnsName.FeedEntry.TABLE_CART, null, contentValues)


        }else{
            db.insert(ColumnsName.FeedEntry.TABLE_CART, null, contentValues)
            db.insert(ColumnsName.FeedEntry.TABLE_LOCATION, null, contentValuesLocation)
        }
        db.close()
    }

    fun deleteJson(id: String):Int {
        val db = writableDatabase
        val result= db.delete(ColumnsName.FeedEntry.TABLE_CART,"id=\"$id\"",null)
        db.close()
        return result
    }

    object ColumnsName {
        object FeedEntry: BaseColumns {
            const val DATABASE_VERSION = 2
            const val DATABASE_NAME = "cart.db"
            const val TABLE_CART = "cart"
            const val TABLE_CART_ID = "id"
            const val TABLE_CART_OBJECT = "json"
            const val TABLE_CART_ID_LOCATION = "idLocation"
            const val TABLE_CART_ID_MERCHANT = "idMerchant"

            const val TABLE_LOCATION = "location"

            const val TABLE_LOCATION_ID = "locationID"
            const val TABLE_LOCATION_DELIVERY="jsonDelivery"
            const val TABLE_LOCATION_NAME = "locationName"
            const val TABLE_LOCATION_MERCHANT = "merchantName"
            const val TABLE_LOCATION_MERCHANT_ID = "merchantID"
            const val TABLE_LOCATION_MERCHANT_URL = "merchantURL"
        }
    }

    companion object {
        private const val CREATE_LOCATION = "CREATE TABLE ${ColumnsName.FeedEntry.TABLE_LOCATION} ( " +
                ColumnsName.FeedEntry.TABLE_LOCATION_ID + " TEXT," +
                ColumnsName.FeedEntry.TABLE_LOCATION_DELIVERY + " TEXT,"+
                ColumnsName.FeedEntry.TABLE_LOCATION_NAME+ " TEXT," +
                ColumnsName.FeedEntry.TABLE_LOCATION_MERCHANT + " TEXT," +
                ColumnsName.FeedEntry.TABLE_LOCATION_MERCHANT_ID + " TEXT,"+
                ColumnsName.FeedEntry.TABLE_LOCATION_MERCHANT_URL + " TEXT" +")"
        private const val  CREATE_CART = "CREATE TABLE ${ColumnsName.FeedEntry.TABLE_CART} ( " +
        ColumnsName.FeedEntry.TABLE_CART_ID + " TEXT," +
        ColumnsName.FeedEntry.TABLE_CART_OBJECT + " TEXT," +
        ColumnsName.FeedEntry.TABLE_CART_ID_LOCATION +" TEXT," +
        ColumnsName.FeedEntry.TABLE_CART_ID_MERCHANT +" TEXT" +")"
    }

}