package com.realpaymentdevelop.app.services;

import com.realpaymentdevelop.app.R;
import com.realpaymentdevelop.app.application.RealPaymentApplication;

public interface ApiParams {

    String deviceId = RealPaymentApplication.getInstance().getResources().getString(R.string.param_deviceId);
    String lang = RealPaymentApplication.getInstance().getResources().getString(R.string.param_lang);
    String region = RealPaymentApplication.getInstance().getResources().getString(R.string.param_region);
    String build = RealPaymentApplication.getInstance().getResources().getString(R.string.param_build);
    String appVersion = RealPaymentApplication.getInstance().getResources().getString(R.string.param_appVersion);
    String appId = RealPaymentApplication.getInstance().getResources().getString(R.string.param_appId);
}
