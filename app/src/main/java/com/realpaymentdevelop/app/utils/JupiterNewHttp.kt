  package com.realpaymentdevelop.app.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.*
import com.github.kittinunf.result.Result
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.loopj.android.http.AsyncHttpClient
import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.services.ApiParams
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.util.*


class JupiterNewHttp {

    private lateinit var dialog: MaterialAlertDialogBuilder
    private lateinit var dialogCreate: androidx.appcompat.app.AlertDialog

    private val endPoint: String
    private val apiKey: String
    private val hostname: String
    private var activity: Context
    private val config: Config

    private val TAG = "NewJupiterHTTP"

    constructor(context: Context, endPoint: String) {
        this.endPoint = endPoint
        this.apiKey = context.getString(R.string.apikey)
        this.hostname = context.getString(R.string.hostname)
        this.activity = context
        dialog = MaterialAlertDialogBuilder(activity)
        config = Config(activity)
    }

    /*
    * With this constructor I can change the API to send parameters
    * */
    constructor(context: Context, api: String, endPoint: String) {
        this.endPoint = endPoint
        this.apiKey = context.getString(R.string.apikey)
        this.hostname = context.getString(R.string.host) + api
        this.activity = context
        dialog = MaterialAlertDialogBuilder(activity)
        config = Config(activity)
    }

    private fun resultApyCallMenu(responseJson: JSONObject) {
        val responseCode: Int
        Log.e("TAG","jsonResp$responseJson")
        try {
            responseCode = responseJson.getInt("responseCode")
            if (responseCode == 100) {
                onSuccess(responseJson)
            } else {
                onFailure(responseJson)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
            onFailure(responseJson)
        }
    }

    private var onSuccess: (response: JSONObject) -> Unit = {
        Log.d(TAG, "Default onSuccess")
    }

    fun setOnSuccess(func: (response: JSONObject) -> Unit) {
        onSuccess = func
    }

    private var onFailure: (response: JSONObject?) -> Unit = {
        Log.d(TAG, "Default onFailure")
    }

    fun setOnFailure(func: (response: JSONObject?) -> Unit) {
        onFailure = func

    }

    private var onConnectFailure: (response: JSONObject?) -> Unit = {
        Log.d(TAG, "Default onConnectFailure")
    }

    fun setOnConnectFailure(func: (response: JSONObject?) -> Unit) {
        onConnectFailure = func
    }

    private inner class ourResponseHandler : Handler<String> {

        override fun failure(error: FuelError) {
            Log.d("Error: ", error.toString())
            onConnectFailure(null)
        }

        override fun success(value: String) {
            val responseCode: Int
            val responseJson = JSONObject(value)
            try {
                responseCode = responseJson.getInt("responseCode")
                if (responseCode == 100) {
                    // We already have a merchant
                    onSuccess(responseJson)
                } else {
                    onFailure(responseJson)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
                onFailure(responseJson)
            }
        }

    }


    fun updatePerfilFile(request: ArrayList<Pair<String, Any>>,file: String) {
        Log.d(TAG, "Doing Async Call")
        Log.d(TAG, "Doing API Call")
        request.add(Pair("ApiKey", apiKey))
        request.add(Pair(ApiParams.deviceId, config.deviceUUID!!))
        request.add(Pair(ApiParams.build!!, config.buildNumber!!))
        request.add(Pair(ApiParams.appVersion!!, config.appVersion!!))
        request.add(Pair(ApiParams.lang!!, config.language!!))
        request.add(Pair(ApiParams.region!!, config.region!!))
        Log.d(TAG, request.toString())


        object : AsyncTask<Void, Void, Exception>() {
            override fun onPreExecute() {
                super.onPreExecute()
                Log.e(TAG,"onPreExecute")
                dialog.setTitle(activity.getString(R.string.processing))
                dialog.setMessage(activity.getString(R.string.please_wait))
                dialog.setCancelable(false)
                dialogCreate = dialog.create()
                dialogCreate.show()
            }
            override fun doInBackground(vararg params: Void): Exception? {
                Log.e(TAG,"doInBackground")
                Fuel.upload(path = hostname + endPoint,method = Method.POST, parameters = request).add{
                    FileDataPart(File(file),name = "file")
                }.responseString(ourResponseHandler())
                return null
            }
            override fun onProgressUpdate(vararg values: Void?) {
                super.onProgressUpdate(*values)
                Log.e(TAG,"onProgressUpdate")
                dialog.setTitle(activity.getString(R.string.processing))
                dialog.setMessage(activity.getString(R.string.please_wait))
                dialog.setCancelable(false)
                dialogCreate = dialog.create()
                dialogCreate.show()
            }

            override fun onPostExecute(result: Exception?) {
                super.onPostExecute(result)
                Log.e(TAG,"onPostExecute")
                dialogCreate.dismiss()
            }


        }.execute()
    }

    fun doAsyncApiCall(request: ArrayList<Pair<String, String>>) {
        Log.d(TAG, "Doing Async Call")
        Log.d(TAG, "Doing API Call")
        request.add(Pair("ApiKey", apiKey))
        request.add(Pair(ApiParams.deviceId, config.deviceUUID!!))
        request.add(Pair(ApiParams.build!!, config.buildNumber!!))
        //request.add(Pair(ApiParams.appId!!, config.appId!!))
        request.add(Pair(ApiParams.appVersion!!, config.appVersion!!))
        request.add(Pair(ApiParams.lang!!, config.language!!))
        request.add(Pair(ApiParams.region!!, config.region!!))
        Log.d(TAG, request.toString())

        object : AsyncTask<Void, Void, Exception>() {
            override fun doInBackground(vararg params: Void): Exception? {
                Fuel.post(hostname + endPoint, request).responseString(ourResponseHandler())

                return null
            }
        }.execute()
    }

    fun doAsyncApiCallMenu(request: ArrayList<Pair<String, String>>) {
        Log.d(TAG, "Doing Async Call")
        Log.d(TAG, "Doing API Call")
        Log.e("Params", request.toString())
        request.add(Pair("ApiKey", apiKey))
        request.add(Pair(ApiParams.deviceId!!, config.deviceUUID!!))
        request.add(Pair(ApiParams.build!!, config.buildNumber!!))
        //request.add(Pair(ApiParams.appId!!, config.appId!!))
        request.add(Pair(ApiParams.appVersion!!, config.appVersion!!))
        request.add(Pair(ApiParams.lang!!, config.language!!))
        request.add(Pair(ApiParams.region!!, config.region!!))

        object : AsyncTask<Void, Void, Exception>() {
            @SuppressLint("StaticFieldLeak")
            override fun onPreExecute() {
                dialog.setTitle(activity.getString(R.string.processing))
                dialog.setMessage(activity.getString(R.string.please_wait))
                dialog.setCancelable(false)
                dialogCreate = dialog.create()
                dialogCreate.show()
            }

            override fun doInBackground(vararg params: Void): Exception? {

                val (req, response, result) = Fuel.post(hostname + endPoint, request)
                    .responseString()
                if (result is Result.Success) {
                    resultApyCallMenu(JSONObject(result.value))
                } else {
                    onConnectFailure(null)
                }
                return null
            }

            override fun onPostExecute(result: Exception?) {
                super.onPostExecute(result)
                dialogCreate.dismiss()
            }

        }.execute()
    }

    fun doApiCall(request: ArrayList<Pair<String, String>>) {
        Log.d(TAG, "Doing API Call")
        request.add(Pair("ApiKey", apiKey))
        request.add(Pair(ApiParams.deviceId!!, config.deviceUUID!!))
        request.add(Pair(ApiParams.build!!, config.buildNumber!!))
        //request.add(Pair(ApiParams.appId!!, config.appId!!))
        request.add(Pair(ApiParams.appVersion!!, config.appVersion!!))
        request.add(Pair(ApiParams.lang!!, config.language!!))
        request.add(Pair(ApiParams.region!!, config.region!!))

        val (_, _, result) = Fuel.post(hostname + endPoint, request).responseString()
        if (result is Result.Success) {
            resultApyCallMenu(JSONObject(result.value))
        } else {
            onConnectFailure(null)
        }

    }


}
