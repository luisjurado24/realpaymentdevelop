package com.realpaymentdevelop.app.utils;

import android.util.Base64;
import android.util.Log;

import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by john on 3/1/16.
 * Modified by Eduardo on 4/30/19
 */
public class RPEncrypter {

    public static String encrypt(String key, String initVector, String value) {
        try {
            int len = initVector.length()/2;
            String hexChar, binChar;
            byte[] initVectorByteArray = new byte[len];

            for (int n=0;n<len;n++) {
                hexChar = initVector.substring(2*n, 2*n+2);
                byte b = (byte) (Integer.parseInt(hexChar, 16) & 0xFF);
                initVectorByteArray[n] = b;
            }

            Log.d("RPEncrypter", "Size of initVectorByteArray: " + Array.getLength(initVectorByteArray));

            IvParameterSpec iv = new IvParameterSpec(initVectorByteArray);
            //IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, iv);

            int paddingRequired = (128 - value.length() % 128) % 128; // padding size
            Log.d("RPEncrypter", "Padding size: " + paddingRequired);
            if (paddingRequired > 0) { // pad
                int i = 0;
                while (i < paddingRequired) {
                    value = value.concat("\0");
                    i++;
                }
            }
            String paddingSizeInHex = String.format(Locale.US, "%02x", paddingRequired);

            byte[] encrypted = cipher.doFinal(value.getBytes()); // encrypt

            String encryptedStringToHex = Utils.getHexString(encrypted);
//            Log.d("RPEncrypter", "encrypt: " + foo);
//            String encryptedArrayToString = new String(encrypted); // byte[] -> String
//            String encryptedStringToHex = Utils.stringToHex(encryptedArrayToString); // String -> hex string
            encryptedStringToHex = paddingSizeInHex.concat(encryptedStringToHex); // prepend padding size

            return encryptedStringToHex;
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            Log.d("RPEncrypter", "Encryption exception caught: " + errorMessage);
        }

        return null;
    }

    /*public static String encryptNew(String key, String initVector, String value){

        try{

            byte[] initVectorByteArray

            //Log.d("RPEncrypter", "Size of initVectorByteArray: " + Array.getLength(initVectorByteArray));

            SecretKey secretKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            //GCMParameterSpec parameterSpec = new GCMParameterSpec(128,initVectorByteArray); //128 bit auth tag length
            IvParameterSpec parameterSpec = new IvParameterSpec(initVectorByteArray);

            cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);
            byte[] cipherText = cipher.doFinal(value.getBytes());

            Log.d("Cipher",Utils.getHexString(cipherText));

            return Utils.getHexString(cipherText);

        }catch(Exception e){
            String errorMessage = e.getMessage();
            Log.d("RPEncrypter", "Encryption exception caught: " + errorMessage);
        }

        return null;
    }*/


    public static String encryptNew(String key, String initVector, String value){

        try{

            int len = initVector.length()/2;
            String hexChar;
            byte[] initVectorByteArray = new byte[len];

            for (int n=0;n<len;n++) {
                hexChar = initVector.substring(2*n, 2*n+2);
                byte b = (byte) (Integer.parseInt(hexChar, 16) & 0xFF);
                initVectorByteArray[n] = b;
            }

            IvParameterSpec iv = new IvParameterSpec(initVectorByteArray);

            int paddingRequired = (128 - (value.length() % 128)) % 128;

            Log.d("Value.Length:",String.valueOf(value.length()));

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
            //IvParameterSpec ivSpec = new IvParameterSpec(initVector.getBytes());
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, iv);
            byte[] cipherText = cipher.doFinal(value.getBytes());


            if (paddingRequired > 0) { // pad
                int i = 0;
                while (i < paddingRequired) {
                    value = value.concat("\0");
                    i++;
                }
            }

            String paddingSizeInHex = String.format("%02x", paddingRequired);

            String encryptedStringToHex = Utils.getHexString(cipherText);

            encryptedStringToHex = paddingSizeInHex.concat(encryptedStringToHex);

            //paddingSizeInHex = paddingSizeInHex.concat(Base64.encodeToString(cipherText,Base64.NO_WRAP));

            return encryptedStringToHex;

        }catch(Exception e){
            String errorMessage = e.getMessage();
            Log.d("RPEncrypter", "Encryption exception caught: " + errorMessage);
        }

        return null;
    }


    public static String decrypt(String key, String initVector, String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decode(encrypted, Base64.DEFAULT));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /*______________________________________________________________________*/


  /*  public static void main(String[] args) {
        String key = "Bar12345Bar12345"; // 128 bit key
        String initVector = "RandomInitVector"; // 16 bytes IV

        System.out.println(decrypt(key, initVector,
                encrypt(key, initVector, "Hello World")));
    }*/


}
