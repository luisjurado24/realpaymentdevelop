package com.realpaymentdevelop.app.utils;

import org.json.JSONObject;

public class ApiResult {
    public int httpStatusCode = 0;
    public int success = 0;
    public int responseCode = 0;
    public String message = "";
    public JSONObject response;
}
