package com.realpaymentdevelop.app.utils;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Gomez Adams on 27/6/15.
 */
public class User {

    /**
     * The api/login JSON result to be dissected
     */
    public JSONObject user;

    /**
     * A user'spotSelected access token, used to authenticate api calls
     */
    public String token;

    /**
     * A user'spotSelected unique id
     */
    public String userId;

    /**
     * A user'spotSelected unique username
     */
    public String username;

    /**
     * Email address
     */
    public String email;

    /**
     * Phone number
     */
    public String phone;

    /**
     * A merchant'spotSelected unique id
     */
    public String merchantId;

    /**
     * A merchant'spotSelected location id
     */
    public String merchantLocationid;

    /**
     * A user'spotSelected first name
     */
    public String firstName;

    public String name;

    /**
     * Pin number similar to debit card pin
     */
    public String pin;

    /**
     * The name of the user'spotSelected profile photo image
     */
    public String image;

    /**
     * The path to the user'spotSelected profile photo
     */
    public String fullImageUrl;


    /**
     * Create a user obj from api/login JSON result
     * @param jsonObject
     */
    public User(JSONObject jsonObject){
        this.user = jsonObject;

        try {
            this.token = jsonObject.getString("token");
            this.userId = jsonObject.getString("userId");

            if ( ! jsonObject.getString("name").equals("null") ) {
                this.name = jsonObject.getString("name");
                this.firstName = jsonObject.getString("firstName");
            }
            else {
                this.name = "";
                this.firstName = "";
            }

            if ( ! jsonObject.getString("fullImageUrl").equals("null") )  {
                this.fullImageUrl = jsonObject.getString("fullImageUrl");
            }

        }catch(Exception e){
            Log.d("utils", "User class JSONException caught: " + e.getMessage());
        }

    }


    /**
     * return a "non" user obj
     */
    public User(){
        this.token = "undefined";
        this.userId = "undefined";
    }

}