package com.realpaymentdevelop.app.utils


import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.os.AsyncTask
import android.preference.PreferenceActivity
import android.util.Log
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.loopj.android.http.SyncHttpClient
import com.realpaymentdevelop.app.R
import cz.msebera.android.httpclient.Header
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject




/**
 * Created by dani on 12/16/17.
 */

class JupiterHttp {
  private val TAG = "JupiterHttp"

  private var onSuccess: (response: JSONObject) -> Unit = {
    Log.d(TAG, "Default onSuccess")

  }
  fun setOnSuccess(func: (response: JSONObject) -> Unit) {
    onSuccess = func
  }

  private var onFailure: (response: JSONObject?) -> Unit = {
    Log.d(TAG, "Default onFailure")
  }
  fun setOnFailure(func: (response: JSONObject?) -> Unit) {
    onFailure = func

  }

  private var onConnectFailure: (response: JSONObject?) -> Unit = {
    Log.d(TAG, "Default onConnectFailure")
  }
  fun setOnConnectFailure(func: (response: JSONObject?) -> Unit) {
    onConnectFailure = func
  }



  private lateinit var dialog : ProgressDialog

  private val endPoint: String
  private val apiKey: String
  private val hostname: String
  private var activity :Context


  private inner class ourResponseHandler : JsonHttpResponseHandler() {
    override fun onSuccess(statusCode: Int, headers: Array<Header>?, response: JSONObject?) {
      Log.d(TAG, "Success Back")
      val responseCode: Int
      try {
        responseCode = response!!.getInt("responseCode")
        if (responseCode == 100) {
          onSuccess(response)
        } else {
          onFailure(response)
        }
      } catch (e: JSONException) {
        e.printStackTrace()
        onFailure(response)
      }

    }

    override fun onFailure(statusCode: Int, headers: Array<Header>?, response: String?, e: Throwable?) {
      Log.d(TAG, "No success Back")
      onFailure(null)
    }

    override fun onFailure(statusCode: Int, headers: Array<Header>?, e: Throwable?, jsonObject: JSONObject?) {
      Log.d(TAG, "Other No Success Back")

      onConnectFailure(null)
    }
  }

  constructor(context: Context, endPoint: String) {
    this.endPoint = endPoint
    this.apiKey = context.getString(R.string.apikey)
    this.hostname = context.getString(R.string.hostname)
    this.activity = context
  }

  /*
  * With this constuctor I can change the API to send parameters
  * */
  constructor(context: Context,api:String ,endPoint: String) {
    this.endPoint = endPoint
    this.apiKey = context.getString(R.string.apikey)
    this.hostname = context.getString(R.string.host)+api
    this.activity = context
    dialog = ProgressDialog(activity)

  }

  fun doAsyncApiCall(requestParams: RequestParams) {
    Log.d(TAG, "Doing Async Call")
    Log.d(TAG, "Doing API Call")
    requestParams.put("ApiKey", apiKey)

    object : AsyncTask<Void, Void, Exception>() {
      override fun doInBackground(vararg params: Void): Exception? {
        val asyncClient = SyncHttpClient()
        asyncClient.post(hostname + endPoint, requestParams, ourResponseHandler())
        return null
      }
    }.execute()
  }

  fun doAsyncApiCallMenu(requestParams: RequestParams) {
    Log.d(TAG, "Doing Async Call")
    Log.d(TAG, "Doing API Call")
    requestParams.put("ApiKey", apiKey)

    object : AsyncTask<Void, Void, Exception>() {
      override fun onPreExecute() {
        dialog.setMessage("Please wait...")
        dialog.setCancelable(false)
        dialog.show()

      }

      override fun doInBackground(vararg params: Void): Exception? {
        val asyncClient = SyncHttpClient()
        asyncClient.post(hostname + endPoint, requestParams, ourResponseHandler())
        return null
      }

      override fun onPostExecute(result: Exception?) {
        super.onPostExecute(result)
        dialog.dismiss()
      }

    }.execute()
  }

  fun doApiCall(requestParams: RequestParams) {
    Log.d(TAG, "Doing API Call")
    val syncClient = SyncHttpClient()
    // Always need API Key
    requestParams.put("ApiKey", apiKey)
    syncClient.post(hostname + endPoint, requestParams, ourResponseHandler())
  }
}
