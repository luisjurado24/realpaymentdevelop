package com.realpaymentdevelop.app.utils;

import android.content.Context;

import com.squareup.picasso.Picasso;

public class singletonPiccaso {

    private static Picasso instance;

    public static Picasso with(Context context) {
        if (instance == null) {
            instance = new Picasso.Builder(context.getApplicationContext()).build();
        }
        return instance;
    }

    private singletonPiccaso() {
        throw new AssertionError("No instances.");
    }
}
