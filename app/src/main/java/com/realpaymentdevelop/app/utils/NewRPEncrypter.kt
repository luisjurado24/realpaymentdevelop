package com.realpaymentdevelop.app.utils


import android.util.Base64
import android.util.Log
import java.lang.StringBuilder
import java.nio.charset.StandardCharsets
import java.security.*
import java.security.spec.X509EncodedKeySpec
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import org.apache.commons.codec.binary.Hex






const val CRYPTO_METHOD = "RSA"
const val CRYPTO_TRANSFORM = "RSA/ECB/PKCS1Padding"
const val AES_TRANSFORMATION = "AES/CBC/PKCS5Padding"

class NewRPEncrypter {


    private fun String.toPublicKey(): PublicKey? {

        var publicKey : PublicKey? = null
        try{
            val keyBytes: ByteArray = Base64.decode(this.toByteArray(),Base64.DEFAULT)
            val spec = X509EncodedKeySpec(keyBytes)
            val keyFactory = KeyFactory.getInstance(CRYPTO_METHOD)
            publicKey = keyFactory.generatePublic(spec)
        }catch(e:Exception){
            Log.d("ErrorPK", e.message!!)
        }
        return publicKey
    }

    fun newEncrypt(key:String, message:String):String{
        val encrypted: ByteArray
        var keyLocal = key.replace("-----BEGIN PUBLIC KEY-----","")
        keyLocal = keyLocal.replace("-----END PUBLIC KEY-----","")
        val cipher: Cipher = Cipher.getInstance(CRYPTO_TRANSFORM)
        cipher.init(Cipher.ENCRYPT_MODE, keyLocal.toPublicKey())
        encrypted = cipher.doFinal(message.toByteArray(StandardCharsets.UTF_8))
        return Base64.encodeToString(encrypted,Base64.DEFAULT)
    }

    /*
    * Encrypt AES
    * */

    fun newEncryptAES (aesKey:String,iv:String,data:String):String{
        try{

            var keyLocal = aesKey.replace("-----BEGIN PUBLIC KEY-----","")
            keyLocal = keyLocal.replace("-----END PUBLIC KEY-----","")
            var dataInBase64 = data

            val initVectorByteArray = Hex.decodeHex(iv.toCharArray())

            val cipher = Cipher.getInstance(AES_TRANSFORMATION)

            val keySpec = SecretKeySpec(keyLocal.toByteArray(), "AES")
            val ivSpec = IvParameterSpec(initVectorByteArray)
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec)

            val paddingRequired = (16 - (data.length % 16)) % 16

            if (paddingRequired > 0) { // pad
                var i = 0
                while (i < paddingRequired) {
                    dataInBase64 += '\u0000'
                    i++
                }
            }

            val result = cipher.doFinal(dataInBase64.toByteArray())


            val sb = StringBuilder()
            for ( b in result ){
                sb.append(String.format("%02x", b))
            }

            val paddingSizeInHex = String.format("%02x",paddingRequired)

            return paddingSizeInHex + sb.toString()

        }catch(e:Exception){
            Log.d("Encrypter","ERROR${e.message}")
        }
        return ""
    }

}