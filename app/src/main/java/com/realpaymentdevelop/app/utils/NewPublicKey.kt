package com.realpaymentdevelop.app.utils

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.github.kittinunf.fuel.httpPost
import com.realpaymentdevelop.app.R

class NewPublicKey {

    private val TAG = "GetPublicKey"


    private fun resultApyCall(value : String){
        Log.d(TAG, "Success Back")
        val success: Int
        val responseCode: Int
        try {
            responseCode = 100
            if (responseCode == 100) {
                // We already have a merchant
                onSuccess(value)
            } else {
                onFailure(value)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            onFailure(value)
        }
    }

    private var onSuccess: (response: String) -> Unit = {
        Log.d(TAG, "Default onSuccess")
    }
    fun setOnSuccess(func: (response: String) -> Unit) {
        onSuccess = func
    }

    private var onFailure: (response: String?) -> Unit = {
        Log.d(TAG, "Default onFailure")
    }
    fun setOnFailure(func: (response: String?) -> Unit) {
        onFailure = func

    }

    private var onConnectFailure: (response: String?) -> Unit = {
        Log.d(TAG, "Default onConnectFailure")
    }
    fun setOnConnectFailure(func: (response: String?) -> Unit) {
        onConnectFailure = func
    }



    // private val endPoint: String
    private val hostname: String

    constructor(context: Context, api:String, endPoint: String) {
        // this.endPoint = endPoint
        this.hostname = context.getString(R.string.host)+api+endPoint
    }


    fun doAsyncApiCall() {

        object : AsyncTask<Void, Void, Exception>() {
            override fun doInBackground(vararg params: Void): Exception? {
                //val asyncClient = SyncHttpClient()
                // Always need API Key
                //Log.d("Dizsaor........:",requestParams.toString())
                Log.d("Host",hostname)
                //asyncClient.post(hostname ,ourResponseHandler())
                val (_, _, result) = hostname.httpPost().responseString()
                if(result is com.github.kittinunf.result.Result.Success){
                    resultApyCall(result.value)
                }else{
                    Log.d("Key", "Error")
                    onConnectFailure(null)
                }
                return null
            }
        }.execute().get()
    }
}