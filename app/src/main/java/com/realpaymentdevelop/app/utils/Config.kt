package com.realpaymentdevelop.app.utils

import android.content.Context
import android.content.SharedPreferences

import com.realpaymentdevelop.app.R
import com.realpaymentdevelop.app.application.RealPaymentApplication
import com.realpaymentdevelop.app.application.configuration.PreferencesHandler

class Config(private val mContext: Context) : PreferencesHandler {
    private val SHARED_PREFS_FILE = "RPAppByRTC"

    val settings: SharedPreferences
        get() = mContext.getSharedPreferences(SHARED_PREFS_FILE, 0)

    operator fun get(key: String, value: String): String? {
        return settings.getString(key, value)
    }

    operator fun set(key: String, value: String) {
            val editor = settings.edit()
            editor.putString(key, value)
            editor.commit()
    }

    fun clear() {
        val editor = settings.edit()
        editor.clear()
        editor.commit()
    }

    operator fun set(key: String, value: Any) {
        val editor = settings.edit()
        if (value is Int)
            editor.putInt(key, value)
        if (value is Int)
            editor.putInt(key, value)
        else if (value is String)
            editor.putString(key, value)
        else if (value is Boolean)
            editor.putBoolean(key, value)
        else if (value is Float)
            editor.putFloat(key, value)
        else if (value is Long)
            editor.putLong(key, value)
        else if (value is Set<*>)
            editor.putStringSet(key, value as Set<String>)

        editor.apply()
    }

    private fun getBoolean(key: String, value: Boolean): Boolean {
        return settings.getBoolean(key, value)
    }

    override fun isAutoLogoutEnabled(): Boolean {
        return getBoolean(
            RealPaymentApplication.getInstance().resources.getString(R.string.auto_logout),
            false
        )
    }

    override fun setAutoLogoutEnabled(enabled: Boolean) {
        set(RealPaymentApplication.getInstance().resources.getString(R.string.auto_logout), enabled)
    }

    override fun logoutUser() {
        clear()
    }

    override fun isLaunchedFirstTime(): Boolean {
        return settings.getBoolean(
            RealPaymentApplication.getInstance().resources.getString(R.string.first_time_use),
            true
        )
    }

    override fun setLaunchedFirstTime(isLaunchedFirstTime: Boolean) {
        set(
            RealPaymentApplication.getInstance().resources.getString(R.string.first_time_use),
            isLaunchedFirstTime
        )
    }

    override fun setDeviceUUID(deviceUUID: String) {
        set(
            RealPaymentApplication.getInstance().resources.getString(R.string.device_uuid),
            deviceUUID
        )
    }

    override fun getDeviceUUID(): String? {
        return settings.getString(
            RealPaymentApplication.getInstance().resources.getString(R.string.device_uuid),
            null
        )
    }

    override fun setLanguage(language: String) {
        set(RealPaymentApplication.getInstance().resources.getString(R.string.language), language)
    }

    override fun getLanguage(): String? {
        return settings.getString(
            RealPaymentApplication.getInstance().resources.getString(R.string.language),
            null
        )
    }

    override fun setRegion(region: String) {
        set(RealPaymentApplication.getInstance().resources.getString(R.string.region), region)
    }

    override fun getRegion(): String? {
        return settings.getString(
            RealPaymentApplication.getInstance().resources.getString(R.string.region),
            null
        )
    }

    override fun setName(name: String?) {
        set("name","$name")
    }

    override fun getName(): String? {
        return settings.getString("name",null)
    }

    override fun setBuildNumber(buildNumber: String) {
        set(
            RealPaymentApplication.getInstance().resources.getString(R.string.build_number),
            buildNumber
        )
    }

    override fun getBuildNumber(): String? {
        return settings.getString(
            RealPaymentApplication.getInstance().resources.getString(R.string.build_number),
            null
        )
    }

    override fun setAppVersion(appVersion: String) {
        set(
            RealPaymentApplication.getInstance().resources.getString(R.string.app_version),
            appVersion
        )
    }

    override fun getAppVersion(): String? {
        return settings.getString(
            RealPaymentApplication.getInstance().resources.getString(R.string.app_version),
            null
        )
    }

    override fun setAppId(appId: String) {
        set(RealPaymentApplication.getInstance().resources.getString(R.string.app_id), appId)
    }

    override fun getAppId(): String? {
        return settings.getString(
            RealPaymentApplication.getInstance().resources.getString(R.string.app_id),
            null
        )
    }

    var rememberMe: Boolean
        get() = getBoolean("rememberMe", true)
        set(b: Boolean) {
            set("rememberMe", b)
        }

    var username: String?
        get() = this["username", "undefined"]
        set(s: String?) {
            if (s != null) {
                this["username"] = s
            }
        }

    var avatarURL: String?
        get() = this["avatarURL", "undefined"]
        set(s: String?) {
            if (s != null) {
                this["avatarURL"] = s
            }
        }
}
