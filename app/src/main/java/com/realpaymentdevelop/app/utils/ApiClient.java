package com.realpaymentdevelop.app.utils;

import android.os.AsyncTask;
import android.util.Log;


import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.realpaymentdevelop.app.services.*;
import com.realpaymentdevelop.app.services.ApiParams;
import com.realpaymentdevelop.app.utils.Config;

//import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

/**
 * Created by ryan on 6/4/17.
 */

public abstract class ApiClient {
    private AsyncTask<String, Void, ApiResult> task;
    public String apiKey;
    public String accessToken;
    public Config config;
    private ApiResult result;


    public ApiClient(Config config) {
        this.config = config;
    }

    // Kinda sucks that we have to do this
    public void complete(ApiResult result) {
        this.result = result;
    }

    // Code that uses this class must implement this
    abstract public void success(ApiResult result);
    abstract public void fail(ApiResult result);

    protected void onPostExecute(ApiResult result) {
        Log.d("onPostExecute: ", "Called.");
        if ( result.httpStatusCode == 200 && result.responseCode == 100 ) {
            success(result);
        } else {
            fail(result);
        }
    }

    public void post(final String apiKey, final String accessToken, final String host, final String endpoint,  final HashMap<String,String> params) {
        task = new AsyncTask<String, Void, ApiResult>() {

            @Override
            protected ApiResult doInBackground(String... strings) {
                ApiResult result = new ApiResult();
                //result = httpPost(apiKey, accessToken, host, endpoint, params);

                return result;
            }

            @Override
            protected void onPostExecute(ApiResult result) {
                Log.d("onPostExecute: ", "Called.");
                ApiClient.this.onPostExecute(result);
            }
        };
        String[] args = {};
        task.execute(args);
    }

    public void get() {

    }



    protected ApiResult httpPost(String apiKey, String accessToken, String host, final String endpoint,  HashMap<String,String> params) {

        SyncHttpClient client = new SyncHttpClient();
        client.setTimeout(5000);
        RequestParams requestParams = new RequestParams(params);

        requestParams.put("ApiKey", apiKey);

        if ( accessToken != null ) {
            requestParams.put("AccessToken", accessToken);
        } else if (! accessToken.equals("undefined")) {
            // If we already have an access token, add it
            Log.d("AccessToken check: ", "Exists. Adding to params.");
            requestParams.put("AccessToken", accessToken);
        } else {
            Log.d("AccessToken check: ", "Not found. Continuing without it.");
        }


        requestParams.put(ApiParams.deviceId, config.getDeviceUUID());
        requestParams.put(ApiParams.build, config.getBuildNumber());
        requestParams.put(ApiParams.appId, config.getAppId());
        requestParams.put(ApiParams.appVersion, config.getAppVersion());
        requestParams.put(ApiParams.lang, config.getLanguage());
        requestParams.put(ApiParams.region, config.getRegion());

        Log.d("POST hostname: ", host);
        Log.d("POST endpoint: ", endpoint);


        ApiResult result = new ApiResult();
        client.post(host + endpoint, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("onSuccess: ", "Called.");
                Log.d(endpoint + " response", response.toString());




                ApiResult result = new ApiResult();
                result.httpStatusCode = statusCode;
                try {
                    result.responseCode = response.getInt("responseCode");
                    result.success = response.getInt("success");
                    result.message = response.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // Update the class variable so we can return it.
                complete(result);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable e) {
                super.onFailure(statusCode, headers, response, e);



                ApiResult result = new ApiResult();
                result.httpStatusCode = statusCode;
                result.responseCode = 69997;
                result.success = 0;
                result.message = response;



                // Update the class variable so we can return it.
                complete(result);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);



                ApiResult result = new ApiResult();
                result.httpStatusCode = statusCode;
                try {
                    result.responseCode = errorResponse.getInt("responseCode");
                    result.success = errorResponse.getInt("success");
                    result.message = errorResponse.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // Update the class variable so we can return it.
                complete(result);

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)



                ApiResult result = new ApiResult();
                result.httpStatusCode = statusCode;
                result.responseCode = 69000 + statusCode;
                result.success = 0;
                result.message = throwable.getMessage();


                // Update the class variable so we can return it.
                complete(result);

            }

        });




        return this.result;
    }

    protected void httpGet(String apiKey, String accessToken, String host, String urlPath,  String[] params) {

    }

}