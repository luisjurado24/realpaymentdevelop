package com.realpaymentdevelop.app.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import com.realpaymentdevelop.app.activity.LogInActivity;
import com.realpaymentdevelop.app.utils.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Bob Saget on 6/13/15.
 */
public class Utils {

    /**
     *
     */
    public static void launch (Activity target, Class launch) {
        Utils.launch(target, launch, (String)null);
    }


    /**
     *
     * @param hex
     * @return
     */
    public static String hex_to_binary(String hex) {
        String hex_char,bin_char,binary;
        binary = "";
        int len = hex.length()/2;
        for(int i=0;i<len;i++){
            hex_char = hex.substring(2*i,2*i+2);
            int conv_int = Integer.parseInt(hex_char,16);
            bin_char = Integer.toBinaryString(conv_int);
            bin_char = zero_pad_bin_char(bin_char);
            if(i==0) binary = bin_char;
            else binary = binary+bin_char;
            //out.printf("%spotSelected %spotSelected\n", hex_char,bin_char);
        }
        return binary;
    }


    /**
     *
     * @param bin_char
     * @return
     */
    public static String zero_pad_bin_char(String bin_char){
        int len = bin_char.length();
        if(len == 8) return bin_char;
        String zero_pad = "0";
        for(int i=1;i<8-len;i++) zero_pad = zero_pad + "0";
        return zero_pad + bin_char;
    }


    /**
     *
     */
    public static String stringToHex(String arg) {
        return String.format("%x", new BigInteger(1, arg.getBytes()));
    }


    /**
     *
     */
    public static void launch (Activity target, Class launch, String extra) {
        Intent intent = new Intent(target.getApplicationContext(), launch);
        if (extra != null) {
            intent.putExtra("EXTRA", extra);
        }
        target.startActivity(intent);
    }

    /**
     *
     */
    public static void launch (Activity target, Class launch, String extraKeyName, String extra) {
        Intent intent = new Intent(target.getApplicationContext(), launch);
        if (extra != null) {
            intent.putExtra(extraKeyName, extra);
        }
        target.startActivity(intent);
    }


    /**
     *
     */
    public static void launch (Activity target, Class launch, Bundle bundle) {
        Intent intent = new Intent(target.getApplicationContext(), launch);
        if ( bundle != null ) {
            intent.putExtras(bundle);
        }
        target.startActivity(intent);
    }

    /**
     *
     */
    public static void launch(Activity target, Class launch, boolean addToBackStack) {
        Intent intent = new Intent(target.getApplicationContext(), launch);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        target.startActivity(intent);
        if (!addToBackStack) {
            target.finish();
        }
    }

    /**
     *
     */
    public static void launch(Activity target, Class launch, int flags) {
        Intent intent = new Intent(target.getApplicationContext(), launch);
        intent.addFlags(flags);
        target.startActivity(intent);
    }

    /**
     *
     */
    public static String formatDate (String date) {
        // LANG: translation and date formatting
        Log.d("Func: formatDate: ", "Input: "+date); // Ex. 2015-12-25 11:33:33
        String[] dateInfo = date.split(" ");
        String[] dateDate = dateInfo[0].split("-");
        String dateDay = dateDate[2];
        int dateM = Integer.valueOf(dateDate[1]);

        String[] months = "Jan Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "); // Changed MON to Jan, dont change back
        String dateMonth = months[dateM];

        String response;
        if (dateDay == "0" || dateDay == "00") {
            response = "01" + " " + dateMonth;
        }
        else {
            response = dateDay + " " + dateMonth;
        }

        return response;
    }

    public static String formatDateWithYear (String date) {
        // LANG: translation and date formatting
        //Log.d("Func: formatDate: ", "Input: "+date); // Ex. 2015-12-25 11:33:33
        String[] dateInfo = date.split(" ");
        String[] dateDate = dateInfo[0].split("-");
        String dateDay = dateDate[2];
        int dateM = Integer.valueOf(dateDate[1]);

        String[] months = "Jan Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "); // Changed MON to Jan, dont change back
        String dateMonth = months[dateM];

        String response;
        if (dateDay == "0" || dateDay == "00") {
            response = "01" + " " + dateMonth;
        }
        else {
            response = dateDay + " " + dateMonth;
        }

        return response + " " + dateDate[0];
    }

    /**
     *
     */
    public static String formatToken(String token_){
        String[] token = token_.split("");
        String _token = "";
        Log.d("Func: FormatToken: ", token.toString());

        int counter = 0;

        for(int i = 0 ; i < token.length ; i++) {
            if (token[i] != null) {
                _token += token[i];
            }

            if(counter == 4) {
                _token += " "; counter = 0;}
            counter++;
        }

        return _token;
    }


    /**
     *
     */
    public static void Alert(String title, String msg, Activity atx, Boolean okayButton){
        AlertDialog alert;
        alert = new AlertDialog.Builder(atx).create();

        alert.setMessage(msg);
        alert.setTitle(title);

        if(okayButton)
        alert.setButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

       alert.show();
    }


    /**
     *
     */
    public static AlertDialog Alert(String title, String msg, Activity atx){
        AlertDialog alert;
        alert = new AlertDialog.Builder(atx).create();

        alert.setMessage(msg);
        alert.setTitle(title);

        return alert;
    }

    public static boolean isDeviceInSleepMode(Context context) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            isScreenOn = pm.isScreenOn();
        } else {
            isScreenOn = pm.isInteractive();
        }
        return !isScreenOn;
    }


    public static User makeUser(String json) {
        JSONObject object;
        try {
            object = new JSONObject(json);
            return new User(object);
        }
        catch (JSONException e) {
            Log.d("JSONException", e.getMessage());
        }
        return new User();
    }

    /**
     *
     */
    static public User makeUser(Context context, Activity activity) {
        JSONObject user;
        Config config = new Config(context);
        String loginJson = config.get("login", "undefined");
        Log.d("makeUser", "Value of loginJson: "+loginJson);

        if (! loginJson.equals("undefined")) {
            try {
                user = new JSONObject(loginJson);
                return new User(user);
            }
            catch (JSONException e) {
                Log.d("JSONException", e.getMessage());
            }
        }
        else {
            config.set("login", "undefined");
            config.set("remember", "false");
            config.set("token", "undefined");
            Log.d("Utils", "makeUser error: launching Welcome class.");
            launch(activity, LogInActivity.class);
            return new User();
        }

        return new User();
    }


    /**
     *
     */
    public static void backButton(View button, Activity atx){
            View backButtonView = button;
            backButtonView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  Utils.launch(atx, MainInitial.class);
                }
            });
    }

    public static String getDeviceUID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static String getPrepaidType(int prepaidAccountTypeId)  {
        String cardType;
        switch (prepaidAccountTypeId)  {
            case 1:
                cardType = "Giftcard";
                break;
            case 2:
                cardType = "Promo";
                break;
            default:
                cardType = "";
                break;
        }

        return cardType;
    }

    public static String formatMoney(String rawMoney)  {
        String formatMoney = "$" + String.format("%.2f", Float.valueOf(rawMoney));
        return formatMoney;
    }

    public static String formatDateIntl(String inDate, Context context)  {
        String date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dDate = sdf.parse(inDate);
            java.text.DateFormat dateFormat = android.text.format.DateFormat.getMediumDateFormat(context);
            date = dateFormat.format(dDate);
        } catch (java.text.ParseException e) {
            date = "";
            e.printStackTrace();
        }
        return date;
    }

    public static String formatTimeFromDate(String inDate)  {
        int hour = Integer.valueOf(inDate.substring(11,13));
        int minutes = Integer.valueOf(inDate.substring(14,16));

        String suffix = " AM";
        if (hour > 12)  {
            hour -= 12;
            suffix = " PM";
        }

        return String.format("%d:%02d %s", hour, minutes, suffix);
    }

    public static String formatValidDates(String activationDate, String expirationDate, Context context)  {
        boolean hasActive = false, hasExpire = false;
        String activationFormatted = "", expirationFormatted = "";
        if (! (activationDate.isEmpty() || activationDate.equals("null")))  {
            activationFormatted = formatDateIntl(activationDate, context);
            hasActive = true;
        }
        if (! (expirationDate.isEmpty() || expirationDate.equals("null")))  {
            hasExpire = true;
            expirationFormatted = formatDateIntl(expirationDate, context);
        }

        String retString;
        if (hasActive && hasExpire)  {
            retString = "Valid " + activationFormatted + " - " + expirationFormatted;
        }
        else if (hasActive)  {
            retString = "Valid From " + activationFormatted;
        }
        else if (hasExpire)  {
            retString = "Valid Until " + expirationFormatted;
        }
        else  {
            retString = "Never Expires";
        }

        return retString;
    }

    private static final char[] BYTE2HEX=
            ("000102030405060708090A0B0C0D0E0F" +
             "101112131415161718191A1B1C1D1E1F" +
             "202122232425262728292A2B2C2D2E2F" +
             "303132333435363738393A3B3C3D3E3F" +
             "404142434445464748494A4B4C4D4E4F" +
             "505152535455565758595A5B5C5D5E5F" +
             "606162636465666768696A6B6C6D6E6F" +
             "707172737475767778797A7B7C7D7E7F" +
             "808182838485868788898A8B8C8D8E8F" +
             "909192939495969798999A9B9C9D9E9F" +
             "A0A1A2A3A4A5A6A7A8A9AAABACADAEAF" +
             "B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF" +
             "C0C1C2C3C4C5C6C7C8C9CACBCCCDCECF" +
             "D0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF" +
             "E0E1E2E3E4E5E6E7E8E9EAEBECEDEEEF" +
             "F0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF").toCharArray();

    public static String getHexString(byte[] bytes) {
        final int len=bytes.length;
        final char[] chars=new char[len<<1];
        int hexIndex;
        int idx=0;
        int ofs=0;
        while (ofs<len) {
            hexIndex=(bytes[ofs++] & 0xFF)<<1;
            chars[idx++]=BYTE2HEX[hexIndex++];
            chars[idx++]=BYTE2HEX[hexIndex];
        }
        return new String(chars);
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

}


