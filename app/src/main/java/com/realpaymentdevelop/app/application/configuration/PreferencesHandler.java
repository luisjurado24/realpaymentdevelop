package com.realpaymentdevelop.app.application.configuration;

import androidx.annotation.NonNull;

public interface PreferencesHandler {
    boolean isAutoLogoutEnabled();

    void setAutoLogoutEnabled(boolean enabled);

    void logoutUser();

    boolean isLaunchedFirstTime();

    void setLaunchedFirstTime(boolean isLaunchedFirstTime);

    void setDeviceUUID(@NonNull String deviceUUID);

    String getDeviceUUID();

    void setLanguage(String language);

    String getLanguage();

    void setRegion(String region);

    String getRegion();

    void setName(String name);
    String getName();



    void setBuildNumber(String buildNumber);

    String getBuildNumber();

    void setAppVersion(String appVersion);

    String getAppVersion();

    void setAppId(String appId);

    String getAppId();
}
