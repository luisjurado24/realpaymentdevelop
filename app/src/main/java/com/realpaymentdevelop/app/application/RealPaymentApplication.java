package com.realpaymentdevelop.app.application;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustConfig;
import com.adjust.sdk.BuildConfig;
import com.realpaymentdevelop.app.*;
import com.realpaymentdevelop.app.R;
import com.realpaymentdevelop.app.utils.Config;
import com.realpaymentdevelop.app.utils.Utils;

import java.util.Locale;

public class RealPaymentApplication extends Application {
    private static RealPaymentApplication instance;
    private Activity currentActivity;
    private Config config;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        config = new Config(this);
        if (config.isLaunchedFirstTime()) {
            config.setDeviceUUID(Utils.getDeviceUID(this));
            config.setLaunchedFirstTime(false);
        }
        setUpAppInfo();
        setupAdjustTracking();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static RealPaymentApplication getInstance() {
        return instance;
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }

    private void setUpAppInfo() {
        config.setLanguage(Locale.getDefault().getLanguage());
        config.setRegion(Locale.getDefault().getCountry());
        config.setBuildNumber(String.valueOf(BuildConfig.VERSION_CODE));
        //config.setAppId(BuildConfig.APPLICATION_ID);
        config.setAppVersion(BuildConfig.VERSION_NAME);
    }

    public Config getConfig() {
        return config;
    }

    public void setupAdjustTracking() {
        boolean adjustSdkEnable = this.getResources().getBoolean(R.bool.adjust_sdk_enable);
        if ( ! adjustSdkEnable ) {
            Log.d("ADJUST","Adjust is not enabled");
            return;
        }
        String appToken = this.getString(R.string.adjust_sdk_token);
        Log.d("ADJUST", "Token = " + appToken);
        boolean adjustSdkTest = this.getResources().getBoolean(R.bool.adjust_sdk_test);

        String environment = AdjustConfig.ENVIRONMENT_SANDBOX;
        String mode = "sandbox";
        if ( ! adjustSdkTest ) {
            mode = "production";
            environment = AdjustConfig.ENVIRONMENT_PRODUCTION;
        }
        Log.d("ADJUST", "Adjust is in "+ mode + " mode.");
        AdjustConfig config = new AdjustConfig(this, appToken, environment);
        Adjust.onCreate(config);
    }
}


